# SGE - Empire

A real time strategy game based on the game Empire (1977)

# Installation
Available through jitpack
[![Release](https://jitpack.io/v/com.gitlab.rt-sge/rt-sge-empire.svg)](https://jitpack.io/#com.gitlab.rt-sge/rt-sge-empire/)

**Gradle:** 

Add the JitPack repository to your build.gradle
```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```
Add the Empire dependency
```
dependencies {
    implementation 'com.gitlab.rt-sge:rt-sge-empire:{version}'
}
```
**Other build automation tools:** check [JitPack](https://jitpack.io/#com.gitlab.rt-sge/rt-sge-empire/).

# Ruleset
Refer to the [GitLab Wiki](https://gitlab.com/rt-sge/core/-/wikis/empire).
