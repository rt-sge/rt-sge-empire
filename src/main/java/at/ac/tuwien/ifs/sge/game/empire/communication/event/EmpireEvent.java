package at.ac.tuwien.ifs.sge.game.empire.communication.event;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;

import java.io.Serializable;
import java.util.List;

public interface EmpireEvent extends Serializable {

    default List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {return List.of();}
}
