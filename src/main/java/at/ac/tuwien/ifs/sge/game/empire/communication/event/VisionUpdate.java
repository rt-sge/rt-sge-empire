package at.ac.tuwien.ifs.sge.game.empire.communication.event;

import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;

import java.util.List;
import java.util.Map;

public class VisionUpdate implements EmpireEvent {
    private final Map<Position, EmpireTerrain> newActive;
    private final List<Position> newInactive;
    private final int playerId;

    public VisionUpdate(int activePlayerId, Map<Position, EmpireTerrain> tiles, List<Position> newInactive) {
        this.newActive = tiles;
        this.playerId = activePlayerId;
        this.newInactive = newInactive;
    }

    public Map<Position, EmpireTerrain> getNewActive() {
        return newActive;
    }

    public int getPlayerId() {
        return playerId;
    }

    public List<Position> getNewInactive() {
        return newInactive;
    }

    @Override
    public String toString() {
        var nrOfNewActive = newActive != null ? newActive.size() : 0;
        var nrOfNewInactive = newInactive != null ? newInactive.size() : 0;
        return "vision update -> tiles discovered: " + nrOfNewActive + " | tiles hidden: " + nrOfNewInactive;
    }
}
