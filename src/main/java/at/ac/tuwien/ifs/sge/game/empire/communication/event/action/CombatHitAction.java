package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.VisionUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitHitActionResult;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;

import java.util.*;
import java.util.stream.Collectors;

public class CombatHitAction implements EmpireAction {
    private final UUID attackerId;
    private final UUID targetId;
    private final int calculatedDamage;

    public CombatHitAction(UUID attackerId, UUID targetId, int calculatedDamage) {
        this.attackerId = attackerId;
        this.targetId = targetId;
        this.calculatedDamage = calculatedDamage;
    }

    public UUID getAttackerId() {
        return attackerId;
    }

    public UUID getTargetId() {
        return targetId;
    }

    public int getCalculatedDamage() {
        return calculatedDamage;
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var map = empire.getBoard();
        var unitsById = empire.getUnitsById();
        var hitResult = (UnitHitActionResult) result;
        var unitKilled = hitResult.getKilledUnit() != null;

        var target = unitKilled ? hitResult.getKilledUnit() : unitsById.get(targetId);
        var attacker = unitsById.get(attackerId);

        var targetPos = target.getPosition();
        var attackPos = attacker.getPosition();

        var playersVisionOnTarget = map.getListOfPlayersWithVisionOnPosition(targetPos);

        var targetPlayerId = target.getPlayerId();
        if (unitKilled && !playersVisionOnTarget.contains(targetPlayerId)) {
            playersVisionOnTarget.add(targetPlayerId);
        }
        var playersVisionOnAttacker = map.getListOfPlayersWithVisionOnPosition(attackPos);

        var fullVision = playersVisionOnAttacker.stream().filter(playersVisionOnTarget::contains).collect(Collectors.toSet());
        var partialVisionTarget = playersVisionOnTarget.stream().filter(t -> !fullVision.contains(t)).toList();

        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);

        if (!fullVision.isEmpty()) {
            gameUpdateComposite.addActionForPlayers(this, fullVision);
        }
        if (!partialVisionTarget.isEmpty()) {
            var unitDamagedAction = new UnitDamagedAction(calculatedDamage, targetId);
            gameUpdateComposite.addActionForPlayers(unitDamagedAction, partialVisionTarget);
        }

        var hiddenTiles = hitResult.getHiddenTiles();
        if (unitKilled && hiddenTiles != null && !hiddenTiles.isEmpty()) {
            var visionUpdate = new VisionUpdate(targetPlayerId, null, hiddenTiles);
            gameUpdateComposite.addActionForPlayer(visionUpdate, targetPlayerId);
        }

        return gameUpdateComposite.toList();
    }

    @Override
    public String toString() {
        return "attacker with id " + attackerId + " hits target with id " + targetId + " and deals " + calculatedDamage + " damage";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CombatHitAction that = (CombatHitAction) o;
        return attackerId.equals(that.attackerId) && targetId.equals(that.targetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attackerId, targetId);
    }
}
