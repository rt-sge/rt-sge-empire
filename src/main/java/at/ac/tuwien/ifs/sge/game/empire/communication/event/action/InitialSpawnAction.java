package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.VisionUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitCreationResult;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class InitialSpawnAction implements EmpireAction {
    private final Position position;
    private final int unitTypeId;
    private final UUID unitId;
    private final int playerId;

    public InitialSpawnAction(Position position, int unitTypeId, UUID unitId, int playerId) {
        this.position = new Position(position);
        this.unitTypeId = unitTypeId;
        this.unitId = unitId;
        this.playerId = playerId;
    }

    public Position getPosition() {
        return position;
    }

    public int getUnitTypeId() {
        return unitTypeId;
    }

    public UUID getUnitId() {
        return unitId;
    }

    public int getPlayerId() {
        return playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InitialSpawnAction that = (InitialSpawnAction) o;
        return unitTypeId == that.unitTypeId && playerId == that.playerId && position.equals(that.position) && unitId.equals(that.unitId);
    }

    @Override
    public String toString() {
        return "spawn unit with id " + unitId + " at position " + position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, unitTypeId, unitId, playerId);
    }


    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var creationResult = (UnitCreationResult) result;
        var discoveredTiles = creationResult.getDiscoveredTiles();
        if (!discoveredTiles.isEmpty()) {
            var visionUpdate = new VisionUpdate(playerId, discoveredTiles, null);
            return List.of(new GameUpdate<>(List.of(visionUpdate), playerId, executionTimeMs));
        }
        return List.of();
    }
}
