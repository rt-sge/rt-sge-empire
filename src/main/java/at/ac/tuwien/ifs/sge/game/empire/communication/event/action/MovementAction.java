package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.VisionUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitMoveActionResult;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class MovementAction implements EmpireAction {
    private final UUID unitId;
    private final Position destination;
    private final Position origin;

    public MovementAction(UUID unitId, Position destination, Position origin) {
        this.unitId = unitId;
        this.destination = new Position(destination);
        this.origin = new Position(origin);
    }

    public UUID getUnitId() {
        return unitId;
    }

    public Position getDestination() {
        return destination;
    }

    public Position getOrigin() {
        return origin;
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var map = empire.getBoard();
        var unitsById = empire.getUnitsById();
        var playerId = empire.getUnit(unitId).getPlayerId();
        var movementResult = (UnitMoveActionResult) result;
        var visionTileChanges = movementResult.getVisionTileChanges();
        var discoveredTiles = visionTileChanges.getDiscoveredTiles();
        var hiddenTiles = visionTileChanges.getHiddenTiles();

        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);

        if (discoveredTiles.size() > 0 || hiddenTiles.size() > 0) {
            var visionUpdate = new VisionUpdate(playerId, discoveredTiles, hiddenTiles);
            gameUpdateComposite.addActionForPlayer(visionUpdate, playerId);
        }

        var playersVisionToOrigin = map.getListOfPlayersWithVisionOnPosition(origin);
        var playersVisionToDestination = map.getListOfPlayersWithVisionOnPosition(destination);

        var playersFullVision = playersVisionToOrigin.stream().filter(playersVisionToDestination::contains).toList();
        var playersPartialVisionOrigin = playersVisionToOrigin.stream().filter(e -> !playersFullVision.contains(e)).toList();
        var playersPartialVisionDestination = playersVisionToDestination.stream().filter(e -> !playersFullVision.contains(e)).toList();


        if (!playersPartialVisionOrigin.isEmpty())
            gameUpdateComposite.addActionForPlayers(new UnitVanishedAction(unitId), playersPartialVisionOrigin);
        if (!playersPartialVisionDestination.isEmpty())
            gameUpdateComposite.addActionForPlayers(new UnitAppearedAction(unitsById.get(unitId)), playersPartialVisionDestination);
        if (!playersFullVision.isEmpty())
            gameUpdateComposite.addActionForPlayers(this, playersFullVision);

        return gameUpdateComposite.toList();
    }

    @Override
    public String toString() {
        return "move unit with id " + unitId + " from " + origin + " to " + destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovementAction that = (MovementAction) o;
        return unitId.equals(that.unitId) && destination.equals(that.destination) && origin.equals(that.origin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unitId, destination, origin);
    }
}
