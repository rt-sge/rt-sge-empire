package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.VisionUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitCreationResult;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class ProductionAction implements EmpireAction {

    private final Position cityPosition;
    private final int unitTypeId;
    private final UUID unitId;
    private final boolean isAgentUpdate;

    public ProductionAction(Position cityPosition, int unitTypeId, UUID unitId) {
        this.cityPosition = new Position(cityPosition);
        this.unitTypeId = unitTypeId;
        this.unitId = unitId;
        this.isAgentUpdate = false;
    }

    public ProductionAction(ProductionAction action, boolean isAgentUpdate) {
        this.cityPosition = new Position(action.getCityPosition());
        this.unitTypeId = action.getUnitTypeId();
        this.unitId = action.getUnitId();
        this.isAgentUpdate = isAgentUpdate;
    }

    public ProductionAction(ProductionStartOrder order){
        this(order.getCityPosition(), order.getUnitTypeId(), order.hasUnitId() ? order.getUnitId() : UUID.randomUUID());
    }

    public UUID getUnitId() {
        return unitId;
    }

    public Position getCityPosition() {
        return cityPosition;
    }

    public int getUnitTypeId() {
        return unitTypeId;
    }

    public boolean isAgentUpdate() {
        return isAgentUpdate;
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var playerId = empire.getUnit(unitId).getPlayerId();
        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);
        var creationResult = (UnitCreationResult) result;
        var discoveredTiles = creationResult.getDiscoveredTiles();
        if (discoveredTiles.size() > 0) {
            var visionUpdate = new VisionUpdate(playerId, discoveredTiles, null);
            gameUpdateComposite.addActionForPlayer(visionUpdate, playerId);
        }
        var playersToUpdate = empire.getBoard().getListOfPlayersWithVisionOnPosition(cityPosition);
        if (!playersToUpdate.isEmpty())
            gameUpdateComposite.addActionForPlayers(new ProductionAction(this, true), playersToUpdate);

        return gameUpdateComposite.toList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductionAction that = (ProductionAction) o;
        return unitTypeId == that.unitTypeId && cityPosition.equals(that.cityPosition) && unitId.equals(that.unitId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityPosition, unitTypeId, unitId);
    }

    @Override
    public String toString() {
        return "produce unit with id " + unitId + " at city with position " + cityPosition;
    }
}
