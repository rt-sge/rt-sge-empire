package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;

import java.util.Objects;

public class UnitAppearedAction implements EmpireAction {
    private final EmpireUnit unit;

    public UnitAppearedAction(EmpireUnit unit) {
        this.unit = new EmpireUnit(unit);
    }

    public EmpireUnit getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return "unit appeared: " + unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitAppearedAction that = (UnitAppearedAction) o;
        return unit.equals(that.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unit);
    }
}
