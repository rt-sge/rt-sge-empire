package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import java.util.Objects;
import java.util.UUID;

public class UnitDamagedAction implements EmpireAction {
    private final int damageDealt;
    private final UUID targetId;

    public UnitDamagedAction(int damageDealt, UUID targetId) {
        this.damageDealt = damageDealt;
        this.targetId = targetId;
    }

    public int getDamageDealt() {
        return damageDealt;
    }

    public UUID getTargetId() {
        return targetId;
    }

    @Override
    public String toString() {
        return "unit with id " + targetId + " was damaged by " + damageDealt + " hitpoints";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UnitDamagedAction)) return false;
        UnitDamagedAction that = (UnitDamagedAction) o;
        return damageDealt == that.damageDealt && Objects.equals(targetId, that.targetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(damageDealt, targetId);
    }
}
