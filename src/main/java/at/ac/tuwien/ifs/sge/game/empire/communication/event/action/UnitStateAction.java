package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;

import java.util.UUID;

public class UnitStateAction implements EmpireAction {
    private final UUID unitId;
    private final EmpireUnitState state;
    private final UUID targetId;

    public UnitStateAction(UUID unitId, EmpireUnitState state, UUID targetId) {
        this.unitId = unitId;
        this.state = state;
        this.targetId = targetId;
    }

    public UUID getUnitId() {
        return unitId;
    }

    public EmpireUnitState getState() {
        return state;
    }

    public UUID getTargetId() {
        return targetId;
    }

    @Override
    public String toString() {
        return "unit with id " + unitId + " had a state change to " + state + " and target change to " + targetId;
    }
}
