package at.ac.tuwien.ifs.sge.game.empire.communication.event.action;

import java.util.Objects;
import java.util.UUID;

public class UnitVanishedAction implements EmpireAction {
    private final UUID vanishedId;

    public UnitVanishedAction(UUID vanishedId) {
        this.vanishedId = vanishedId;
    }

    public UUID getVanishedId() {
        return vanishedId;
    }

    @Override
    public String toString() {
        return "unit with id " + vanishedId + " vanished";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitVanishedAction that = (UnitVanishedAction) o;
        return vanishedId.equals(that.vanishedId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vanishedId);
    }
}
