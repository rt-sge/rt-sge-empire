package at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import java.util.UUID;

public class ProductionStartResult extends ActionResult {

    private final UUID producingUnitId;


    public ProductionStartResult(boolean success, UUID producingUnitId) {
        super(success);
        this.producingUnitId = producingUnitId;
    }

    public UUID getProducingUnitId() {
        return producingUnitId;
    }

}
