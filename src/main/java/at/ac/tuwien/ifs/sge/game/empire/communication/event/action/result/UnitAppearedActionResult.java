package at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;

import java.util.List;
import java.util.UUID;

public class UnitAppearedActionResult extends ActionResult {

    private final List<UUID> stoppedMovingUnits;

    public UnitAppearedActionResult(boolean success, List<UUID> stoppedMovingUnits) {
        super(success);
        this.stoppedMovingUnits = stoppedMovingUnits;
    }

    public List<UUID> getStoppedMovingUnits() {
        return stoppedMovingUnits;
    }
}
