package at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;

import java.util.HashMap;

public class UnitCreationResult extends ActionResult {

    private final HashMap<Position, EmpireTerrain> discoveredTiles;

    public UnitCreationResult(boolean success, HashMap<Position, EmpireTerrain> discoveredTiles) {
        super(success);
        this.discoveredTiles = discoveredTiles;
    }

    public HashMap<Position, EmpireTerrain> getDiscoveredTiles() {
        return discoveredTiles;
    }
}
