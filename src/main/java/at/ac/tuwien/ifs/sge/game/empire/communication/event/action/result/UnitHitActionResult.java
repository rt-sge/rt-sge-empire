package at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;

import java.util.List;
import java.util.UUID;

public class UnitHitActionResult extends ActionResult {
    private final EmpireUnit killedUnit;
    private final List<Position> hiddenTiles;
    private final List<UUID> idleUnits;

    public UnitHitActionResult(boolean success, EmpireUnit killedUnit, List<Position> hiddenTiles, List<UUID> idleUnits) {
        super(success);
        this.killedUnit = killedUnit;
        this.hiddenTiles = hiddenTiles;
        this.idleUnits = idleUnits;
    }

    public EmpireUnit getKilledUnit() {
        return killedUnit;
    }

    public List<Position> getHiddenTiles() {
        return hiddenTiles;
    }

    public List<UUID> getIdleUnits() {
        return idleUnits;
    }
}
