package at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.game.empire.map.VisionTileChanges;

import java.util.List;
import java.util.UUID;

public class UnitMoveActionResult extends ActionResult {
    private final VisionTileChanges visionTileChanges;
    private final List<UUID> interruptedMovingUnits;
    private final List<UUID> interruptedFightingUnits;
    private final boolean startedDefending;

    public UnitMoveActionResult(boolean success,
                                VisionTileChanges visionTileChanges,
                                List<UUID> interruptedMovingUnits,
                                List<UUID> interruptedFightingUnits,
                                boolean startedDefending) {
        super(success);
        this.visionTileChanges = visionTileChanges;
        this.interruptedMovingUnits = interruptedMovingUnits;
        this.interruptedFightingUnits = interruptedFightingUnits;
        this.startedDefending = startedDefending;
    }

    public VisionTileChanges getVisionTileChanges() {
        return visionTileChanges;
    }

    public List<UUID> getInterruptedMovingUnits() {
        return interruptedMovingUnits;
    }

    public List<UUID> getInterruptedFightingUnits() {
        return interruptedFightingUnits;
    }

    public boolean hasStartedDefending() {
        return startedDefending;
    }
}
