package at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

import java.util.List;
import java.util.UUID;

public class UnitVanishedActionResult extends ActionResult {

    private final List<Position> hiddenTiles;
    private final List<UUID> interruptedFightingUnits;

    public UnitVanishedActionResult(boolean success, List<Position> hiddenTiles, List<UUID> interruptedFightingUnits) {
        super(success);
        this.hiddenTiles = hiddenTiles;
        this.interruptedFightingUnits = interruptedFightingUnits;
    }

    public List<Position> getHiddenTiles() {
        return hiddenTiles;
    }

    public List<UUID> getInterruptedFightingUnits() {
        return interruptedFightingUnits;
    }
}
