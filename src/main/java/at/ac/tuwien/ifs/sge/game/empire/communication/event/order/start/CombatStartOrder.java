package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitStateAction;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class CombatStartOrder implements EmpireStartOrder {
    private final UUID attackerId;
    private final UUID targetId;

    public CombatStartOrder(UUID attackerId, UUID targetId) {
        this.attackerId = attackerId;
        this.targetId = targetId;
    }

    public UUID getAttackerId() {
        return attackerId;
    }

    public UUID getTargetId() {
        return targetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CombatStartOrder that = (CombatStartOrder) o;
        return attackerId.equals(that.attackerId) && targetId.equals(that.targetId);
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var map = empire.getBoard();
        var unitsById = empire.getUnitsById();
        if (!unitsById.containsKey(attackerId) || !unitsById.containsKey(targetId))
            return List.of();

        var attackPos = unitsById.get(attackerId).getPosition();
        var targetPos = unitsById.get(targetId).getPosition();
        var playersVisionOnAttacker = map.getListOfPlayersWithVisionOnPosition(attackPos);
        var playersVisionOnTarget = map.getListOfPlayersWithVisionOnPosition(targetPos);

        var playersFullVision = playersVisionOnAttacker.stream().filter(playersVisionOnTarget::contains).toList();
        var partialVisionAttacker = playersVisionOnAttacker.stream().filter(e -> !playersFullVision.contains(e)).toList();

        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);
        if (!playersFullVision.isEmpty())
            gameUpdateComposite.addActionForPlayers(this, playersFullVision);
        if (!partialVisionAttacker.isEmpty())
            gameUpdateComposite.addActionForPlayers(new UnitStateAction(attackerId, EmpireUnitState.Fighting, targetId), partialVisionAttacker);
        return gameUpdateComposite.toList();
    }

    @Override
    public int hashCode() {
        return Objects.hash(attackerId, targetId);
    }

    @Override
    public String toString() {
        return attackerId + " attack " + targetId;
    }
}
