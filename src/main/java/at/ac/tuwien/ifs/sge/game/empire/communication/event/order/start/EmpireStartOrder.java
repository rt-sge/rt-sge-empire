package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;

public interface EmpireStartOrder extends EmpireEvent {
}
