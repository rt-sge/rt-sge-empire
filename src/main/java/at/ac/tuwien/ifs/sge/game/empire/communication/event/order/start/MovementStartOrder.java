package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class MovementStartOrder implements EmpireStartOrder {
    private final UUID unitId;
    private final Position destination;

    public MovementStartOrder(UUID unitId, Position destination) {
        this.unitId = unitId;
        this.destination = new Position(destination);
    }
    public MovementStartOrder(EmpireUnit unit, Position destination) {
        this.unitId = unit.getId();
        this.destination = new Position(destination);
    }

    public UUID getUnitId() {
        return unitId;
    }

    public Position getDestination() {
        return destination;
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var map = empire.getBoard();
        var unitsById = empire.getUnitsById();
        if (!unitsById.containsKey(unitId))
            return Collections.emptyList();
        var unit = unitsById.get(unitId);

        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);
        var playersVisionToOrigin = map.getListOfPlayersWithVisionOnPosition(unit.getPosition());

        if (!playersVisionToOrigin.isEmpty())
            gameUpdateComposite.addActionForPlayers(this, playersVisionToOrigin);

        return gameUpdateComposite.toList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovementStartOrder that = (MovementStartOrder) o;

        if (!unitId.equals(that.unitId)) return false;
        return destination.equals(that.destination);
    }

    @Override
    public int hashCode() {
        int result = unitId.hashCode();
        result = 31 * result + destination.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return unitId + " move to position " + destination;
    }
}
