package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.ProductionStartResult;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

public class ProductionStartOrder implements EmpireStartOrder {
    private final Position cityPosition;
    private final int unitTypeId;
    private UUID unitId;


    public ProductionStartOrder(Position cityPosition, int unitTypeId) {
        this.cityPosition = new Position(cityPosition);
        this.unitTypeId = unitTypeId;
    }

    private ProductionStartOrder(Position cityPosition, int unitTypeId, UUID unitId) {
        this.cityPosition = new Position(cityPosition);
        this.unitTypeId = unitTypeId;
        this.unitId = unitId;
    }

    public Position getCityPosition() {
        return cityPosition;
    }

    public int getUnitTypeId() {
        return unitTypeId;
    }

    public UUID getUnitId() { return unitId; }

    public boolean hasUnitId() { return unitId != null; }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var productionStartResult = (ProductionStartResult) result;
        var unitId = productionStartResult.getProducingUnitId();
        return List.of( new GameUpdate<>(
            List.of(new ProductionStartOrder(cityPosition, unitTypeId, unitId)),
            empire.getCitiesByPosition().get(cityPosition).getPlayerId(),
            executionTimeMs
        ));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductionStartOrder that = (ProductionStartOrder) o;

        if (unitTypeId != that.unitTypeId) return false;
        return cityPosition.equals(that.cityPosition);
    }

    @Override
    public int hashCode() {
        int result = cityPosition.hashCode();
        result = 31 * result + unitTypeId;
        return result;
    }

    @Override
    public String toString() {
        return "city at " + cityPosition + " start to produce unitType " + unitTypeId;
    }
}
