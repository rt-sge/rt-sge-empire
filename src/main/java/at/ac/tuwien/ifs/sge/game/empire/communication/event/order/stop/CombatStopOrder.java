package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CombatStopOrder implements EmpireStopOrder {
    private final UUID attackerId;

    public CombatStopOrder(UUID attackerId) {
        this.attackerId = attackerId;
    }
    public CombatStopOrder(CombatStartOrder order)
    {
        this(order.getAttackerId());
    }

    public UUID getAttackerId() {
        return attackerId;
    }


    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var map = empire.getBoard();
        var unitsById = empire.getUnitsById();
        if (!unitsById.containsKey(attackerId))
            return List.of();
        var attackPos = unitsById.get(attackerId).getPosition();
        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);
        var playersToUpdate = map.getListOfPlayersWithVisionOnPosition(attackPos);
        if (!playersToUpdate.isEmpty())
            gameUpdateComposite.addActionForPlayers(this, playersToUpdate);
        return gameUpdateComposite.toList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CombatStopOrder that = (CombatStopOrder) o;

        return attackerId.equals(that.attackerId);
    }

    @Override
    public int hashCode() {
        return attackerId.hashCode();
    }

    @Override
    public String toString() {
        return attackerId + " stops attacking";
    }
}
