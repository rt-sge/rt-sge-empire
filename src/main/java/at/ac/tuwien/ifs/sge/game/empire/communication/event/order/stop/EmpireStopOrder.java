package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;

public interface EmpireStopOrder extends EmpireEvent {
}
