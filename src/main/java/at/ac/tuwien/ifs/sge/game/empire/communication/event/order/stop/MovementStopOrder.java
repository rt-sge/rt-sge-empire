package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.GameUpdateComposite;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MovementStopOrder implements EmpireStopOrder {
    private final UUID unitId;

    public MovementStopOrder(UUID unitId) {
        this.unitId = unitId;
    }

    public UUID getUnitId() {
        return unitId;
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        var map = empire.getBoard();
        var unitsById = empire.getUnitsById();
        if (!unitsById.containsKey(unitId))
            return null;
        var targetPos = unitsById.get(unitId).getPosition();
        var gameUpdateComposite = new GameUpdateComposite<EmpireEvent>(executionTimeMs);
        var playersToUpdate = map.getListOfPlayersWithVisionOnPosition(targetPos);
        if (!playersToUpdate.isEmpty())
            gameUpdateComposite.addActionForPlayers(this, playersToUpdate);
        return gameUpdateComposite.toList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovementStopOrder that = (MovementStopOrder) o;

        return unitId.equals(that.unitId);
    }

    @Override
    public int hashCode() {
        return unitId.hashCode();
    }

    @Override
    public String toString() {
        return unitId + " stops moving";
    }
}
