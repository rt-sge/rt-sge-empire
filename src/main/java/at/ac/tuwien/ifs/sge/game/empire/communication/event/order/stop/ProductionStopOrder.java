package at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

import java.util.List;

public class ProductionStopOrder implements EmpireStopOrder {
    private final Position cityPosition;

    public ProductionStopOrder(Position cityPosition) {

        this.cityPosition = new Position(cityPosition);
    }

    public Position getCityPosition() {
        return cityPosition;
    }

    @Override
    public List<GameUpdate<EmpireEvent>> getUpdates(Empire empire, ActionResult result, long executionTimeMs) {
        return List.of(new GameUpdate<>(List.of(this), empire.getCitiesByPosition().get(cityPosition).getPlayerId(), executionTimeMs));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductionStopOrder that = (ProductionStopOrder) o;

        return cityPosition.equals(that.cityPosition);
    }

    @Override
    public int hashCode() {
        return cityPosition.hashCode();
    }

    @Override
    public String toString() {
        return "city at" + cityPosition + " stops producing";
    }
}
