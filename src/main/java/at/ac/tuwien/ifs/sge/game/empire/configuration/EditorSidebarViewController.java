package at.ac.tuwien.ifs.sge.game.empire.configuration;

import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireRandomMapGeneratorException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.map.Size;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.RandomMapGeneratorConfig;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrainType;
import at.ac.tuwien.ifs.sge.game.empire.visualization.EmpireTerrainSprite;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import io.reactivex.rxjava3.subjects.PublishSubject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

public class EditorSidebarViewController implements Initializable {


    public final PublishSubject<Size> updateMapSizeSubject = PublishSubject.create();

    public final PublishSubject<char[][]> updateMapSubject = PublishSubject.create();

    private final ObservableList<EmpireTerrainSprite> terrainSprites = FXCollections.observableArrayList();
    private final ObservableList<Position> startingPositions = FXCollections.observableArrayList();

    private EmpireConfiguration configuration;

    private RandomMapGenerator rndGenerator;

    @FXML
    TextField mapWidthTextField;
    @FXML
    TextField mapHeightTextField;
    @FXML
    ListView<EmpireTerrainSprite> terrainTypeListView;
    @FXML
    ListView<Position> startingPositionsListView;

    @FXML
    TextField textFieldCityY0;
    @FXML
    TextField textFieldCityX1;
    @FXML
    TextField textFieldCityY1;
    @FXML
    TextField textFieldCityC;

    @FXML
    TextField textFieldMGY0;
    @FXML
    TextField textFieldMGX1;
    @FXML
    TextField textFieldMGY1;


    @FXML
    TextField textFieldMountainProbability;

    @FXML
    TextField textFieldMaximumCitiesPerPlayer;

    @FXML
    Label labelAllCitiesReachable;

    @FXML
    CheckBox checkBoxIsAutoGenerated;

    @FXML
    CheckBox checkBoxMakeCitiesReachable;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupControls();
    }

    public void setup(EmpireConfiguration configuration, Collection<EmpireTerrainSprite> terrainSprites) {
        this.configuration = configuration;
        this.terrainSprites.setAll(terrainSprites);
        this.startingPositions.setAll(configuration.getStartingCities());

        var mapSize = configuration.getMapSize();
        mapWidthTextField.setText(Integer.toString(mapSize.getWidth()));
        mapHeightTextField.setText(Integer.toString(mapSize.getHeight()));

        var generatorConfig = configuration.getGeneratorConfig();
        initRandomGeneratorConfig(generatorConfig, configuration.getHasAutoGeneratedMap());

        rndGenerator = new RandomMapGenerator(this.configuration);

        labelAllCitiesReachable.setText(Boolean.toString(rndGenerator.allCitiesReachable().size()==0));
    }

    private void initRandomGeneratorConfig(RandomMapGeneratorConfig config, boolean isAutoGenerated) {
        checkBoxIsAutoGenerated.setSelected(isAutoGenerated);
        if (config == null) {
            textFieldCityC.clear();
            textFieldMaximumCitiesPerPlayer.clear();
            textFieldCityY0.clear();
            textFieldCityX1.clear();
            textFieldCityY1.clear();

            textFieldMountainProbability.clear();
            textFieldMGY0.clear();
            textFieldMGX1.clear();
            textFieldMGY1.clear();
        } else {
            textFieldCityC.setText(Double.toString(config.getCityC()));
            textFieldMaximumCitiesPerPlayer.setText(Integer.toString(config.getMaxCitiesPerPlayer()));
            textFieldCityY0.setText(Double.toString(config.getCityY0()));
            textFieldCityX1.setText(Double.toString(config.getCityX1()));
            textFieldCityY1.setText(Double.toString(config.getCityY1()));

            textFieldMountainProbability.setText(Double.toString(config.getMountainProbability()));
            textFieldMGY0.setText(Double.toString(config.getMountainY0()));
            textFieldMGX1.setText(Double.toString(config.getMountainX1()));
            textFieldMGY1.setText(Double.toString(config.getMountainY1()));
        }
    }

    public void applyMapSize() {
        try {
            var width = Integer.parseInt(mapWidthTextField.getText());
            var height = Integer.parseInt(mapHeightTextField.getText());
            if (width > 0 && height > 0) {
                if (width < configuration.getMapSize().getWidth() || height < configuration.getMapSize().getHeight()) {
                    List<Position> toRemove = new ArrayList<>();
                    for (var pos : startingPositions) {
                        if (pos.getX() > width - 1 || pos.getY() > height - 1)
                            toRemove.add(pos);
                    }
                    toRemove.forEach(this::removeStartingPos);
                }
                updateMapSizeSubject.onNext(new Size(width, height));
            }
        } catch (NumberFormatException e) {
            UI.showAlertAndWait(null,"Error while applying map size!", "Invalid map size", Alert.AlertType.ERROR);
        }

    }

    public EmpireTerrainType getSelectedTerrainType() {
        var terrainSprite = terrainTypeListView.getSelectionModel().getSelectedItem();
        return terrainSprite != null ? terrainSprite.getTerrainType() : null;
    }

    public void addStartingPos() {
        UI.showAlertAndWait(null,"How to add starting positions?", "Hold CTRL while left clicking on a city on the map.", Alert.AlertType.INFORMATION);
    }

    public void addStartingPos(Position pos) {
        configuration.getStartingCities().add(pos);
        startingPositions.add(pos);
    }

    public void removeStartingPos() {
        var pos = startingPositionsListView.getSelectionModel().getSelectedItem();
        removeStartingPos(pos);
    }

    public void removeStartingPos(Position pos) {
        if (pos == null) return;
        configuration.getStartingCities().remove(pos);
        startingPositions.remove(pos);
    }

    public void startingPosUp() {
        var pos = startingPositionsListView.getSelectionModel().getSelectedItem();
        if (pos == null) return;
        var i = startingPositions.indexOf(pos);
        if (i > 0) {
            Util.swap(startingPositions, i, i - 1);
            Util.swap(configuration.getStartingCities(), i, i - 1);
            startingPositionsListView.getSelectionModel().select(i - 1);
        }
    }

    public void startingPosDown() {
        var pos = startingPositionsListView.getSelectionModel().getSelectedItem();
        if (pos == null) return;
        var i = startingPositions.indexOf(pos);
        if (i < (startingPositions.size() - 1)) {
            Util.swap(startingPositions, i + 1, i);
            Util.swap(configuration.getStartingCities(), i + 1, i);
            startingPositionsListView.getSelectionModel().select(i + 1);
        }
    }

    private void setupControls() {
        terrainTypeListView.setItems(terrainSprites);
        startingPositionsListView.setItems(startingPositions);
        mapWidthTextField.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
        mapHeightTextField.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
    }

    public void generateRandomMap() {
        int citiesPerPlayer;
        double mountainProbability;

        double cityC;
        double cityY0;
        double cityX1;
        double cityY1;

        double mGY0;
        double mGX1;
        double mGY1;

        try {
            citiesPerPlayer = Integer.parseInt(textFieldMaximumCitiesPerPlayer.getText());
            mountainProbability = Double.parseDouble(textFieldMountainProbability.getText());

            cityC = Double.parseDouble(textFieldCityC.getText());
            cityY0 = Double.parseDouble(textFieldCityY0.getText());
            cityX1 = Double.parseDouble(textFieldCityX1.getText());
            cityY1 = Double.parseDouble(textFieldCityY1.getText());

            mGY0 = Double.parseDouble(textFieldMGY0.getText());
            mGX1 = Double.parseDouble(textFieldMGX1.getText());
            mGY1 = Double.parseDouble(textFieldMGY1.getText());
        } catch (NumberFormatException e) {
            UI.showAlertAndWait(null,"Invalid parameters!", "Invalid parameters!. Parameters have to be numerical!", Alert.AlertType.ERROR);
            return;
        }
        var newConfig = new RandomMapGeneratorConfig(citiesPerPlayer, cityX1, cityY0, cityY1, cityC, mountainProbability, mGX1, mGY0, mGY1);
        try {
            RandomMapGenerator.validateGeneratorConfig(newConfig, configuration.getMapSize());
        } catch (EmpireRandomMapGeneratorException e) {
            UI.showAlertAndWait(null,"Invalid parameters!", e.getMessage(), Alert.AlertType.ERROR);
            return;
        }

        configuration.setGeneratorConfig(newConfig);
        configuration.setHasAutoGeneratedMap(checkBoxIsAutoGenerated.isSelected());

        var generatedData = rndGenerator.generateMap(checkBoxMakeCitiesReachable.isSelected());
        labelAllCitiesReachable.setText(generatedData.getB().toString());
        updateMapSubject.onNext(generatedData.getA());
    }

}
