package at.ac.tuwien.ifs.sge.game.empire.configuration;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInvalidConfigurationException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.map.Size;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.visualization.*;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXConfigEditor;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.*;
import java.util.Map;
import java.util.Objects;

public class EmpireConfigEditor implements JavaFXConfigEditor {


    private static final int TILE_SIZE = 48;
    private static final int MIN_TILE_SIZE = 16;
    private static final int MAX_TILE_SIZE = 64;
    private static final Color HIGHLIGHT_TILE_PAINT = new Color(1, 1, 1, 0.2);
    private static final String STARTING_CITY_INDICATOR_SPRITE_SRC = "at/ac/tuwien/ifs/sge/game/empire/visualization/sprites/starting_city.png";

    private static final int SIDEBAR_WIDTH = 300;

    private static final double FRAMES_PER_SECOND = 60;

    private final Logger log;
    private Timeline visualizationTimeline;

    private double width;
    private double height;

    private EmpireConfiguration configuration;
    private Map<String, EmpireTerrainSprite> terrainSpritesByType;
    private EmpireTerrain[][] empireTiles;
    private Frame startingCityIndicatorFrame;

    private Camera camera;
    private MouseNavigation mouseNavigation;

    private Canvas background;
    private Pane gamePane;
    private HBox editorView;

    private EditorSidebarViewController editorSidebarViewController;
    private VBox editorSidebarViewRoot;

    private CompositeDisposable subscriptions = new CompositeDisposable();

    public EmpireConfigEditor(GameConfiguration initialConfiguration, Logger log) {
        this.log = log;
        try {
            configuration = initialConfiguration == null ? Empire.loadAndValidateDefaultConfig() : (EmpireConfiguration) initialConfiguration;

            visualizationTimeline = new Timeline(
                    new KeyFrame(
                            Duration.millis(1000 / FRAMES_PER_SECOND),
                            event -> redraw()));
            visualizationTimeline.setCycleCount(Timeline.INDEFINITE);

            FXMLLoader editorSidebarViewLoader = new FXMLLoader(EditorSidebarViewController.class.getResource("editor_sidebar_view.fxml"));
            editorSidebarViewLoader.setClassLoader(getClass().getClassLoader());
            editorSidebarViewRoot = editorSidebarViewLoader.load();
            editorSidebarViewController = editorSidebarViewLoader.getController();
            subscriptions.add(editorSidebarViewController.updateMapSizeSubject.subscribe(this::updateMapSize));
            subscriptions.add(editorSidebarViewController.updateMapSubject.subscribe(this::updateMapTiles));

            loadStartingCitySprite();

        } catch (EmpireInvalidConfigurationException e) {
            log.printStackTrace(e);
            log.error("Could not load empire default configuration!");
        } catch (IOException e) {
            log.printStackTrace(e);
            log.error("Could not load editor sidebar view!");
        }
    }

    @Override
    public void initialize(double width, double height) {
        this.width = width;
        this.height = height;

        createEditorViewNodes();

        updateConfig(configuration);

        visualizationTimeline.play();
    }

    @Override
    public Node getNode() {
        return editorView;
    }

    @Override
    public GameConfiguration getConfiguration() {
        configuration.setMap(empireTiles);
        return configuration;
    }

    @Override
    public void openFile(File file) throws FileNotFoundException {
        try {
            EmpireConfiguration configuration = EmpireConfiguration.getYaml().load(new FileInputStream(file));
            configuration.validateConfiguration();
            updateConfig(configuration);
        } catch (EmpireInvalidConfigurationException e) {
            UI.showAlertAndWait(null,"Error while loading config file!", "Configuration is invalid: " + e.getMessage(), Alert.AlertType.ERROR);
        }
    }

    @Override
    public void saveFile(File file) throws IOException {
        configuration.setMap(empireTiles);
        var writer = new FileWriter(file);
        EmpireConfiguration.getYaml().dump(configuration, writer);
    }

    @Override
    public void updateViewSize(double width, double height) {
        camera.updateViewSize(width - SIDEBAR_WIDTH, height);
        background.setWidth(camera.getViewWidth());
        background.setHeight(camera.getViewHeight());
        redraw();
    }

    @Override
    public void dispose() {
        visualizationTimeline.stop();
        subscriptions.dispose();
    }

    private void updateConfig(EmpireConfiguration configuration) {
        this.configuration = configuration;

        empireTiles = configuration.getEmpireTiles();

        if (camera == null) {
            camera = new Camera(empireTiles, configuration.getMapSize(), TILE_SIZE, MIN_TILE_SIZE, MAX_TILE_SIZE, width - SIDEBAR_WIDTH, height, null);
            registerNavigationEvents();
        } else
            camera.updateMap(empireTiles, configuration.getMapSize(), null);

        loadSprites();

        editorSidebarViewController.setup(configuration, terrainSpritesByType.values());

        redraw();
    }

    private void updateMapTiles(char [][] map)
    {
        configuration.setMap(map);
        empireTiles = configuration.getEmpireTiles();
        camera.updateMap(empireTiles, configuration.getMapSize(), null);
        redraw();
    }

    private void updateMapSize(Size mapSize) {
        var oldSize = configuration.getMapSize();
        configuration.setMapSize(mapSize);

        var newEmpireTiles = new EmpireTerrain[mapSize.getHeight()][mapSize.getWidth()];
        for (var y = 0; y < mapSize.getHeight(); y++) {
            for (var x = 0; x < mapSize.getWidth(); x++) {
                if (y >= oldSize.getHeight() || x >= oldSize.getWidth()) {
                    newEmpireTiles[y][x] = new EmpireTerrain(configuration.getTerrainTypes().get(0), new Position(x, y));
                } else newEmpireTiles[y][x] = empireTiles[y][x];

            }
        }
        empireTiles = newEmpireTiles;
        camera.updateMap(empireTiles, mapSize, null);
        redraw();
    }

    private void redraw() {
        var bgGc = background.getGraphicsContext2D();
        bgGc.clearRect(0, 0, camera.getViewWidth(), camera.getViewHeight());

        var tiles = camera.getTerrainInView();
        if (tiles.length == 0 || tiles[0].length == 0) return;

        var firstTilePos = tiles[0][0].getPosition();
        var firstTileViewPos = camera.viewPositionFromTilePosition(firstTilePos);
        var xView = firstTileViewPos.getX();
        var yView = firstTileViewPos.getY();

        var mousePosition = mouseNavigation.getHoverPosition();
        var leftMouseButtonPressed = mouseNavigation.isPressed(MouseButton.PRIMARY);
        var ctrlPressed = mouseNavigation.isControlDown();
        var currentBrushTile = editorSidebarViewController.getSelectedTerrainType();
        var startingCities = configuration.getStartingCities();

        var zoom = camera.getZoom();
        var tileSize = TILE_SIZE * zoom;

        for (var y = 0; y < tiles.length; y++) {
            for (var x = 0; x < tiles[0].length; x++) {
                var tile = tiles[y][x];
                var xUpLeft = xView + x * tileSize;
                var yUpLeft = yView + y * tileSize;

                var mouseInsideTile = isInsideTile(mousePosition, xUpLeft, yUpLeft, tileSize);
                var isStartingCity = tile.getMapIdentifier() == 'c' && startingCities.contains(tile.getPosition());
                var shouldReplaceTile = leftMouseButtonPressed
                        && mouseInsideTile
                        && currentBrushTile != null
                        && tile.getMapIdentifier() != currentBrushTile.getMapIdentifier()
                        && !ctrlPressed
                        && !isStartingCity;

                if (shouldReplaceTile) {
                    var tilePos = tile.getPosition();
                    tile = new EmpireTerrain(currentBrushTile, new Position(tilePos));
                    empireTiles[tilePos.getY()][tilePos.getX()] = tile;
                }

                var terrainSprite = terrainSpritesByType.get(tile.getName());
                var xPos = xUpLeft + tileSize / 2;
                var yPos = yUpLeft + tileSize / 2;
                terrainSprite.draw(bgGc, xPos, yPos, zoom);

                if (tile.getMapIdentifier() == 'c' && startingCities.contains(tile.getPosition())) {
                    startingCityIndicatorFrame.draw(bgGc, xPos, yPos, zoom, false);
                }

                if (mouseInsideTile) {
                    bgGc.setFill(HIGHLIGHT_TILE_PAINT);
                    bgGc.fillRect(xUpLeft - 1, yUpLeft - 1, tileSize, tileSize);
                }
            }
        }
    }

    private boolean isInsideTile(Point2D point, double xTile, double yTile, double tileSize) {
        var x = point.getX();
        var y = point.getY();
        var x2Tile = xTile + tileSize;
        var y2Tile = yTile + tileSize;
        return x > xTile && y > yTile && x < x2Tile && y < y2Tile;
    }

    private void loadSprites() {
        try {
            terrainSpritesByType = EmpireTerrainSprite.fromConfig(configuration, TILE_SIZE);
        } catch (Exception e) {
            log.error("Could not load sprite:");
            log.printStackTrace(e);
        }
    }

    private void loadStartingCitySprite() {
        var url = Objects.requireNonNull(EmpireVisualization.class.getClassLoader().getResource(STARTING_CITY_INDICATOR_SPRITE_SRC)).toExternalForm();
        var image = new Image(url);
        startingCityIndicatorFrame = new Frame(image, new Point2D(image.getWidth() / 2, image.getHeight() / 2));
    }

    private void createEditorViewNodes() {
        background = new Canvas(width - SIDEBAR_WIDTH, height);
        gamePane = new Pane(background);
        editorView = new HBox(editorSidebarViewRoot, gamePane);
    }

    private void registerNavigationEvents() {
        mouseNavigation = new MouseNavigation(camera, gamePane);
        mouseNavigation.registerDragMove();
        mouseNavigation.registerScrollZoom();
        mouseNavigation.registerHoverCoordinates();
        mouseNavigation.registerButtonPressed(MouseButton.PRIMARY);
        mouseNavigation.registerOnMouseClicked(MouseButton.PRIMARY, this::onLeftClick);
    }

    private void onLeftClick(MouseEvent event) {
        if (event.isControlDown()) {
            var mapPos = camera.mapPositionFromViewPosition(event.getX(), event.getY());
            var tileX = camera.getTileIndex(mapPos.getX());
            var tileY = camera.getTileIndex(mapPos.getY());
            var mapSize = configuration.getMapSize();
            if (tileX < 0 || tileY < 0 || tileX >= mapSize.getWidth() || tileY >= mapSize.getHeight())
                return;
            var tile = empireTiles[tileY][tileX];
            var pos = new Position(tileX, tileY);
            if (tile.getMapIdentifier() == 'c') {
                if (configuration.getStartingCities().contains(pos))
                    editorSidebarViewController.removeStartingPos(pos);
                else
                    editorSidebarViewController.addStartingPos(pos);
            }
        }
    }
}
