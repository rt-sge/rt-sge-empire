package at.ac.tuwien.ifs.sge.game.empire.configuration;

import at.ac.tuwien.ifs.sge.core.util.pair.ImmutablePair;
import at.ac.tuwien.ifs.sge.core.util.pair.Pair;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireRandomMapGeneratorException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.map.Size;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.RandomMapGeneratorConfig;


import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class RandomMapGenerator {

    private final EmpireConfiguration configuration;


    private HashMap<Position, ArrayList<Position>> citiesByPlayers;
    private ArrayList<Position> positions;

    private char[][] map;

    public RandomMapGenerator(EmpireConfiguration configuration) {
        this.configuration = configuration;
        init();
    }

    private void init() {
        citiesByPlayers = new HashMap<>();
        if (configuration.getMap() != null) {
            map = Arrays.stream(configuration.getMap()).map(char[]::clone).toArray(char[][]::new);
            configuration.getStartingCities().forEach(c -> {
                citiesByPlayers.put(c, new ArrayList<>(List.of(c)));
                map[c.getY()][c.getX()] = 'c';
            });
        } else {
            clearMap();
        }
    }

    private void clearMap() {
        map = new char[configuration.getMapSize().getHeight()][];
        for (int i = 0; i < configuration.getMapSize().getHeight(); i++) {
            map[i] = new char[configuration.getMapSize().getWidth()];
        }
    }

    /**
     * Generates a map with the specified configuration.
     *
     * @param makeAllCitiesReachable if set the methods modifies the map so that all cities are reachable.
     * @return a pair of the map in char[][] format and a boolean which specifies if all cities are reachable.
     */
    public Pair<char[][], Boolean> generateMap(boolean makeAllCitiesReachable) {
        clearMap();
        citiesByPlayers.clear();
        configuration.getStartingCities().forEach(c -> {
            citiesByPlayers.put(c, new ArrayList<>(List.of(c)));
            map[c.getY()][c.getX()] = 'c';
        });
        positions = new ArrayList<>();

        for (int y = 0; y < configuration.getMapSize().getHeight(); y++) {
            for (int x = 0; x < configuration.getMapSize().getWidth(); x++) {
                if (!configuration.getStartingCities().contains(new Position(x, y))) {
                    positions.add(new Position(x, y));
                }
            }
        }

        while (!positions.isEmpty()) {

            var index = ThreadLocalRandom.current().nextInt(positions.size());
            var pos = positions.get(index);
            var result = setTileIdentifierForPosition(pos);

            if (result == 'm') {
                do {
                    result = growMountain(pos);
                }
                while (result == 'm');
            }
        }
        balanceCities();
        if (makeAllCitiesReachable)
            makeAllCitiesReachable();
        var reachable = allCitiesReachable().size() == 0;

        return new ImmutablePair<>(map, reachable);
    }

    private char setTileIdentifierForPosition(Position pos) {
        var generatorConfig = configuration.getGeneratorConfig();
        positions.remove(pos);
        var chainSize = getRegionSize(pos, new HashSet<>(), 'm');
        var unmovableProb = mountainProbability(chainSize);

        var nearestPlayer = getShortestBaseDistance(pos);

        double cityProb = 0;
        if (getRegionSize(pos, new HashSet<>(), 'c') == 0 && citiesByPlayers.get(nearestPlayer.getA()).size() < generatorConfig.getMaxCitiesPerPlayer())
            cityProb = cityProbability(nearestPlayer.getB());

        var probSum = cityProb + unmovableProb;
        var value = ThreadLocalRandom.current().nextDouble(probSum > 1 ? probSum : 1);
        char mapIdentifier = '\0';
        if (value < unmovableProb)
            mapIdentifier = 'm';
        else if (value < unmovableProb + cityProb) {
            mapIdentifier = 'c';
            citiesByPlayers.get(nearestPlayer.getA()).add(pos);

        } else
            mapIdentifier = 'g';
        map[pos.getY()][pos.getX()] = mapIdentifier;
        return mapIdentifier;
    }

    private void balanceCities() {
        while (!citiesByPlayers.values().stream().allMatch(l -> l.size() == citiesByPlayers.values().stream().findAny().orElse(null).size())) {
            var maxPosition = Collections.max(citiesByPlayers.entrySet(), Comparator.comparingInt(entry -> entry.getValue().size())).getKey();

            var playerCityList = citiesByPlayers.get(maxPosition);
            var rndValue = ThreadLocalRandom.current().nextInt(1, playerCityList.size());

            var positionToRemove = playerCityList.get(rndValue);
            map[positionToRemove.getY()][positionToRemove.getX()] = 'g';
            playerCityList.remove(rndValue);
        }
    }

    private void makeAllCitiesReachable() {
        var reachableCities = new ArrayList<>(citiesByPlayers.values().stream().flatMap(List::stream).toList());
        var unreachableCities = allCitiesReachable();
        unreachableCities.forEach(reachableCities::remove);

        for (var city : unreachableCities) {

            var newUnreachableCities = allCitiesReachable();
            reachableCities = new ArrayList<>(citiesByPlayers.values().stream().flatMap(List::stream).toList());
            newUnreachableCities.forEach(reachableCities::remove);

            var nearestReachableCity = getNearestReachableCity(reachableCities, city);
            var diff = nearestReachableCity.subtract(city);
            int x = city.getX();
            int y = city.getY();

            int xIndex = 0;
            int yIndex = 0;

            while (xIndex != diff.getX() || yIndex != diff.getY()) {
                if (diff.getX() != xIndex) {
                    if (diff.getX() < 0) {
                        if (xIndex > diff.getX()) {
                            map[y][--x] = 'g';
                            xIndex--;
                        }
                    } else {
                        if (xIndex < diff.getX()) {
                            map[y][++x] = 'g';
                            xIndex++;
                        }
                    }
                } else {
                    if (diff.getY() < 0) {
                        if (yIndex > diff.getY()) {
                            map[--y][x] = 'g';
                            yIndex--;
                        }
                    } else {
                        if (yIndex < diff.getY()) {
                            map[++y][x] = 'g';
                            yIndex++;
                        }
                    }
                }
            }
        }
        balanceCities();
    }

    private Position getNearestReachableCity(List<Position> reachableCities, Position city) {
        double min = Double.MAX_VALUE;
        Position minCity = null;
        for (var reachableCity : reachableCities) {
            var value = getEuclideanDistance(city, reachableCity);
            if (value < min) {
                min = value;
                minCity = reachableCity;
            }
        }
        return minCity;
    }

    private double mountainProbability(int chainSize) {
        // a * e^x*k
        var generatorConfig = configuration.getGeneratorConfig();
        if (chainSize == 0)
            return generatorConfig.getMountainProbability();
        double k = Math.log(generatorConfig.getMountainY1() / generatorConfig.getMountainY0()) / generatorConfig.getMountainX1();

        return generatorConfig.getMountainY0() * Math.exp(k * (chainSize - 1));
    }

    public static void validateGeneratorConfig(RandomMapGeneratorConfig config, Size size) throws EmpireRandomMapGeneratorException {
        // mp + integral(e^(k+x), 0, inf)
        // DO NOT REMOVE THIS COMMENT
        /*
        double k = Math.log(config.getMountainY1() / config.getMountainY0()) / config.getMountainX1();

        // empirical value
        double cap = 100;

        double mapSize = (size.getHeight() + size.getWidth()) / 2d;
        var value = config.getMountainProbability() + (-1 / k) / Math.pow(mapSize, 2);

         */

        //if (config.getMountainProbability() > 0.5)
        //    throw new EmpireRandomMapGeneratorException("Invalid parameter!. Mountain probability can not be greater than 0.5!");

        if (!(config.getCityY0() <= 1 && config.getCityY1() <= 1 && config.getCityC() <= 1 && config.getMountainY0() <= 1 && config.getMountainY1() <= 1 && config.getMountainProbability() <= 1))
            throw new EmpireRandomMapGeneratorException("Probability values shall not be greater than 1!");
    }

    private double cityProbability(double distance) {
        // a * e^(k*x) + c
        var generatorConfig = configuration.getGeneratorConfig();
        double k = Math.log(generatorConfig.getCityY1() / generatorConfig.getCityY0()) / generatorConfig.getCityX1();

        return generatorConfig.getCityY0() * Math.exp(k * distance) + generatorConfig.getCityC();
    }

    private int getRegionSize(Position position, HashSet<Position> visited, char regionIdentifier) {
        int result = 0;

        var left = position.getLeft();
        var top = position.getUpper();
        var right = position.getRight();
        var lower = position.getLower();
        var leftUpper = position.getLeftUpper();
        var rightUpper = position.getRightUpper();
        var rightLower = position.getRightLower();
        var leftLower = position.getLeftLower();

        if (left.getX() >= 0 && regionIdentifier == map[left.getY()][left.getX()] && !visited.contains(left)) {
            visited.add(left);
            result += 1 + getRegionSize(left, visited, regionIdentifier);
        }
        if (top.getY() >= 0 && regionIdentifier == map[top.getY()][top.getX()] && !visited.contains(top)) {
            visited.add(top);
            result += 1 + getRegionSize(top, visited, regionIdentifier);
        }
        if (right.getX() < configuration.getMapSize().getWidth() && regionIdentifier == map[right.getY()][right.getX()] && !visited.contains(right)) {
            visited.add(right);
            result += 1 + getRegionSize(right, visited, regionIdentifier);
        }
        if (lower.getY() < configuration.getMapSize().getHeight() && regionIdentifier == map[lower.getY()][lower.getX()] && !visited.contains(lower)) {
            visited.add(lower);
            result += 1 + getRegionSize(lower, visited, regionIdentifier);
        }
        if (leftLower.getX() >= 0 && leftLower.getY() < configuration.getMapSize().getHeight() && regionIdentifier == map[leftLower.getY()][leftLower.getX()] && !visited.contains(leftLower)) {
            visited.add(leftLower);
            result += 1 + getRegionSize(leftLower, visited, regionIdentifier);
        }
        if (rightUpper.getX() < configuration.getMapSize().getWidth() && rightUpper.getY() >= 0 && regionIdentifier == map[rightUpper.getY()][rightUpper.getX()] && !visited.contains(rightUpper)) {
            visited.add(rightUpper);
            result += 1 + getRegionSize(rightUpper, visited, regionIdentifier);
        }
        if (rightLower.getX() < configuration.getMapSize().getWidth() && rightLower.getY() < configuration.getMapSize().getHeight() && regionIdentifier == map[rightLower.getY()][rightLower.getX()] && !visited.contains(rightLower)) {
            visited.add(rightLower);
            result += 1 + getRegionSize(rightLower, visited, regionIdentifier);
        }
        if (leftUpper.getY() >= 0 && leftUpper.getX() >= 0 && regionIdentifier == map[leftUpper.getY()][leftUpper.getX()] && !visited.contains(leftUpper)) {
            visited.add(leftUpper);
            result += 1 + getRegionSize(leftUpper, visited, regionIdentifier);
        }
        return result;
    }

    private Pair<Position, Double> getShortestBaseDistance(Position position) {
        var startingCities = configuration.getStartingCities();
        double min = Integer.MAX_VALUE;
        Position minPos = null;
        for (var city : startingCities) {
            var value = getEuclideanDistance(position, city);
            if (value < min) {
                min = value;
                minPos = city;
            }
        }
        return new ImmutablePair<>(minPos, min);
    }

    private char growMountain(Position origin) {

        var possibleNeighbours = new ArrayList<Position>();
        possibleNeighbours = origin.getAllNeighbours();
        possibleNeighbours.removeIf(n -> !positions.contains(n));

        Position position;
        if (possibleNeighbours.size() == 0)
            return '\0';
        if (possibleNeighbours.size() == 1)
            position = possibleNeighbours.get(0);
        else {
            var rndValue = ThreadLocalRandom.current().nextInt(0, possibleNeighbours.size());
            position = positions.get(rndValue);
        }
        return setTileIdentifierForPosition(position);

    }

    public List<Position> allCitiesReachable() {

        var unreachedCities = citiesByPlayers.values().stream().flatMap(List::stream).collect(Collectors.toList());

        HashMap<Position, Vertex> graphByPosition = new HashMap<>();

        for (int y = 0; y < configuration.getMapSize().getHeight(); y++) {
            for (int x = 0; x < configuration.getMapSize().getWidth(); x++) {
                if (map[y][x] == 'm')
                    continue;
                var pos = new Position(x, y);
                graphByPosition.put(pos, new Vertex(pos));
            }
        }
        for (int y = 0; y < configuration.getMapSize().getHeight(); y++) {
            for (int x = 0; x < configuration.getMapSize().getWidth(); x++) {
                var mapIdentifier = map[y][x];
                if (mapIdentifier == 'g' || mapIdentifier == 'c') {
                    var pos = new Position(x, y);
                    var neighbours = pos.getAllNeighbours();
                    neighbours.removeIf(p -> p.getX() < 0 || p.getY() < 0 || p.getX() >= configuration.getMapSize().getWidth() || p.getY() >= configuration.getMapSize().getHeight());
                    neighbours.removeIf(p -> map[p.getY()][p.getX()] == 'm');
                    Map<Vertex, Edge> map = new HashMap<>();
                    neighbours.forEach(n -> map.put(graphByPosition.get(n), new Edge(Math.sqrt(Math.pow(pos.getX() - n.getX(), 2) + Math.pow(pos.getY() - n.getY(), 2)))));
                    graphByPosition.get(pos).setEdges(map);
                }
            }
        }
        var graph = graphByPosition.values().stream().toList();
        if (graph.size() > 0) {
            graph.get(0).setVisited(true);
        }
        boolean terminate = false;
        while (isDisconnected(graph)) {

            Edge nextMinimum = new Edge(Integer.MAX_VALUE);
            Vertex nextVertex = graph.get(0);
            for (Vertex vertex : graph) {
                if (vertex.isVisited()) {
                    Pair<Vertex, Edge> candidate = vertex.nextMinimum();
                    if (candidate.getB().getWeight() < nextMinimum.getWeight()) {
                        nextMinimum = candidate.getB();
                        nextVertex = candidate.getA();
                    }
                }
            }
            if (terminate)
                break;
            terminate = nextVertex.isVisited();

            nextMinimum.setIncluded(true);
            nextVertex.setVisited(true);
            unreachedCities.remove(nextVertex.getPosition());
        }
        return unreachedCities;
    }


    private boolean isDisconnected(List<Vertex> graph) {
        for (Vertex vertex : graph) {
            if (!vertex.isVisited()) {
                return true;
            }
        }
        return false;
    }

    private double getEuclideanDistance(Position a, Position b) {
        var diff = a.subtract(b);
        return Math.sqrt(diff.getX() * diff.getX() + diff.getY() * diff.getY());
    }

    public RandomMapGeneratorConfig getGeneratorConfig() {
        return configuration.getGeneratorConfig();
    }

    public void setGeneratorConfig(RandomMapGeneratorConfig generatorConfig) {
        this.configuration.setGeneratorConfig(generatorConfig);
    }
}

class Edge {
    private double weight;
    private boolean isIncluded = false;

    public Edge(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isIncluded() {
        return isIncluded;
    }

    public void setIncluded(boolean included) {
        isIncluded = included;
    }
}

class Vertex {
    private Map<Vertex, Edge> edges = new HashMap<>();
    private boolean isVisited = false;
    private Position position;

    public Vertex(Position position) {
        this.position = position;
    }

    public Pair<Vertex, Edge> nextMinimum() {
        Edge nextMinimum = new Edge(Integer.MAX_VALUE);
        Vertex nextVertex = this;
        for (Map.Entry<Vertex, Edge> pair : edges.entrySet()) {
            if (!pair.getKey().isVisited()) {
                if (!pair.getValue().isIncluded()) {
                    if (pair.getValue().getWeight() < nextMinimum.getWeight()) {
                        nextMinimum = pair.getValue();
                        nextVertex = pair.getKey();
                    }
                }
            }
        }
        return new ImmutablePair<>(nextVertex, nextMinimum);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Map<Vertex, Edge> getEdges() {
        return edges;
    }

    public void setEdges(Map<Vertex, Edge> edges) {
        this.edges = edges;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }
}
