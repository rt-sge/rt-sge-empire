package at.ac.tuwien.ifs.sge.game.empire.core;

import static at.ac.tuwien.ifs.sge.game.empire.util.EmpireUtil.streamToString;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.ActionScheduler;
import at.ac.tuwien.ifs.sge.core.game.FileGameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.GameClock;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.InvalidGameActionUpdate;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.game.TimedActionRecord;
import at.ac.tuwien.ifs.sge.core.game.VisualizationType;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.VisionUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.EmpireAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitAppearedAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitDamagedAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitStateAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitVanishedAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.*;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.CombatStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.MovementStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.ProductionStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.configuration.EmpireConfigEditor;
import at.ac.tuwien.ifs.sge.game.empire.configuration.RandomMapGenerator;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireCombatException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInvalidConfigurationException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInvalidGameEventException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMovementException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireProductionException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.map.VisionTileChanges;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import at.ac.tuwien.ifs.sge.game.empire.validation.EmpireCombatValidator;
import at.ac.tuwien.ifs.sge.game.empire.validation.EmpireMovementValidator;
import at.ac.tuwien.ifs.sge.game.empire.validation.EmpireProductionValidator;
import at.ac.tuwien.ifs.sge.game.empire.validation.EmpireValidator;
import at.ac.tuwien.ifs.sge.game.empire.visualization.EmpireVisualization;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Empire implements RealTimeGame<EmpireEvent, EmpireMap> {

    private static final int NR_OF_THREADS = 1;
    public static final int EMPTY_ID = -1;

    public static final boolean AUTO_REQUEUE_PRODUCTION = false;
    public static final boolean AUTO_REQUEUE_MOVEMENT = false;

    public static EmpireConfiguration loadAndValidateDefaultConfig() throws IOException, EmpireInvalidConfigurationException {
        var defaultConfig = streamToString(Objects.requireNonNull(Empire.class.getResourceAsStream("default_config_2p.yaml")));
        EmpireConfiguration config = EmpireConfiguration.getYaml().load(defaultConfig);
        config.validateConfiguration();
        return config;
    }

    private final Object gameOverLock = new Object();
    private final AtomicBoolean gameOver = new AtomicBoolean(false);
    private int winner = EMPTY_ID;

    private EmpireLogger logger;
    private GameClock gameClock;
    private final AtomicBoolean running = new AtomicBoolean(false);

    private List<TimedActionRecord<EmpireEvent>> eventRecord;

    private ActionScheduler<Empire, EmpireEvent> actionScheduler;

    private EmpireConfiguration empireConfiguration;
    private List<Integer> players;
    private EmpireMap empireMap;

    private ScheduledExecutorService pool;

    private Map<UUID, EmpireUnit> unitsById;
    private Map<Integer, List<EmpireUnit>> unitsByPlayer;
    private Map<Position, EmpireCity> citiesByPosition;
    private Map<Integer, EmpireUnitType> unitTypesById;

    private EmpireCombatValidator combatValidator;
    private EmpireProductionValidator productionValidator;
    private EmpireMovementValidator movementValidator;

    public HashMap<Class, EmpireValidator> applyActionValidationMap = new HashMap<>();


    public Empire(Empire empire) {
        empireConfiguration = empire.empireConfiguration;
        this.unitsById = new HashMap<>();
        this.unitsByPlayer = new HashMap<>();
        empire.unitsByPlayer.forEach(
            (playerId, units) -> this.unitsByPlayer.put(playerId, new ArrayList<>()));

        this.citiesByPosition = new HashMap<>();

        this.unitTypesById = empire.unitTypesById;

        this.gameClock = new GameClock(empire.getGameClock());
        this.actionScheduler = new ActionScheduler<>(this, empire.actionScheduler);

        this.eventRecord = new ArrayList<>(empire.eventRecord);
        this.players = new ArrayList<>(empire.players);
        this.empireMap = new EmpireMap(empire.empireMap, citiesByPosition, unitsById, unitsByPlayer, getNumberOfPlayers());

        this.logger = new EmpireLogger(empire.logger.LOGGER(), unitsById, citiesByPosition, unitTypesById, empireMap);

        this.combatValidator = new EmpireCombatValidator(unitsById, empireMap, unitTypesById, players);
        this.productionValidator = new EmpireProductionValidator(empireConfiguration.getUnitCap(), citiesByPosition, unitsByPlayer, empireMap, unitTypesById, players);
        this.movementValidator = new EmpireMovementValidator(unitsById, empireMap, unitTypesById, players);

        initValidationMap();
    }

    public Empire(GameConfiguration configuration, Logger logger) {
        this(configuration, 2, logger);
    }

    public Empire(GameConfiguration configuration, int numberOfPlayers, Logger logger) {
        if (configuration instanceof FileGameConfiguration fileConfig) {
            try {
                this.empireConfiguration = EmpireConfiguration.getYaml().load(new FileInputStream(fileConfig.getFile()));
                this.empireConfiguration.validateConfiguration();
                logger.info("Successfully loaded config");
            } catch (FileNotFoundException | EmpireInvalidConfigurationException e) {
                try {
                    logger.error("Failed to load specified config");
                    logger.error(e.getMessage());
                    this.empireConfiguration = loadAndValidateDefaultConfig();
                } catch (IOException | EmpireInvalidConfigurationException e2) {
                    logger.error("Failed to load default config!");
                    logger.error(e2.getMessage());
                    return;
                }
            }
        } else if (!(configuration instanceof EmpireConfiguration)) {
            try {
                logger.info("No configuration given, loading default config...");
                this.empireConfiguration = loadAndValidateDefaultConfig();
            } catch (IOException | EmpireInvalidConfigurationException e) {
                logger.error("Failed to load default config!");
                logger.error(e.getMessage());
                return;
            }
        } else {
            this.empireConfiguration = (EmpireConfiguration) configuration;
            try {
                this.empireConfiguration.validateConfiguration();
            } catch (EmpireInvalidConfigurationException e) {
                logger.error("Failed to load config!");
                logger.error(e.getMessage());
                return;
            }
        }

        if(!empireConfiguration.isConfigForPlayer() && empireConfiguration.getHasAutoGeneratedMap()){
            RandomMapGenerator rnd = new RandomMapGenerator(empireConfiguration);
            empireConfiguration.setMap(rnd.generateMap(true).getA());
        }

        this.eventRecord = new ArrayList<>();

        if (!empireConfiguration.isConfigForPlayer()
            && numberOfPlayers > this.empireConfiguration.getStartingCities().size()) {
            logger.error("The Empire configuration declares a maximum of "
                + this.empireConfiguration.getStartingCities().size() + " players but "
                + numberOfPlayers + " are connected");
            return;
        }
        this.gameClock = new GameClock();
        this.players = IntStream.rangeClosed(0, numberOfPlayers - 1).boxed()
            .collect(Collectors.toList());
        unitTypesById = empireConfiguration.getUnitTypes().stream().collect(
            Collectors.toConcurrentMap(EmpireUnitType::getUnitTypeId, unitType -> unitType));
        unitsById = new HashMap<>();
        unitsByPlayer = new HashMap<>();
        players.forEach(p -> unitsByPlayer.put(p, new ArrayList<>()));
        citiesByPosition = new HashMap<>();

        empireMap = new EmpireMap(empireConfiguration.getEmpireTiles(), citiesByPosition, unitsById,
            unitsByPlayer, numberOfPlayers);

        this.logger = new EmpireLogger(logger, unitsById, citiesByPosition, unitTypesById,
            empireMap);

        initThreads();

        combatValidator = new EmpireCombatValidator(unitsById, empireMap, unitTypesById, players);
        productionValidator = new EmpireProductionValidator(empireConfiguration.getUnitCap(), citiesByPosition, unitsByPlayer, empireMap, unitTypesById, players);
        movementValidator = new EmpireMovementValidator(unitsById, empireMap, unitTypesById, players);

        initValidationMap();
    }

    public void initValidationMap() {
        applyActionValidationMap = new HashMap<>();
        applyActionValidationMap.put(CombatStartOrder.class, combatValidator);
        applyActionValidationMap.put(CombatStopOrder.class, combatValidator);
        applyActionValidationMap.put(CombatHitAction.class, combatValidator);
        applyActionValidationMap.put(MovementStartOrder.class, movementValidator);
        applyActionValidationMap.put(MovementStopOrder.class, movementValidator);
        applyActionValidationMap.put(MovementAction.class, movementValidator);
        applyActionValidationMap.put(ProductionStartOrder.class, productionValidator);
        applyActionValidationMap.put(ProductionStopOrder.class, productionValidator);
        applyActionValidationMap.put(ProductionAction.class, productionValidator);
        applyActionValidationMap.put(InitialSpawnAction.class, productionValidator);
    }


    private void initThreads() {
        pool = Executors.newScheduledThreadPool(NR_OF_THREADS);
        actionScheduler = new ActionScheduler<>(this, logger.LOGGER());
    }

    //region Init/Start/Stop
    @Override
    public Map<Integer, List<EmpireEvent>> initialize() {
        logger.logDebug("Empire game initializing.");
        // Spawn starting units
        if (!empireConfiguration.isConfigForPlayer() && empireConfiguration.getStartingUnitUnitType() != -1) {
            for (int i = 0; i < players.size(); i++) {
                var cityPos = empireConfiguration.getStartingCities().get(i);
                var id = UUID.randomUUID();
                try {
                    empireMap.addUnitWithoutUpdatingVision(new EmpireUnit(id, i, unitTypesById.get(empireConfiguration.getStartingUnitUnitType()), cityPos));
                } catch (EmpireMapException | EmpireInternalException e) {
                    e.printStackTrace();
                }
            }
            Map<Integer, List<EmpireEvent>> eventsByPlayer = new HashMap<>();
            players.forEach(p -> eventsByPlayer.put(p, new ArrayList<>()));
            unitsById.values().forEach(u -> {
                try {
                    var updatedTiles = empireMap.updateVisionThroughNewEntity(u);
                    eventsByPlayer.get(u.getPlayerId()).add(new VisionUpdate(u.getPlayerId(), updatedTiles, null));
                } catch (EmpireInternalException | EmpireMapException e) {
                    e.printStackTrace();
                }
            });
            logger.logDebug("Spawned units.");
            return eventsByPlayer;
        }
        return null;
    }

    @Override
    public void start() {
        running.set(true);
        gameClock.start();
        actionScheduler.setExecutorService(pool);
        actionScheduler.start();
        logger.logDebug("Empire game started.");

    }


    @Override
    public void stop() {
        actionScheduler.stop();
        pool.shutdown();
        logger.logDebug("Empire game stopped.");
        running.set(false);
    }

    @Override
    public RealTimeGame<EmpireEvent, EmpireMap> copy() {
        return new Empire(this);
    }


    @Override
    public void advance(long timeSpan) throws ActionException {
        if (!isRunning()) {
            if (gameClock.getStartTimeMs() == 0) {
                if (gameClock.getGameTimeMs() != 0) gameClock.setStartTimeMs(gameClock.getGameTimeMs());
                else {
                    var time = System.currentTimeMillis();
                    gameClock.setStartTimeMs(time);
                    gameClock.setGameTimeMs(time);
                }
            }
            actionScheduler.clearUntil(gameClock.getGameTimeMs());
            var newTime = gameClock.getGameTimeMs() + timeSpan;
            actionScheduler.applyUntil(newTime);
            gameClock.setGameTimeMs(newTime);
        }
    }

    @Override
    public boolean isRunning() {
        return running.get();
    }
    //endregion

    //region Apply Action
    @Override
    public List<GameUpdate<EmpireEvent>> applyActionEvent(GameActionEvent<EmpireEvent> actionEvent) {
        var action = actionEvent.getAction();
        var executionTimeMs = actionEvent.getExecutionTimeMs();
        if (actionEvent.getPlayerId() != GameActionEvent.INTERNAL_EVENT_ID && (action instanceof EmpireAction || action instanceof VisionUpdate)) {
            logger.logError("EmpireAction, EmpireNotification and VisionUpdate are only allowed from internal processes!");
            return List.of();
        } else {
            var verified = verifyGameEvent(actionEvent);
            if (!verified) {
                logger.logError("Couldn't verify event!");
                return List.of(new InvalidGameActionUpdate<>(action, actionEvent.getPlayerId(), executionTimeMs, "Game Action could not be verified!"));
            }
        }

        ActionResult result;
        try {
            result = applyAction(action, executionTimeMs);
        } catch (Exception e) {
            logger.logDebug(e.getMessage());
            //e.printStackTrace();
            var invalidActions = List.<GameUpdate<EmpireEvent>>of();
            if (actionEvent.getPlayerId() != GameActionEvent.INTERNAL_EVENT_ID)
                invalidActions = List.of(new InvalidGameActionUpdate<>(action, actionEvent.getPlayerId(), executionTimeMs, "Game Action is Invalid: " + e.getMessage()));
            return invalidActions;
        }

        List<GameUpdate<EmpireEvent>> updates = new ArrayList<>();
        try {
            updates = action.getUpdates(this, result, executionTimeMs);
        } catch (Exception e) {
            logger.logError(e.getMessage());
            e.printStackTrace();
        }
        return updates;
    }

    private boolean verifyGameEvent(GameActionEvent<EmpireEvent> event) {
        try {
            if (event.getAction() instanceof CombatStartOrder order) {
                if (!combatValidator.verifyCombatStartOrder(order, event.getPlayerId()))
                    throw new EmpireInvalidGameEventException("Couldn't verify combat start order. " + order);
            } else if (event.getAction() instanceof CombatStopOrder order) {
                if (!combatValidator.verifyCombatStopOrder(order, event.getPlayerId()))
                    throw new EmpireInvalidGameEventException("Couldn't verify combat stop order. " + order);
            } else if (event.getAction() instanceof ProductionStartOrder order) {
                if (!productionValidator.verifyProductionStartOrder(order, event.getPlayerId()))
                    throw new EmpireInvalidGameEventException("Couldn't verify production start order. " + order);
            } else if (event.getAction() instanceof ProductionStopOrder order) {
                if (!productionValidator.verifyProductionStopOrder(order, event.getPlayerId()))
                    throw new EmpireInvalidGameEventException("Couldn't verify production stop order. " + order);
            } else if (event.getAction() instanceof MovementStartOrder order) {
                if (!movementValidator.verifyMovementStartOrder(order, event.getPlayerId()))
                    throw new EmpireInvalidGameEventException("Couldn't verify movement start order. " + order);
            } else if (event.getAction() instanceof MovementStopOrder order) {
                if (!movementValidator.verifyMovementStopOrder(order, event.getPlayerId()))
                    throw new EmpireInvalidGameEventException("Couldn't verify movement stop order. " + order);
            }
        } catch (Exception e) {
            logger.logError(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public ActionResult applyAction(EmpireEvent action, long executionTimeMs) throws ActionException {

        actionScheduler.clearUntil(executionTimeMs);
        actionScheduler.removeEqualActionAtSameTime(action, executionTimeMs);

        ActionResult result = null;

        try {
            if (applyActionValidationMap.containsKey(action.getClass())) {
                applyActionValidationMap.get(action.getClass()).validate(action);
            }
        } catch (Exception e) {
            throw new ActionException("Action validation failed: " + action, e);
        }

        try {
            if (action instanceof CombatStartOrder order) {
                result = applyCombatStartOrder(order, executionTimeMs);
            } else if (action instanceof CombatStopOrder order) {
                result = applyCombatStopOrder(order);
            } else if (action instanceof CombatHitAction hit) {
                result = applyCombatHitAction(hit, executionTimeMs);
            } else if (action instanceof ProductionStartOrder order) {
                result = applyProductionStartOrder(order, executionTimeMs);
            } else if (action instanceof ProductionStopOrder order) {
                result = applyProductionStopOrder(order);
            } else if (action instanceof ProductionAction production) {
                result = applyProductionAction(production);
            } else if (action instanceof InitialSpawnAction spawnAction) {
                result = applySpawnAction(spawnAction);
            } else if (action instanceof MovementStartOrder order) {
                result = applyMovementStartOrder(order, executionTimeMs);
            } else if (action instanceof MovementStopOrder order) {
                result = applyMovementStopOrder(order);
            } else if (action instanceof MovementAction move) {
                result = applyMovementAction(move, executionTimeMs);
            } else if (action instanceof VisionUpdate update) {
                result = applyVisionUpdate(update);
            } else if (action instanceof UnitAppearedAction appearedAction) {
                result = applyUnitAppearedAction(appearedAction);
            } else if (action instanceof UnitDamagedAction damagedAction) {
                result = applyUnitDamagedAction(damagedAction, executionTimeMs);
            } else if (action instanceof UnitStateAction stateAction) {
                result = applyUnitStateAction(stateAction);
            } else if (action instanceof UnitVanishedAction vanishedAction) {
                result = applyUnitVanishedAction(vanishedAction);
            }
            //logger.logEmpireEvent(action);
        } catch (Exception e) {
            throw new ActionException("Applying action failed: " + action, e);
        }
        eventRecord.add(new TimedActionRecord<>(executionTimeMs, action));
        gameClock.setGameTimeMs(executionTimeMs);
        return result;
    }

    public void clearQueue() {
        this.actionScheduler.getActionEventQueue().clear();
    }

    public void removeCombatHitActionFromQueue(UUID attackerId) {
        var queue = actionScheduler.getActionEventQueue();
        for (var actionEvent : queue) {
            if (actionEvent.getAction() instanceof CombatHitAction combatHitAction) {
                if (combatHitAction.getAttackerId().equals(attackerId)) {
                    queue.remove(actionEvent);
                    return;
                }
            }
        }
    }

    public void removeMovementActionFromQueue(UUID unitId) {
        var queue = actionScheduler.getActionEventQueue();
        for (var actionEvent : queue) {
            if (actionEvent.getAction() instanceof MovementAction movementAction) {
                if (movementAction.getUnitId().equals(unitId)) {
                    queue.remove(actionEvent);
                    return;
                }
            }
        }
    }

    public void removeProductionActionFromQueue(Position cityPosition) {
        var queue = actionScheduler.getActionEventQueue();
        for (var actionEvent : queue) {
            if (actionEvent.getAction() instanceof ProductionAction productionAction) {
                if (productionAction.getCityPosition().equals(cityPosition)) {
                    queue.remove(actionEvent);
                    return;
                }
            }
        }
    }


    // removes all action events that are no longer valid after the specified unit's death and returns a list of units that were attacking the dead unit
    public List<UUID> removeDeathRelatedActionsFromQueue(EmpireUnit deadUnit) {
        var deadUnitId = deadUnit.getId();
        var queue = actionScheduler.getActionEventQueue();
        var unitPos = deadUnit.getPosition();
        boolean lastInCity = getCitiesByPosition().containsKey(unitPos)
                && getCity(unitPos).getState().equals(EmpireProductionState.Producing)
                && getCity(unitPos).getOccupants().size() == 1;
        var affectedAttackers = new ArrayList<UUID>();
        var actionEventsToRemove = new ArrayList<GameActionEvent<EmpireEvent>>();
        for (var actionEvent : queue) {
            var action = actionEvent.getAction();
            if (action instanceof CombatStartOrder startOrder) {
                if (startOrder.getAttackerId().equals(deadUnitId) || startOrder.getTargetId().equals(deadUnitId))
                    actionEventsToRemove.add(actionEvent);
            } else if (action instanceof CombatHitAction hitAction) {
                if (hitAction.getAttackerId().equals(deadUnitId))
                    actionEventsToRemove.add(actionEvent);
                else if (hitAction.getTargetId().equals(deadUnitId)) {
                    actionEventsToRemove.add(actionEvent);
                    affectedAttackers.add(hitAction.getAttackerId());
                }

            } else if (action instanceof CombatStopOrder stopOrder) {
                if (stopOrder.getAttackerId().equals(deadUnitId)) actionEventsToRemove.add(actionEvent);
            } else if (action instanceof MovementStartOrder startOrder) {
                if (startOrder.getUnitId().equals(deadUnitId)) actionEventsToRemove.add(actionEvent);
            } else if (action instanceof MovementAction movementAction) {
                if (movementAction.getUnitId().equals(deadUnitId)) actionEventsToRemove.add(actionEvent);
            } else if (action instanceof MovementStopOrder stopOrder) {
                if (stopOrder.getUnitId().equals(deadUnitId)) actionEventsToRemove.add(actionEvent);
            }
            if (lastInCity) {
                if (action instanceof ProductionAction productionAction) {
                    if (productionAction.getCityPosition().equals(deadUnit.getPosition()))
                        actionEventsToRemove.add(actionEvent);
                }
            }
        }
        for (var actionEvent : actionEventsToRemove) {
            queue.remove(actionEvent);
        }
        return affectedAttackers;
    }

    /**
     * @param attackOrder order to process
     * @return true if the attacking unit is idle. false if the attacking unit is already fighting, therefore that fight will be stopped automatically.
     * In both cases the unit will start attacking
     */
    public ActionResult applyCombatStartOrder(CombatStartOrder attackOrder, long executionTimeMs) {
        var attackerId = attackOrder.getAttackerId();
        var attacker = unitsById.get(attackerId);
        var targetId = attackOrder.getTargetId();
        var target = unitsById.get(targetId);

        if (attacker.isFighting()) {
            removeCombatHitActionFromQueue(attackerId);
        } else if (attacker.isMoving()) {
            removeMovementActionFromQueue(attackerId);
        }

        attackUnit(attacker, targetId, executionTimeMs);

        if (target.isIdle()) {
            var defendOrder = new CombatStartOrder(target.getId(), attacker.getId());
            actionScheduler.scheduleActionEvent(GameActionEvent.internal(defendOrder));
        }
        return new ActionResult(true);
    }

    private void attackUnit(EmpireUnit attacker, UUID targetId, long executionTimeMs) {
        attacker.setState(EmpireUnitState.Fighting);
        attacker.setTargetId(targetId);
        var attackTime = executionTimeMs + Math.round(1000d / attacker.getHitsPerSecond());
        actionScheduler.scheduleActionEvent(GameActionEvent.internal(calcUnitHit(attacker, targetId), attackTime));
    }

    public ActionResult applyCombatStopOrder(CombatStopOrder combatStopOrder) {
        stopUnitCombat(combatStopOrder.getAttackerId());
        return new ActionResult(true);
    }

    private void stopUnitCombat(UUID attackerId) {
        removeCombatHitActionFromQueue(attackerId);
        var unit = unitsById.get(attackerId);
        unit.setState(EmpireUnitState.Idle);
        unit.setTargetId(null);
    }

    private List<UUID> startDefendingOrSetIdle(List<UUID> units, long executionTimeMs) {
        var idleUnits = new ArrayList<UUID>();
        for (var unitId : units) {
            var affectedAttacker = unitsById.get(unitId);
            if (!startDefending(affectedAttacker, executionTimeMs)) {
                affectedAttacker.setState(EmpireUnitState.Idle);
                affectedAttacker.setTargetId(null);
                idleUnits.add(unitId);
            }
        }
        return idleUnits;
    }

    public ActionResult applyCombatHitAction(CombatHitAction combatHitAction, long executionTimeMs) throws EmpireCombatException, EmpireMapException {

        var attackerId = combatHitAction.getAttackerId();
        var attacker = unitsById.get(attackerId);
        var targetId = combatHitAction.getTargetId();
        var target = unitsById.get(targetId);
        var dmg = combatHitAction.getCalculatedDamage();

        target.damageUnit(dmg);

        if (!target.isAlive()) {
            // logger.logDebug(combatHitAction + " INFORMATION " + attacker + " Killed enemy unit" + " " + target + " !");

            var affectedAttackers = removeDeathRelatedActionsFromQueue(target);
            affectedAttackers.add(attackerId);

            var idleUnits = startDefendingOrSetIdle(affectedAttackers, executionTimeMs);

            var hiddenTiles = removeUnit(target.getId());

            var playerId = target.getPlayerId();
            if (unitsByPlayer.get(playerId).size() == 0) checkGameOver();
            return new UnitHitActionResult(true, target, hiddenTiles, idleUnits);
        } else {
            var nextHitTime = executionTimeMs + Math.round(1000d / attacker.getHitsPerSecond());
            actionScheduler.scheduleActionEvent(GameActionEvent.internal(calcUnitHit(attacker, targetId), nextHitTime));
            return new UnitHitActionResult(true, null, null, null);
        }
    }


    public ActionResult applyProductionStartOrder(ProductionStartOrder order, long executionTime) {
        var cityPos = order.getCityPosition();
        var city = citiesByPosition.get(cityPos);
        if (city.getState() == EmpireProductionState.Producing) {
            removeProductionActionFromQueue(cityPos);
        }
        city.setState(EmpireProductionState.Producing);
        var productionAction = new ProductionAction(order);
        actionScheduler.scheduleActionEvent(GameActionEvent.internal(productionAction, executionTime + 1000L * unitTypesById.get(order.getUnitTypeId()).getProductionTime()));
        return new ProductionStartResult(true, productionAction.getUnitId());
    }

    public ActionResult applyProductionStopOrder(ProductionStopOrder order) {
        var cityPos = order.getCityPosition();
        citiesByPosition.get(cityPos).setState(EmpireProductionState.Idle);
        removeProductionActionFromQueue(cityPos);
        return new ActionResult(true);
    }

    public ActionResult applyProductionAction(ProductionAction action) throws EmpireProductionException {
        var unitType = unitTypesById.get(action.getUnitTypeId());
        var city = citiesByPosition.get(action.getCityPosition());
        HashMap<Position, EmpireTerrain> discoveredTiles = null;

        try {
            discoveredTiles = spawnUnit(action.getUnitId(), unitType, city.getPosition(), city.getPlayerId());
        } catch (EmpireMapException | EmpireInternalException e) {
            throw new EmpireProductionException("Spawning new unit failed!", e);
        }

        if (!Empire.AUTO_REQUEUE_PRODUCTION) city.setState(EmpireProductionState.Idle);
        return new UnitCreationResult(true, discoveredTiles);
    }

    public ActionResult applySpawnAction(InitialSpawnAction action) throws EmpireMapException, EmpireInternalException {
        var unitType = unitTypesById.get(action.getUnitTypeId());
        var discoveredTiles = spawnUnit(action.getUnitId(), unitType, action.getPosition(), action.getPlayerId());
        return new UnitCreationResult(true, discoveredTiles);
    }


    public ActionResult applyMovementStartOrder(MovementStartOrder order, long executionTimeMs) throws EmpireMovementException {
        var unitId = order.getUnitId();
        var unit = unitsById.get(unitId);
        if (unit.isMoving()) {
            removeMovementActionFromQueue(unitId);
        } else if (unit.isFighting()) {
            removeCombatHitActionFromQueue(unitId);
        }
        unit.setState(EmpireUnitState.Moving);
        unit.setDestination(order.getDestination());

        try {
            executionTimeMs += 1000d / unit.getTilesPerSecond() * (1 / empireMap.getTile(unit.getPosition()).getSpeed());
        } catch (EmpireMapException e) {
            throw new EmpireMovementException("Retrieving tile speed failed!", e);
        }

        actionScheduler.scheduleActionEvent(GameActionEvent.internal(new MovementAction(order.getUnitId(), order.getDestination(), unit.getPosition()), executionTimeMs));

        return new ActionResult(true);
    }

    public ActionResult applyMovementStopOrder(MovementStopOrder order) {
        stopUnitMovement(order.getUnitId());
        return new ActionResult(true);
    }

    private void stopUnitMovement(UUID unitId) {
        var unit = unitsById.get(unitId);
        unit.setState(EmpireUnitState.Idle);
        unit.setDestination(null);
        removeMovementActionFromQueue(unitId);
    }

    private List<UUID> stopUnitsWithInvalidDestination(Position destination, int ownerId) throws EmpireMapException {
        var tile = empireMap.getTile(destination);
        var unitsWithSameDest = getUnitsWithDestination(tile.getPosition());
        var stoppedUnits = new ArrayList<UUID>();
        unitsWithSameDest.forEach(u -> {
            if (tile.getOccupants().size() >= tile.getMaxOccupants() || u.getPlayerId() != ownerId) {
                stopUnitMovement(u.getId());
                stoppedUnits.add(u.getId());
            }
        });
        return stoppedUnits;
    }

    private List<UUID> stopInvalidCombatWithUnit(EmpireUnit unit) {
        var attackingUnits = getUnitsAttackingUnit(unit.getId());
        var stoppedUnits = new ArrayList<UUID>();
        for (var attacker : attackingUnits) {
            if (!empireMap.areAdjacent(attacker.getPosition(), unit.getPosition())) {
                stopUnitCombat(attacker.getId());
                stoppedUnits.add(attacker.getId());
            }
        }
        return stoppedUnits;
    }

    private List<UUID> stopAllCombatWithUnit(EmpireUnit unit) {
        var attackingUnits = getUnitsAttackingUnit(unit.getId());
        for (var attacker : attackingUnits) {
            stopUnitCombat(attacker.getId());
        }
        return attackingUnits.stream().map(EmpireUnit::getId).toList();
    }

    // unit starts defending when attacked; returns true if counter attack is started
    private boolean startDefending(EmpireUnit unit, long executionTimeMs) {
        var attackingUnits = getUnitsAttackingUnit(unit.getId());
        if (attackingUnits.size() > 0) {
            var unitToAttack = attackingUnits.get(0);
            attackUnit(unit, unitToAttack.getId(), executionTimeMs);
            return true;
        }
        return false;
    }

    public ActionResult applyMovementAction(MovementAction action, long executionTimeMs) throws EmpireMovementException {
        try {
            var visionTileChanges = moveUnitAndSetState(action.getUnitId(), action.getDestination());

            var unitId = action.getUnitId();
            var unit = unitsById.get(unitId);
            var unitPos = unit.getPosition();

            var originPos = action.getOrigin();
            var originTile = empireMap.getTile(originPos);
            if (!originTile.isOccupied()){
                removeProductionActionFromQueue(originPos);
            }

            var interruptedMovingUnits = stopUnitsWithInvalidDestination(unitPos, unit.getPlayerId());

            var interruptedFightingUnits = stopInvalidCombatWithUnit(unit);

            var startedDefending = startDefending(unit, executionTimeMs);

            return new UnitMoveActionResult(true, visionTileChanges, interruptedMovingUnits, interruptedFightingUnits, startedDefending);
        } catch (EmpireMapException | EmpireInternalException e) {
            throw new EmpireMovementException("Moving unit failed!", e);
        }
    }

    public ActionResult applyVisionUpdate(VisionUpdate update) throws EmpireMapException, EmpireInternalException {
        if (update.getNewInactive() != null) {
            var removedUnits = empireMap.removeUnitsFromTiles(update.getNewInactive());
            removedUnits.forEach(id -> {
                removeMovementActionFromQueue(id);
                removeCombatHitActionFromQueue(id);
            });
        }
        if (update.getNewActive() != null) empireMap.addTiles(update.getNewActive());
        return new ActionResult(true);
    }

    public ActionResult applyUnitAppearedAction(UnitAppearedAction appearedAction) throws EmpireMapException, EmpireInternalException {
        var unit = appearedAction.getUnit();
        spawnUnit(unit);
        var stoppedMovingUnits = stopUnitsWithInvalidDestination(unit.getPosition(), unit.getPlayerId());
        // logger.logDebug("UnitAppearedAction: Unit appeared.");
        return new UnitAppearedActionResult(true, stoppedMovingUnits);
    }

    public ActionResult applyUnitDamagedAction(UnitDamagedAction damagedAction, long executionTimeMs) throws EmpireCombatException, EmpireMapException {
        if (!unitsById.containsKey(damagedAction.getTargetId()))
            throw new EmpireCombatException("Damaged unit does not exists! Unit id: " + damagedAction.getTargetId());

        var target = unitsById.get(damagedAction.getTargetId());
        target.damageUnit(damagedAction.getDamageDealt());

        if (!target.isAlive()) {
            // logger.logDebug("UnitDamagedAction: Unit killed.");

            var affectedAttackers = removeDeathRelatedActionsFromQueue(target);

            var idleUnits = startDefendingOrSetIdle(affectedAttackers, executionTimeMs);

            var hiddenTiles = removeUnit(target.getId());
            return new UnitHitActionResult(true, target, hiddenTiles, idleUnits);
        } else
            return new UnitHitActionResult(true, null, null, null);
    }

    public ActionResult applyUnitStateAction(UnitStateAction stateAction) throws EmpireCombatException {
        if (!unitsById.containsKey(stateAction.getUnitId()))
            throw new EmpireCombatException("Unit which is about to change state does not exists! Unit id: " + stateAction.getUnitId());
        var unit = unitsById.get(stateAction.getUnitId());
        var newState = stateAction.getState();

        if (!newState.equals(EmpireUnitState.Fighting) && unit.getState().equals(EmpireUnitState.Fighting)) {
            removeCombatHitActionFromQueue(unit.getId());
        }
        unit.setState(newState);
        unit.setTargetId(stateAction.getTargetId());
        // logger.logDebug("UnitStateAction: Unit state changed.");
        return new ActionResult(true);
    }

    public ActionResult applyUnitVanishedAction(UnitVanishedAction vanished) throws EmpireMapException {
        var unitId = vanished.getVanishedId();
        var vanishedUnit = getUnit(unitId);
        var interruptedFightingUnits = stopAllCombatWithUnit(vanishedUnit);
        removeMovementActionFromQueue(unitId);
        var hiddenTiles = removeUnit(vanished.getVanishedId());
        // logger.logDebug("UnitVanishedAction: Unit vanished.");
        return new UnitVanishedActionResult(true, hiddenTiles, interruptedFightingUnits);
    }
    //endregion

    //region Helper
    public CombatHitAction calcUnitHit(EmpireUnit attacker, UUID targetId) {
        return new CombatHitAction(attacker.getId(), targetId, attacker.getDamageRange().getNextDamage());
    }

    public List<EmpireUnit> getUnitsWithDestination(Position destination) {
        return unitsById.values().stream().filter(u -> {
            var dest = u.getDestination();
            return dest != null && dest.equals(destination);
        }).collect(Collectors.toList());
    }

    public List<EmpireUnit> getUnitsAttackingUnit(UUID target) {
        return unitsById.values().stream().filter(u -> {
            var targetId = u.getTargetId();
            return targetId != null && targetId.equals(target) && u.isAlive();
        }).collect(Collectors.toList());
    }

    private VisionTileChanges moveUnitAndSetState(UUID unitId, Position destination) throws EmpireMapException, EmpireInternalException {
        var visionTileChanges = empireMap.moveUnit(unitId, destination);
        var unit = unitsById.get(unitId);

        if (!Empire.AUTO_REQUEUE_MOVEMENT) {
            unit.setState(EmpireUnitState.Idle);
            unit.setDestination(null);
        }
        return visionTileChanges;
    }
    //endregion

    //region Add/Remove Unit

    private HashMap<Position, EmpireTerrain> spawnUnit(UUID id, EmpireUnitType unitType, Position position, int playerId) throws EmpireMapException, EmpireInternalException {
        var unit = new EmpireUnit(id, playerId, unitType, position);
        return spawnUnit(unit);
    }

    public HashMap<Position, EmpireTerrain> spawnUnit(EmpireUnit unit) throws EmpireMapException, EmpireInternalException {
        return empireMap.addUnit(unit);
    }

    public List<Position> removeUnit(UUID unitId) throws EmpireMapException {
        return empireMap.removeUnit(unitId);
    }
    //endregion

    //region PossibleActions
    @Override
    public Set<EmpireEvent> getPossibleActions(int playerId) {
        if (!players.contains(playerId)) return null;
        Set<EmpireEvent> set = new HashSet<>();

        // Production Possibilities
        var citiesOccupied = citiesByPosition.values().stream().filter(v -> v.getPlayerId() == playerId).collect(Collectors.toList());
        var nrOfUnits = unitsByPlayer.get(playerId).size();
        citiesOccupied.forEach(c -> {
            if (c.getState() == EmpireProductionState.Idle && nrOfUnits < empireConfiguration.getUnitCap()) {
                unitTypesById.keySet().forEach(k -> set.add(new ProductionStartOrder(c.getPosition(), k)));
            } else if (c.getState() == EmpireProductionState.Producing) {
                set.add(new ProductionStopOrder(c.getPosition()));
            }
        });

        // Unit Possibilities
        for (var unit : unitsByPlayer.get(playerId)) {
            try {
                set.addAll(empireMap.getPossibleActions(unit));
            } catch (Exception e) {
                logger.logError(e.getMessage());
                e.printStackTrace();
            }
        }
        return set;
    }

    @Override
    public Set<EmpireEvent> getPossibleActions() {
        Set<EmpireEvent> actions = new HashSet<>();
        for (var pl : players) {
            actions.addAll(getPossibleActions(pl));
        }
        return actions;
    }
    //endregion

    //region Getter
    public EmpireUnitType getUnitType(int id) {
        if (!unitTypesById.containsKey(id)) return null;
        return unitTypesById.get(id);
    }

    public EmpireUnit getUnit(UUID id) {
        if (!unitsById.containsKey(id)) return null;
        return unitsById.get(id);
    }

    public EmpireCity getCity(Position position) {
        if (!citiesByPosition.containsKey(position)) return null;
        return citiesByPosition.get(position);
    }

    public Map<Position, EmpireCity> getCitiesByPosition() {
        return citiesByPosition;
    }

    public Map<UUID, EmpireUnit> getUnitsById() {
        return unitsById;
    }

    public List<EmpireUnit> getUnitsByPlayer(int playerId) {
        return unitsByPlayer.get(playerId);
    }

    public EmpireLogger getLogger() {
        return logger;

    }

    //endregion

    private void checkGameOver() {
        int playersAlive = 0;
        int winner = 0;
        for (var player : players) {
            var alive = unitsByPlayer.get(player).size() > 0;
            playersAlive += alive ? 1 : 0;
            winner = alive ? player : winner;
        }
        if (playersAlive > 1) return;
        synchronized (gameOverLock) {
            this.winner = winner;
            gameOver.set(true);
            gameOverLock.notifyAll();
        }
    }

    @Override
    public boolean isGameOver() {
        return gameOver.get();
    }

    @Override
    public void waitForGameOver(long timeoutMs) throws InterruptedException {
        var timeoutAt = System.currentTimeMillis() + timeoutMs;
        synchronized (gameOverLock) {
            while (!gameOver.get() && System.currentTimeMillis() < timeoutAt) {
                gameOverLock.wait(timeoutMs);
            }
        }
        logger.logDebug("Game over!");
        if (winner != EMPTY_ID)
            logger.logDebug("Player " + winner + " won!");
        else
            logger.logDebug("Game timed out! No winner could be determined yet...");
    }

    @Override
    public double getPlayerUtilityWeight(int player) {
        return 1d;
    }

    @Override
    public double getUtilityValue(int player) {
        return player == winner ? 1d : 0d;
    }

    @Override
    public List<TimedActionRecord<EmpireEvent>> getActionRecords() {
        return eventRecord;
    }


    @Override
    public GameClock getGameClock() {
        return this.gameClock;
    }

    @Override
    public void scheduleActionEvent(GameActionEvent<EmpireEvent> action) {
        actionScheduler.scheduleActionEvent(action);
    }

    @Override
    public VisualizationType getVisualizationType() {
        return VisualizationType.javafx;
    }

    @Override
    public String getVisualizationClassName() {
        return EmpireVisualization.class.getName();
    }

    @Override
    public double getHeuristicValue(int player) {
        return empireConfiguration.getUnitCap() - unitsByPlayer.get(player).size();
    }

    @Override
    public EmpireConfiguration getGameConfiguration() {
        return empireConfiguration;
    }

    @Override
    public boolean hasDivergingGameConfiguration() {
        return true;
    }

    @Override
    public String getConfigurationEditorClassName() {
        return EmpireConfigEditor.class.getName();
    }

    @Override
    public boolean isCanonical() {
        return false;
    }

    @Override
    public boolean isRealtime() {
        return true;
    }

    @Override
    public EmpireMap getBoard() {
        return empireMap;
    }

    @Override
    public int getMinimumNumberOfPlayers() {
        return 2;
    }

    @Override
    public int getMaximumNumberOfPlayers() {
        return empireConfiguration.getStartingCities().size();
    }

    @Override
    public int getNumberOfPlayers() {
        return players.size();
    }

    @Override
    public GameUpdate<EmpireEvent> readGameUpdate() throws InterruptedException {
        return actionScheduler.readGameUpdate();
    }

    @Override
    public String getHumanAgentClassName() {
        return null;
    }

    @Override
    public String toTextRepresentation() {
        return empireMap.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Empire empire = (Empire) o;

        if (winner != empire.winner) return false;
        if (!eventRecord.equals(empire.eventRecord)) return false;
        if (!empireConfiguration.equals(empire.empireConfiguration)) return false;
        if (!players.equals(empire.players)) return false;
        if (!empireMap.equals(empire.empireMap)) return false;
        if (!unitsById.equals(empire.unitsById)) return false;
        if (!unitsByPlayer.equals(empire.unitsByPlayer)) return false;
        if (!citiesByPosition.equals(empire.citiesByPosition)) return false;
        return unitTypesById.equals(empire.unitTypesById);
    }

    @Override
    public int hashCode() {
        int result = winner;
        result = 31 * result + eventRecord.hashCode();
        result = 31 * result + empireConfiguration.hashCode();
        result = 31 * result + players.hashCode();
        result = 31 * result + empireMap.hashCode();
        result = 31 * result + unitsById.hashCode();
        result = 31 * result + unitsByPlayer.hashCode();
        result = 31 * result + citiesByPosition.hashCode();
        result = 31 * result + unitTypesById.hashCode();
        return result;
    }
}
