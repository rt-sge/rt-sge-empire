package at.ac.tuwien.ifs.sge.game.empire.core;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.VisionUpdate;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitAppearedAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitDamagedAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.UnitVanishedAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.CombatStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.MovementStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.ProductionStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import java.util.Map;
import java.util.UUID;

public record EmpireLogger(Logger LOGGER,
                           Map<UUID, EmpireUnit> unitsById,
                           Map<Position, EmpireCity> citiesByPosition,
                           Map<Integer, EmpireUnitType> unitTypesById,
                           EmpireMap empireMap) {

    public void logEmpireEvent(EmpireEvent action) throws EmpireMapException {
        LOGGER.debug(eventToString(action));
    }

    public void logInfo(String content) {
        LOGGER.info(content);
    }

    public void logError(String content) {
        LOGGER.error(content);
    }

    public void logDebug(String content) {
        LOGGER.debug(content);
    }

    public String eventToString(EmpireEvent action) throws EmpireMapException {
        String content = "";
        if (action instanceof CombatStartOrder combat) {
            var attacker = unitsById.get(combat.getAttackerId());
            var target = unitsById.get(combat.getTargetId());
            content = "ORDER Start Combat " + attacker + " --> " + target;

        } else if (action instanceof CombatStopOrder order) {
            var attacker = unitsById.get(order.getAttackerId());
            content = "ORDER Stop Combat " + attacker;

        } else if (action instanceof CombatHitAction hit) {
            var attacker = unitsById.get(hit.getAttackerId());
            var target = unitsById.get(hit.getTargetId());

            if (target != null)
                content = "ACTION Hit " + attacker + " -- (-" + hit.getCalculatedDamage() + "\u2764) --> " + target;

        }
        else if (action instanceof ProductionStartOrder order) {

            var city = citiesByPosition.get(order.getCityPosition());
            var unitType = unitTypesById.get(order.getUnitTypeId());
            content = "ORDER Produce " + unitType + " in " + city;

        } else if (action instanceof ProductionStopOrder order) {
            var city = citiesByPosition.get(order.getCityPosition());
            content = "ORDER Stop Production in " + city;

        } else if (action instanceof ProductionAction production) {
            var city = citiesByPosition.get(production.getCityPosition());
            var unitType = unitTypesById.get(production.getUnitTypeId());
            content = "ACTION Produced " + unitType + " produced in " + city;
        } else if (action instanceof InitialSpawnAction spawn) {
            var terrain = empireMap.getTile(spawn.getPosition());
            var unitType = unitTypesById.get(spawn.getUnitTypeId());
            content = "ACTION Spawned " + unitType + " on " + terrain;
        } else if (action instanceof MovementStartOrder moveOrder) {
            var unit = unitsById.get(moveOrder.getUnitId());
            content = "ORDER Move " + unit + " from " + unit.getPosition() + " to " + moveOrder.getDestination();
        } else if (action instanceof MovementStopOrder moveOrder) {
            var unit = unitsById.get(moveOrder.getUnitId());
            content = "ORDER Stop Moving " + unit;
        } else if (action instanceof MovementAction move) {
            var unit = unitsById.get(move.getUnitId());
            content = "ACTION Move " + unit + " to " + move.getDestination();
        } else if (action instanceof VisionUpdate update) {
            content = "UPDATE Vision: " + (update.getNewActive() == null ? "0" : update.getNewActive().size()) + " new tiles & "
                    + (update.getNewInactive() == null ? "0" : update.getNewInactive().size()) + " hidden tiles";
        } else if (action instanceof UnitAppearedAction appearedAction) {
            content = "UPDATE Unit appeared: " + appearedAction.getUnit();
        } else if (action instanceof UnitVanishedAction vanishedAction) {
            content = "UPDATE Unit vanished: " + vanishedAction.getVanishedId();
        } else if (action instanceof UnitDamagedAction damagedAction) {
            content = "UPDATE Unit damaged: " + damagedAction.getTargetId() + " receives " + damagedAction.getDamageDealt() + " damage";
        }
        return content;

    }

    private void logInfoForPlayer(String content, int playerId) {
        LOGGER.debug(playerId + content);
    }
}
