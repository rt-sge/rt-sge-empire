package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireCombatException extends Exception {
    public EmpireCombatException(String message) {
        super(message);
    }
    public EmpireCombatException(String message, Throwable cause) {
        super(message, cause);
    }
}
