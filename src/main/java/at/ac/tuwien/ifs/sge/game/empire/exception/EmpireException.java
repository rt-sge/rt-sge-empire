package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireException extends  RuntimeException{
    public EmpireException(String message) {
        super(message);
    }

    public EmpireException(String message, Throwable cause) {
        super(message, cause);
    }
}
