package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireInternalException extends Exception {
    public EmpireInternalException(String message) {
        super(message);
    }
    public EmpireInternalException(String message, Throwable cause) {
        super(message, cause);
    }
}
