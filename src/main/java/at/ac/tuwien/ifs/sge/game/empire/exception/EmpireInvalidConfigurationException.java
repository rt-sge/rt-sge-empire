package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireInvalidConfigurationException extends Exception {
    public EmpireInvalidConfigurationException(String message) {
        super(message);
    }
    public EmpireInvalidConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
