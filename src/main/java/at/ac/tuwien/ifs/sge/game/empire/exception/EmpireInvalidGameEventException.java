package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireInvalidGameEventException extends Exception {
    public EmpireInvalidGameEventException(String message) {
        super(message);
    }

    public EmpireInvalidGameEventException(String message, Throwable cause) {
        super(message, cause);
    }
}
