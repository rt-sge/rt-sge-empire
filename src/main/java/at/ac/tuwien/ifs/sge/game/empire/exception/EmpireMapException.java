package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireMapException extends Exception{
    public EmpireMapException(String message) {
        super(message);
    }

    public EmpireMapException(String message, Throwable cause) {
        super(message, cause);
    }
}
