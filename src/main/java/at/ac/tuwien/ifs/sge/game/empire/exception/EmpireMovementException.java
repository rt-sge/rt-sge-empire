package at.ac.tuwien.ifs.sge.game.empire.exception;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;

public class EmpireMovementException extends Exception {

  private MovementAction causedAction = null;

  public EmpireMovementException(String message, MovementAction causedAction) {
    super(message);
    this.causedAction = causedAction;
  }

  public EmpireMovementException(String message) {
    super(message);
  }

  public EmpireMovementException(String message, Throwable cause) {
    super(message, cause);
  }

  public MovementAction getCausedAction() {
    return causedAction;
  }
}
