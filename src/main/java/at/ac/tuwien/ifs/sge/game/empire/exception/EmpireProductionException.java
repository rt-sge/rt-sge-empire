package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireProductionException extends Exception {
    public EmpireProductionException(String message) {
        super(message);
    }
    public EmpireProductionException(String message, Throwable cause) {
        super(message, cause);
    }
}
