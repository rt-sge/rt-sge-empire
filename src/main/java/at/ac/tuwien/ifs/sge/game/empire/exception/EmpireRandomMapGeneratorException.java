package at.ac.tuwien.ifs.sge.game.empire.exception;

public class EmpireRandomMapGeneratorException  extends Exception {
    public EmpireRandomMapGeneratorException(String message) {
        super(message);
    }
    public EmpireRandomMapGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }
}
