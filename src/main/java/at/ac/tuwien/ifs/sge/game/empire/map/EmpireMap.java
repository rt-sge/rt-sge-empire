package at.ac.tuwien.ifs.sge.game.empire.map;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.CombatStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.MovementStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrainType;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class EmpireMap {

    private EmpireTerrain[][] empireTiles;
    private final Map<Position, VisionOrigin[]> activeVisionByPosition;
    private final Map<Position, boolean[]> discoveredByPosition;
    private final Map<Position, EmpireCity> citiesByPosition;
    private final Map<UUID, EmpireUnit> unitsById;
    Map<Integer, List<EmpireUnit>> unitsByPlayer;
    private final Size mapSize;

    //region Constructors

    public EmpireMap(EmpireTerrain[][] empireTiles,
        Map<Position, EmpireCity> citiesByPosition,
        Map<UUID, EmpireUnit> unitsById,
        Map<Integer, List<EmpireUnit>> unitsByPlayer,
        int numberOfPlayers) {
        this.empireTiles = empireTiles;
        this.mapSize = new Size(empireTiles[0].length, empireTiles.length);
        this.citiesByPosition = citiesByPosition;
        this.unitsById = unitsById;
        this.unitsByPlayer = unitsByPlayer;
        this.activeVisionByPosition = new HashMap<>();
        this.discoveredByPosition = new HashMap<>();
        initCitiesById(this.empireTiles);
        initVision(numberOfPlayers);
    }

    public EmpireMap(EmpireMap map, Map<Position, EmpireCity> citiesByPosition,
        Map<UUID, EmpireUnit> unitsById, Map<Integer, List<EmpireUnit>> unitsByPlayer,
        int numberOfPlayers) {
        this.empireTiles = new EmpireTerrain[map.mapSize.getHeight()][map.mapSize.getWidth()];
        for (int y = 0; y < map.mapSize.getHeight(); y++) {
            for (int x = 0; x < map.mapSize.getWidth(); x++) {
                var tile = map.empireTiles[y][x];
                EmpireTerrain newTile = null;
                if (tile != null) {
                    if (tile instanceof EmpireCity cityTile) {
                        var newCityTile = new EmpireCity(cityTile);
                        citiesByPosition.put(newCityTile.getPosition(), newCityTile);
                        newTile = newCityTile;
                    } else {
                        newTile = new EmpireTerrain(tile);
                    }
                    if (newTile.getOccupants() != null) {
                        var occupants = newTile.getOccupants().stream().toList();
                        occupants.forEach(unit -> {
                            unitsById.put(unit.getId(), unit);
                            unitsByPlayer.get(unit.getPlayerId()).add(unit);
                        });
                    }
                }

                this.empireTiles[y][x] = newTile;
            }
        }

        this.activeVisionByPosition = new HashMap<>();
        map.activeVisionByPosition.forEach(((position, visionOrigins) -> {
            var origins = new VisionOrigin[numberOfPlayers];
            for (int i = 0; i < visionOrigins.length; i++) {
                origins[i] = new VisionOrigin(visionOrigins[i]);
            }
            this.activeVisionByPosition.put(new Position(position), origins);
        }));

        this.discoveredByPosition = new HashMap<>();
        map.discoveredByPosition.forEach(((position, discovered) -> {
            var copy = Arrays.copyOf(discovered, discovered.length);
            this.discoveredByPosition.put(new Position(position), copy);
        }));

        this.citiesByPosition = citiesByPosition;
        this.unitsById = unitsById;
        this.unitsByPlayer = unitsByPlayer;
        this.mapSize = map.mapSize;

    }

    private void initEmpireTiles(char[][] map, Map<Character, EmpireTerrainType> terrainDictionary) {
        this.empireTiles = new EmpireTerrain[map.length][];
        for (int y = 0; y < map.length; y++) {
            empireTiles[y] = new EmpireTerrain[map[y].length];
            for (int x = 0; x < map[y].length; x++) {
                if (!terrainDictionary.containsKey(map[y][x])) {
                    continue;
                }
                if (map[y][x] == 'c') {
                    empireTiles[y][x] = new EmpireCity(terrainDictionary.get(map[y][x]), new Position(x, y));
                } else {
                    empireTiles[y][x] = new EmpireTerrain(terrainDictionary.get(map[y][x]), new Position(x, y));
                }
            }
        }
    }

    private void initCitiesById(EmpireTerrainType[][] map) {
        for (var row : map) {
            for (var e : row) {
                if (e == null)
                    continue;
                if (e.getClass() == EmpireCity.class) {
                    var city = (EmpireCity) e;
                    citiesByPosition.put(city.getPosition(), city);
                }
            }
        }
    }

    private void initVision(int numberOfPlayers) {
        for (int y = 0; y < mapSize.getHeight(); y++) {
            for (int x = 0; x < mapSize.getWidth(); x++) {
                var visionArray = new VisionOrigin[numberOfPlayers];
                var discoveredArray = new boolean[numberOfPlayers];
                for (int player = 0; player < numberOfPlayers; player++) {
                    visionArray[player] = new VisionOrigin(player);
                }
                activeVisionByPosition.put(new Position(x, y), visionArray);
                discoveredByPosition.put(new Position(x, y), discoveredArray);
            }
        }
    }
    //endregion

    //region Getters
    public Size getMapSize() {
        return mapSize;
    }

    public EmpireTerrain[][] getEmpireTiles() {
        return empireTiles;
    }

    public EmpireTerrain getTile(Position position) throws EmpireMapException {
        return getTile(position.getX(), position.getY());
    }

    public EmpireTerrain getTile(int x, int y) throws EmpireMapException {
        if (!isInside(x, y))
            throw new EmpireMapException("Position out of bounds " + "x: " + x + "y: " + y);
        return empireTiles[y][x];
    }

    public Position getPosition(UUID unitId) {
        if (unitId != null && unitsById.containsKey(unitId)) {
            var unit = unitsById.get(unitId);
            return unit.getPosition();
        }
        return null;
    }

    public Map<Position, VisionOrigin[]> getActiveVisionByPosition() {
        return activeVisionByPosition;
    }

    public Map<Position, boolean[]> getDiscoveredByPosition() {
        return discoveredByPosition;
    }

    public String getDiscoveredMap(int playerId) {
        StringBuilder s = new StringBuilder();
        for (int y = 0; y < getMapSize().getHeight(); y++) {
            s.append('|');
            for (int x = 0; x < getMapSize().getWidth(); x++) {
                var discovered = getDiscoveredByPosition().get(new Position(x, y))[playerId];
                s.append(discovered ? 'x' : '-');
            }
            s.append("|\n");
        }
        return s.toString();
    }
    //endregion


    //region Add/Remove Unit

    public HashMap<Position, EmpireTerrain> addUnit(EmpireUnit unit) throws EmpireMapException, EmpireInternalException {
        validateAddUnit(unit.getPosition(), unit.getPlayerId());
        addUnitToTile(unit);
        return addUnitFromTile(unit);
    }

    public void addUnitWithoutUpdatingVision(EmpireUnit unit) throws EmpireMapException, EmpireInternalException {
        validateAddUnit(unit.getPosition(), unit.getPlayerId());
        addUnitToTile(unit);
        unitsById.put(unit.getId(), unit);
        unitsByPlayer.get(unit.getPlayerId()).add(unit);
    }

    public HashMap<Position, EmpireTerrain> addUnitFromTile(EmpireUnit unit) throws EmpireMapException, EmpireInternalException {
        unitsById.put(unit.getId(), unit);
        unitsByPlayer.get(unit.getPlayerId()).add(unit);
        return updateVisionThroughNewEntity(unit);
    }

    private void addUnitToTile(EmpireUnit unit) throws EmpireMapException {
        if (unit == null)
            throw new EmpireMapException("Unit can not be null");
        var position = unit.getPosition();
        var tile = empireTiles[position.getY()][position.getX()];
        tile.getOccupants().add(unit);
    }

    public void removeUnitFromTile(EmpireUnit unit) throws EmpireMapException {
        var tile = getTile(unit.getPosition());
        tile.getOccupants().remove(unit);
    }

    public List<Position> removeUnit(UUID unitId) throws EmpireMapException {
        validateRemoveUnit(unitId);
        var unit = unitsById.get(unitId);
        removeUnitFromTile(unit);
        unitsByPlayer.get(unit.getPlayerId()).remove(unit);
        unitsById.remove(unitId);
        return updateVisionThroughRemovedEntity(unit);
    }
    //endregion

    //region Move Unit
    public VisionTileChanges moveUnit(UUID unitId, Position destination) throws EmpireMapException, EmpireInternalException {
        if (!unitsById.containsKey(unitId))
            throw new EmpireInternalException("Unit not found!");
        var unit = unitsById.get(unitId);
        if (!unit.isMoving())
            throw new EmpireMapException("Unit is not in a moving state");
        var origin = unit.getPosition();
        removeUnitFromTile(unit);
        unit.setPosition(destination);
        addUnitToTile(unit);
        return updateVisionThroughMovement(unit, origin);
    }
    //endregion

    //region Validation Methods

    public void validateAddUnit(Position position, int playerId) throws EmpireMapException {
        var bounds = isInside(position);
        if (!bounds)
            throw new EmpireMapException("Position out of bounds!");

        var tile = empireTiles[position.getY()][position.getX()];
        if (tile == null)
            throw new EmpireMapException("Tile does not exist!");

        if (canHaveOccupants(tile)) {
            if (tile.getMaxOccupants() != -1 && (tile.getOccupants().size() >= tile.getMaxOccupants()))
                throw new EmpireMapException("Tile can not have more than " + tile.getMaxOccupants() + " occupants!");
        } else
            throw new EmpireMapException("Tile can not have occupants!");

        var allowedToOccupy = tile.getPlayerId() == playerId || tile.getPlayerId() == -1; // check if city is allowed to take or fortify
        if (!allowedToOccupy)
            throw new EmpireMapException("Tile is already occupied by another player!");
    }

    public void validateTile(EmpireTerrain tile) throws EmpireMapException {
        if (!isInside(tile.getPosition()))
            throw new EmpireMapException("Tile is outside of map");

        if (canHaveOccupants(tile)) {
            if (tile.getMaxOccupants() != -1 && (tile.getOccupants().size() > tile.getMaxOccupants()))
                throw new EmpireMapException("Tile has more than " + tile.getMaxOccupants() + " occupants!");
        } else if (tile.getOccupants() != null && tile.getOccupants().size() > 0) {
            throw new EmpireMapException("Tile has occupants but should not have occupants!");
        }
    }

    public void validateRemoveUnit(UUID id) throws EmpireMapException {
        var exists = unitsById.containsKey(id);
        if (!exists)
            throw new EmpireMapException("Unit with specified id does not exist!");
    }

    private boolean canHaveOccupants(EmpireTerrain terrain) {
        return (terrain.getMaxOccupants() > 0) || (terrain.getMaxOccupants() == -1);
    }

    public boolean areAdjacent(Position a, Position b) {
        var xDiff = Math.abs(a.getX() - b.getX());
        var yDiff = Math.abs(a.getY() - b.getY());
        return xDiff >= 0 && xDiff <= 1 && yDiff >= 0 && yDiff <= 1;
    }

    public boolean isMovementPossible(int x, int y, int playerId) throws EmpireMapException {
        var tile = getTile(x, y);
        var enemyTerritory = tile.getPlayerId() != playerId && !(tile.getPlayerId() == EmpireCity.UNOCCUPIED_ID);
        var remainingSpace = (tile.getMaxOccupants() > tile.getOccupants().size()) || (tile.getMaxOccupants() == -1);
        return !enemyTerritory && tile.getMaxOccupants() != 0 && remainingSpace;
    }

    public boolean isInside(Position position) {
        return isInside(position.getX(), position.getY());
    }

    public boolean isInside(int x, int y) {
        return x >= 0 && x < mapSize.getWidth() && y >= 0 && y < mapSize.getHeight();
    }
    //endregion

    public void addTiles(Map<Position, EmpireTerrain> tiles) throws EmpireMapException, EmpireInternalException {
        for (var pair : tiles.entrySet()) {
            var pos = pair.getKey();
            var tile = pair.getValue();
            validateTile(tile);
            empireTiles[pos.getY()][pos.getX()] = tile;
            if (tile instanceof EmpireCity city)
                citiesByPosition.put(tile.getPosition(), city);
            if (tile.getMaxOccupants() != 0 && tile.getOccupants().size() > 0) {
                var occupants = tile.getOccupants();
                for (var o : occupants) {
                    if (!unitsById.containsKey(o.getId())) {
                        addUnitFromTile(o);
                    }
                }
            }
        }
    }

    public List<UUID> removeUnitsFromTiles(List<Position> newInactive) throws EmpireMapException {
        var removedUnits = new ArrayList<UUID>();
        for (var pos : newInactive) {
            var tile = getTile(pos);
            var occupants = tile.getOccupants();
            if (occupants != null) {
                for (var occupant : occupants) {
                    var id = occupant.getId();
                    unitsByPlayer.get(occupant.getPlayerId()).remove(occupant);
                    unitsById.remove(id);
                    removedUnits.add(id);
                }
                occupants.clear();
            }
        }
        return removedUnits;
    }

    // adds a players vision origin from a unit to the specified position and returns the discovered tile
    public EmpireTerrain addVision(Position pos, int playerId, UUID unitId) throws EmpireMapException {
        var visionOrigin = activeVisionByPosition.get(pos)[playerId];
        EmpireTerrain tile = null;
        if (!visionOrigin.hasVision()) {
            discoveredByPosition.get(pos)[playerId] = true;
            tile = getTile(pos);
        }
        visionOrigin.getOrigins().put(unitId, pos);
        return tile;
    }

    // removes a players vision origin from a unit  to the specified position and returns false if the player no longer has any vision on this position
    public boolean removeVision(Position pos, int playerId, UUID unitId) {
        var visionOrigin = activeVisionByPosition.get(pos)[playerId];
        visionOrigin.getOrigins().remove(unitId);
        return visionOrigin.hasVision();
    }

    private void addVisionAndUpdateDiscoveredTiles(Position pos, int playerId, UUID unitId, HashMap<Position, EmpireTerrain> discoveredTiles) throws EmpireMapException {
        var discoveredTile = addVision(pos, playerId, unitId);
        if (discoveredTile != null) {
            if (discoveredTile instanceof EmpireCity ec)
                discoveredTile = new EmpireCity(ec);
            else discoveredTile = new EmpireTerrain(discoveredTile);
            discoveredTiles.put(pos, discoveredTile);
        }
    }

    private void removeVisionAndUpdateHiddenTiles(Position pos, int playerId, UUID unitId, List<Position> hiddenTiles) {
        var isStillVisible = removeVision(pos, playerId, unitId);
        if (!isStillVisible)
            hiddenTiles.add(pos);
    }

    public ArrayList<Position> updateVisionThroughRemovedEntity(EmpireUnit killedUnit) {
        var hiddenTiles = new ArrayList<Position>();
        var position = killedUnit.getPosition();
        var fov = killedUnit.getFov();
        var playerId = killedUnit.getPlayerId();
        for (int y = position.getY() - fov; y <= position.getY() + fov; y++) {
            for (int x = position.getX() - fov; x <= position.getX() + fov; x++) {
                if (!isInside(x, y))
                    continue;
                var pos = new Position(x, y);
                removeVisionAndUpdateHiddenTiles(pos, playerId, killedUnit.getId(), hiddenTiles);
            }
        }
        if (hiddenTiles.size() > 0)
            return hiddenTiles;
        return null;
    }

    public HashMap<Position, EmpireTerrain> updateVisionThroughNewEntity(EmpireUnit unit) throws EmpireInternalException, EmpireMapException {
        var unitId = unit.getId();
        var position = unit.getPosition();
        if (!unitsById.containsKey(unitId))
            throw new EmpireInternalException("Unit not found!");
        var fov = unit.getFov();
        var discoveredTiles = new HashMap<Position, EmpireTerrain>();
        int startValY = Math.max(position.getY() - fov, 0);
        int startValX = Math.max(position.getX() - fov, 0);


        for (int y = startValY; y <= position.getY() + fov; y++) {
            for (int x = startValX; x <= position.getX() + fov; x++) {
                if (!isInside(x, y))
                    continue;
                var pos = new Position(x, y);
                addVisionAndUpdateDiscoveredTiles(pos, unit.getPlayerId(), unitId, discoveredTiles);
            }
        }
        return discoveredTiles;
    }

    private VisionTileChanges updateVisionThroughMovement(EmpireUnit unit, Position origin) throws EmpireMapException {
        var unitId = unit.getId();
        var destination = unit.getPosition();
        var fov = unit.getFov();
        var discoveredTiles = new HashMap<Position, EmpireTerrain>();
        var hiddenTiles = new ArrayList<Position>();
        var playerId = unit.getPlayerId();

        var xDiff = destination.getX() - origin.getX();
        var yDiff = destination.getY() - origin.getY();

        if (xDiff != 0 && yDiff != 0) {
            var ringLength = (fov * 2 + 1) * 4;

            var cornerIndex0 = 0;
            var cornerIndex1 = cornerIndex0 + ringLength / 4; // 5
            var cornerIndex2 = cornerIndex1 + ringLength / 4; // 10
            var cornerIndex3 = cornerIndex2 + ringLength / 4; // 15

            int x = xDiff == -1 ? destination.getX() - fov : origin.getX() - fov;
            int y = yDiff == -1 ? destination.getY() - fov : origin.getY() - fov;
            for (int i = 0; i < ringLength; i++) {
                var pos = new Position(x, y);
                if (i < cornerIndex1) {
                    if (isInside(pos)) {
                        if (!(yDiff != xDiff && i == cornerIndex0)) { // skip corner
                            if (yDiff == -1)
                                addVisionAndUpdateDiscoveredTiles(pos, playerId, unitId, discoveredTiles);
                            else
                                removeVisionAndUpdateHiddenTiles(pos, playerId, unitId, hiddenTiles);
                        }
                    }
                    x += 1;
                } else if (i < cornerIndex2) {
                    if (isInside(pos)) {
                        if (!(yDiff == xDiff && i == cornerIndex1)) { // skip corner
                            if (xDiff == 1)
                                addVisionAndUpdateDiscoveredTiles(pos, playerId, unitId, discoveredTiles);
                            else
                                removeVisionAndUpdateHiddenTiles(pos, playerId, unitId, hiddenTiles);
                        }
                    }
                    y += 1;
                } else if (i < cornerIndex3) {
                    if (isInside(pos)) {
                        if (!(yDiff != xDiff && i == cornerIndex2)) { // skip corner
                            if (yDiff == 1)
                                addVisionAndUpdateDiscoveredTiles(pos, playerId, unitId, discoveredTiles);
                            else
                                removeVisionAndUpdateHiddenTiles(pos, playerId, unitId, hiddenTiles);
                        }
                    }
                    x -= 1;
                } else {
                    if (isInside(pos)) {
                        if (!(yDiff == xDiff && i == cornerIndex3)) { // skip corner
                            if (xDiff == -1)
                                addVisionAndUpdateDiscoveredTiles(pos, playerId, unitId, discoveredTiles);
                            else
                                removeVisionAndUpdateHiddenTiles(pos, playerId, unitId, hiddenTiles);
                        }
                    }
                    y -= 1;
                }
            }
        } else if (yDiff == 0 && xDiff != 0) {
            int xAdd = destination.getX() + fov * xDiff;
            int xRemove = origin.getX() - fov * xDiff;

            for (int y = origin.getY() - fov; y <= origin.getY() + fov; y++) {
                var pos = new Position(xRemove, y);
                if (isInside(pos))
                    removeVisionAndUpdateHiddenTiles(pos, playerId, unitId, hiddenTiles);
                pos = new Position(xAdd, y);
                if (isInside(pos))
                    addVisionAndUpdateDiscoveredTiles(pos, playerId, unitId, discoveredTiles);
            }
        } else if (yDiff != 0) {
            int yAdd = destination.getY() + fov * yDiff;
            int yRemove = origin.getY() - fov * yDiff;

            for (int x = origin.getX() - fov; x <= origin.getX() + fov; x++) {
                var pos = new Position(x, yRemove);
                if (isInside(pos))
                    removeVisionAndUpdateHiddenTiles(pos, playerId, unitId, hiddenTiles);
                pos = new Position(x, yAdd);
                if (isInside(pos))
                    addVisionAndUpdateDiscoveredTiles(pos, playerId, unitId, discoveredTiles);
            }
        }
        return new VisionTileChanges(discoveredTiles, hiddenTiles);
    }


    public List<Integer> getListOfPlayersWithVisionOnPosition(Position pos) {
        ArrayList<Integer> players = new ArrayList<>();
        for (VisionOrigin vision : activeVisionByPosition.get(pos)) {
            if (vision == null)
                continue;
            if (vision.hasVision())
                players.add(vision.getPlayerId());
        }
        return players;
    }

    public Set<EmpireEvent> getPossibleActions(EmpireUnit unit) throws EmpireMapException {
        Set<EmpireEvent> moves = new HashSet<>();
        var playerId = unit.getPlayerId();
        var position = unit.getPosition();

        var x0 = position.getX();
        var y0 = position.getY();

        var isTopUnit = getTile(position).getOccupants().peek().getId().equals(unit.getId());

        if (!unit.isIdle()) {
            if (unit.isFighting())
                moves.add(new CombatStopOrder(unit.getId()));

            if (unit.isMoving())
                moves.add(new MovementStopOrder(unit.getId()));
        }

        for (int x = x0 - 1; x <= x0 + 1; x++) {
            for (int y = y0 - 1; y <= y0 + 1; y++) {
                if (!isInside(x, y))
                    continue;
                if (x == x0 && y == y0)
                    continue;
                var tile = getTile(x, y);
                if (tile == null)
                    moves.add(new MovementStartOrder(unit, new Position(x, y)));
                else {
                    if (tile.getMaxOccupants() == 0)
                        continue;
                    if (isMovementPossible(x, y, playerId))
                        moves.add(new MovementStartOrder(unit, new Position(x, y)));
                    else if (isTopUnit && playerId != tile.getPlayerId() && tile.getMaxOccupants() != 0 && tile.getOccupants().size() > 0) {
                        var enemy = tile.getOccupants().peek();
                        moves.add(new CombatStartOrder(unit.getId(), enemy.getId()));
                    }
                }
            }
        }
        return moves;
    }

    @Override
    public String toString() {

        StringBuilder s = new StringBuilder();
        s.append("\nEmpire map state: \n\n");
        s.append("-".repeat(mapSize.getWidth() * 4));
        s.append("|\n");
        for (EmpireTerrain[] empireTile : empireTiles) {
            StringBuilder firstLine = new StringBuilder();
            StringBuilder secondLine = new StringBuilder();
            StringBuilder thirdLine = new StringBuilder();
            for (EmpireTerrain tile : empireTile) {
                if (tile == null) {
                    firstLine.append("|   ");
                    secondLine.append("|   ");
                    thirdLine.append("|   ");
                } else {
                    firstLine.append(tile.mapStringFirstLine());
                    secondLine.append(tile.mapStringSecondLine());
                    thirdLine.append(tile.mapStringThirdLine());
                }
            }
            s.append(firstLine);
            s.append("|\n");
            s.append(secondLine);
            s.append("|\n");
            s.append(thirdLine);
            s.append("|\n");
            s.append("-".repeat(mapSize.getWidth() * 4));
            s.append("|\n");

        }
        return s.toString();
    }
}
