package at.ac.tuwien.ifs.sge.game.empire.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Position implements Serializable {

    private int x;
    private int y;

    public Position() {
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(Position position) {
        this(position.x, position.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Position getLeft() {
        return new Position(x - 1, y);
    }

    public Position getUpper() {
        return new Position(x, y - 1);
    }

    public Position getRight() {
        return new Position(x + 1, y);
    }

    public Position getLower() {
        return new Position(x, y + 1);
    }

    public Position getLeftUpper() {
        return new Position(x - 1, y - 1);
    }
    public Position getRightUpper() {
        return new Position(x + 1, y - 1);
    }
    public Position getLeftLower() {
        return new Position(x - 1, y + 1);
    }
    public Position getRightLower() {
        return new Position(x + 1, y + 1);
    }

    public ArrayList<Position> getAllNeighbours()
    {
        return new ArrayList<>(List.of(getLeft(), getLeftUpper(), getUpper(), getRightUpper(), getRight(), getRightLower(), getLower(), getLeftLower()));
    }

    public Position subtract(Position position) {
        return new Position(x - position.x, y - position.y);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Position(" + x + "," + y + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
