package at.ac.tuwien.ifs.sge.game.empire.map;

import java.io.Serializable;


public class Size implements Serializable {

    private int width;
    private int height;

    public Size() {}
    
    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
