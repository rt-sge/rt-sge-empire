package at.ac.tuwien.ifs.sge.game.empire.map;

import java.util.*;

/**
 * {@link VisionOrigin} is used in a map and therefore bound to a key, which is a position.
 * So it represents from which certain origins a player has vision on a position.
 */
public class VisionOrigin {
    private final int playerId;
    private final Map<UUID, Position> origins;

    public VisionOrigin(int playerId) {
        this.playerId = playerId;
        this.origins = new HashMap<>();
    }
    public VisionOrigin(VisionOrigin origin)
    {
        this(origin.playerId, origin.origins);
    }

    public VisionOrigin(int playerId, Map<UUID, Position> origins) {
        this.playerId = playerId;
        this.origins = new HashMap<>();
        origins.forEach((value, position) -> {
            this.origins.put(value, new Position(position));
        });
    }

    public int getPlayerId() {
        return playerId;
    }

    public boolean hasVision() {
        return origins.size() > 0;
    }

    public Map<UUID, Position> getOrigins() {
        return origins;
    }

    @Override
    public String toString() {
        return "Has vision: " + hasVision();
    }
}
