package at.ac.tuwien.ifs.sge.game.empire.map;

import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;

import java.util.ArrayList;
import java.util.HashMap;

public class VisionTileChanges {
    private final HashMap<Position, EmpireTerrain> discoveredTiles;
    private final ArrayList<Position> hiddenTiles;

    public VisionTileChanges(HashMap<Position, EmpireTerrain> discoveredTiles, ArrayList<Position> hiddenTiles) {
        this.discoveredTiles = discoveredTiles;
        this.hiddenTiles = hiddenTiles;
    }

    public HashMap<Position, EmpireTerrain> getDiscoveredTiles() {
        return discoveredTiles;
    }

    public ArrayList<Position> getHiddenTiles() {
        return hiddenTiles;
    }
}
