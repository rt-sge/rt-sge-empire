package at.ac.tuwien.ifs.sge.game.empire.model.configuration;

import java.io.Serializable;

public class AnimationConfig implements Serializable {

    private String name;
    private int speed;
    private int frames;
    private boolean anchors = true;
    private double scale = 1;

    public AnimationConfig() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getFrames() {
        return frames;
    }

    public void setFrames(int frames) {
        this.frames = frames;
    }

    public boolean hasAnchors() {
        return anchors;
    }

    public void setAnchors(boolean anchors) {
        this.anchors = anchors;
    }

    public boolean isAnchors() {
        return anchors;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }
}
