package at.ac.tuwien.ifs.sge.game.empire.model.configuration;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.representer.Representer;

public class EmpireYaml {
    public static Yaml getYaml(Class<?> clazz) {
        Constructor constructor = new Constructor(clazz);
        Representer representer = new Representer();
        representer.getPropertyUtils().setSkipMissingProperties(true);
        return new Yaml(constructor, representer);
    }
}
