package at.ac.tuwien.ifs.sge.game.empire.model.configuration;

import java.io.Serializable;

public class RandomMapGeneratorConfig implements Serializable {
    private int maxCitiesPerPlayer;
    private double cityX1;
    private double cityY0;
    private double cityY1;
    private double cityC;

    private double mountainProbability;
    private double mountainX1;
    private double mountainY0;
    private double mountainY1;


    public RandomMapGeneratorConfig() {
    }

    public RandomMapGeneratorConfig(int maxCitiesPerPlayer, double cityX1, double cityY0, double cityY1, double cityC, double mountainProbability, double mountainX1, double mountainY0, double mountainY1) {
        this.maxCitiesPerPlayer = maxCitiesPerPlayer;
        this.cityX1 = cityX1;
        this.cityY0 = cityY0;
        this.cityY1 = cityY1;
        this.cityC = cityC;
        this.mountainProbability = mountainProbability;
        this.mountainX1 = mountainX1;
        this.mountainY0 = mountainY0;
        this.mountainY1 = mountainY1;
    }

    public int getMaxCitiesPerPlayer() {
        return maxCitiesPerPlayer;
    }

    public void setMaxCitiesPerPlayer(int maxCitiesPerPlayer) {
        this.maxCitiesPerPlayer = maxCitiesPerPlayer;
    }

    public double getCityX1() {
        return cityX1;
    }

    public void setCityX1(double cityX1) {
        this.cityX1 = cityX1;
    }

    public double getCityY0() {
        return cityY0;
    }

    public void setCityY0(double cityY0) {
        this.cityY0 = cityY0;
    }

    public double getCityY1() {
        return cityY1;
    }

    public void setCityY1(double cityY1) {
        this.cityY1 = cityY1;
    }

    public double getMountainProbability() {
        return mountainProbability;
    }

    public void setMountainProbability(double mountainProbability) {
        this.mountainProbability = mountainProbability;
    }

    public double getMountainX1() {
        return mountainX1;
    }

    public void setMountainX1(double mountainX1) {
        this.mountainX1 = mountainX1;
    }

    public double getMountainY0() {
        return mountainY0;
    }

    public void setMountainY0(double mountainY0) {
        this.mountainY0 = mountainY0;
    }

    public double getMountainY1() {
        return mountainY1;
    }

    public void setMountainY1(double mountainY1) {
        this.mountainY1 = mountainY1;
    }

    public double getCityC() {
        return cityC;
    }

    public void setCityC(double cityC) {
        this.cityC = cityC;
    }
}
