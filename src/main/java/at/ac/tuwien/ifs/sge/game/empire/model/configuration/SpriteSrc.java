package at.ac.tuwien.ifs.sge.game.empire.model.configuration;

import java.io.Serializable;

public class SpriteSrc implements Serializable {

    private String src;
    private boolean embedded = false;

    public SpriteSrc() {
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public boolean isEmbedded() {
        return embedded;
    }

    public void setEmbedded(boolean embedded) {
        this.embedded = embedded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpriteSrc spriteSrc = (SpriteSrc) o;

        if (embedded != spriteSrc.embedded) return false;
        return src.equals(spriteSrc.src);
    }

    @Override
    public int hashCode() {
        int result = src.hashCode();
        result = 31 * result + (embedded ? 1 : 0);
        return result;
    }
}
