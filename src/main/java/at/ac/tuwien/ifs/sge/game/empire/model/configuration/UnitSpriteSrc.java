package at.ac.tuwien.ifs.sge.game.empire.model.configuration;

public class UnitSpriteSrc extends SpriteSrc {

    private AnimationConfig idleAnimation;
    private AnimationConfig attackAnimation;
    private AnimationConfig movementAnimation;

    public UnitSpriteSrc() {}

    public AnimationConfig getIdleAnimation() {
        return idleAnimation;
    }

    public void setIdleAnimation(AnimationConfig idleAnimation) {
        this.idleAnimation = idleAnimation;
    }

    public AnimationConfig getAttackAnimation() {
        return attackAnimation;
    }

    public void setAttackAnimation(AnimationConfig attackAnimation) {
        this.attackAnimation = attackAnimation;
    }

    public AnimationConfig getMovementAnimation() {
        return movementAnimation;
    }

    public void setMovementAnimation(AnimationConfig movementAnimation) {
        this.movementAnimation = movementAnimation;
    }
}
