package at.ac.tuwien.ifs.sge.game.empire.model.map;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;

/**
 * {@link EmpireCity} extends {@link EmpireTerrain} and provides an additional state which is needed for production.
 */
public class EmpireCity extends EmpireTerrain {

    private EmpireProductionState state;

    public EmpireCity(EmpireCity city)
    {
        super(city);
        this.state = city.state;
    }

    public EmpireCity(EmpireTerrainType terrain, Position position) {
        super(terrain, position);
        if (terrain.getMapIdentifier() != 'c')
            throw new IllegalArgumentException("False terrain passed to Empire City. Terrain has to be a city");
        this.state = EmpireProductionState.Idle;
    }

    public EmpireProductionState getState() {
        return state;
    }

    public void setState(EmpireProductionState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "City: " + name + " " + position + " OccupiedBy: " + this.getPlayerId()
            + " Occupants: " + occupants.size();
    }
}
