package at.ac.tuwien.ifs.sge.game.empire.model.map;

/**
 * {@link EmpireProductionState} represents all production states an {@link EmpireCity} can have in a game.
 */
public enum EmpireProductionState {
    Idle,
    Producing
}
