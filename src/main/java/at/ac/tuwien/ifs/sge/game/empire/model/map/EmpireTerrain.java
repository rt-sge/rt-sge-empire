package at.ac.tuwien.ifs.sge.game.empire.model.map;

import static at.ac.tuwien.ifs.sge.game.empire.util.EmpireConsoleColor.ANSI_RESET;

import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.util.EmpireConsoleColor;
import java.io.Serializable;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.function.ToDoubleFunction;

/**
 * {@link EmpireTerrain} represent all attributes a terrain has in empire.
 * Each terrain extends {@code EmpireUnitType} which provide the constant properties of the terrain.
 * {@code playerId} is -1 if the terrain is unoccupied.
 * {@code occupants} contains all {@link EmpireUnit} which are positioned on the field.
 * The unit with the highest damage rating (upper-bound) is listed first.
 */
public class EmpireTerrain extends EmpireTerrainType {
    public static final int UNOCCUPIED_ID = -1;
    public static final Comparator<EmpireUnit> occupantComparator = Comparator.comparingDouble((ToDoubleFunction<EmpireUnit> & Serializable) EmpireTerrain::orderOccupants).reversed();

    private static double orderOccupants(EmpireUnit empireUnit) {
        // if a unit is fighting it remains at the top of the stack
        return empireUnit.isFighting() ? Double.MAX_VALUE : empireUnit.getDamageRange().getUpperBound();
    }

    protected final Position position;
    protected PriorityQueue<EmpireUnit> occupants;

    public EmpireTerrain(EmpireTerrain terrain) {
        super(terrain);
        this.position = new Position(terrain.position);
        if (terrain.occupants != null) {
            this.occupants = new PriorityQueue<>(occupantComparator);
            this.occupants.addAll(terrain.occupants.stream().map(EmpireUnit::new).toList());
        }
    }


    public EmpireTerrain(EmpireTerrainType terrain, Position position) {
        super(terrain);
        this.position = position;
        if (maxOccupants > 0 || maxOccupants == -1)
            this.occupants = new PriorityQueue<>(occupantComparator);
    }


    public int getPlayerId() {
        if (this.occupants == null || this.occupants.size() == 0) {
            return EmpireTerrain.UNOCCUPIED_ID;
        } else {
            return this.occupants.peek().getPlayerId();
        }
    }

    public boolean isOccupied() {
        return getPlayerId() != EmpireTerrain.UNOCCUPIED_ID;
    }

    public Position getPosition() {
        return position;
    }

    public PriorityQueue<EmpireUnit> getOccupants() {
        return occupants;
    }

    @Override
    public String toString() {
        return super.toString() + " at " + position;
    }

    public String mapStringFirstLine() {
        var playerId = this.getPlayerId();
        if (playerId != -1) {
            return "|"
                    + EmpireConsoleColor.getColorForPlayer(playerId)
                    + occupants.peek().getUnitTypeName().substring(0,3)
                    + ANSI_RESET;
        }
        return "|   ";
    }

    public String mapStringSecondLine() {
        var playerId = this.getPlayerId();
        if (playerId != -1) {
            return "|"
                    + EmpireConsoleColor.getColorForPlayer(playerId)
                    + occupants.peek().getState().toString().substring(0,3)
                    + ANSI_RESET;
        }
        return "| " + mapIdentifier + " ";
    }

    public String mapStringThirdLine() {
        var playerId = this.getPlayerId();
        if (playerId != -1) {
            return "|"
                    + EmpireConsoleColor.getColorForPlayer(playerId)
                    + "H" + String.format("%2d", occupants.peek().getHp())
                    + ANSI_RESET;
        }
        return "|   ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmpireTerrain that = (EmpireTerrain) o;

        if (occupants.size() != that.getOccupants().size())
            return false;
        return position.equals(that.position);
    }
}
