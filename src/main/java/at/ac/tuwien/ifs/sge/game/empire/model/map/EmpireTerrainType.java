package at.ac.tuwien.ifs.sge.game.empire.model.map;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.SpriteSrc;

import java.io.Serializable;

/**
 * {@link EmpireTerrainType} represents the configuration of a {@link EmpireTerrain}.
 * It defines every constant aspect of an terrain. Therefore the defined values don't change in an ongoing game.
 * The speed multiplier defines how fast a {@code EmpireUnit} can move or the terrain. E.g. speed 1.5 means that the unit moves 1.5 times faster.
 * Whereas speed 0 means that is impassable.
 *
 */
public class EmpireTerrainType implements Serializable {
    protected String name;
    protected double speed;
    protected char mapIdentifier;
    protected int maxOccupants; // -1 == inf
    protected SpriteSrc spriteSrc;

    public EmpireTerrainType() {
    }

    public EmpireTerrainType(String name, double speed, char mapIdentifier, int maxOccupants) {
        this.name = name;
        this.speed = speed;
        this.mapIdentifier = mapIdentifier;
        this.maxOccupants = maxOccupants;
    }

    public EmpireTerrainType(EmpireTerrainType terrain) {
        this.name = terrain.name;
        this.speed = terrain.speed;
        this.mapIdentifier = terrain.mapIdentifier;
        this.maxOccupants = terrain.maxOccupants;
        this.spriteSrc = terrain.spriteSrc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public char getMapIdentifier() {
        return mapIdentifier;
    }

    public void setMapIdentifier(char mapIdentifier) {
        this.mapIdentifier = mapIdentifier;
    }

    public int getMaxOccupants() {
        return maxOccupants;
    }

    public void setMaxOccupants(int maxOccupants) {
        this.maxOccupants = maxOccupants;
    }

    public SpriteSrc getSpriteSrc() {
        return spriteSrc;
    }

    public void setSpriteSrc(SpriteSrc spriteSrc) {
        this.spriteSrc = spriteSrc;
    }

    @Override
    public String toString() {
        return "|" + name + "|";
    }
}
