package at.ac.tuwien.ifs.sge.game.empire.model.units;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * {@link EmpireDamageRange} represents the damage profile of an {@link EmpireUnit}
 * The damage is defined through and upper und lower bound.
 */
public class EmpireDamageRange implements Serializable {
    private double lowerBound;
    private double upperBound;

    public EmpireDamageRange() {}

    public EmpireDamageRange(double lowerBound, double upperBound) {
        if (lowerBound > upperBound)
            throw new InvalidParameterException("lowerBound is bigger than upperBound");
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;

    }

    public EmpireDamageRange(EmpireDamageRange damageRange) {
        this(damageRange.lowerBound, damageRange.upperBound);
    }

    public double getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(double lowerBound) {
        this.lowerBound = lowerBound;
    }

    public double getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(double upperBound) {
        this.upperBound = upperBound;
    }

    public int getNextDamage() {
        return ThreadLocalRandom.current().ints((int) lowerBound, (int) upperBound + 1).findFirst().getAsInt();
    }
    public boolean damageInBounds(int dmg)
    {
        return dmg >= lowerBound && dmg <= upperBound;
    }

    @Override
    public String toString() {
        return "DmgRange(" + lowerBound + ", " + upperBound + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmpireDamageRange that = (EmpireDamageRange) o;

        if (Double.compare(that.lowerBound, lowerBound) != 0) return false;
        return Double.compare(that.upperBound, upperBound) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lowerBound);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(upperBound);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
