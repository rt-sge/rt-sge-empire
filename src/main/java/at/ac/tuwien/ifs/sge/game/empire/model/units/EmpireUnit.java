package at.ac.tuwien.ifs.sge.game.empire.model.units;

import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import java.util.Objects;
import java.util.UUID;

/**
 * {@link EmpireUnit} represent all attributes a unit has in empire.
 * Each unit extents {@link EmpireUnitType} which provides the constant properties of the unit.
 * {@code name} and {@code consoleColor} are need for visualisation.
 */
public class EmpireUnit extends EmpireUnitType {
    private final UUID id;
    private int playerId;
    private int hp;
    private Position position;
    private EmpireUnitState state;
    private UUID targetId;
    private Position destination;

    private long timeOfLastStateChange = 0;

    public EmpireUnit(EmpireUnit unit) {
        super(unit.getUnitTypeId(), unit.getUnitTypeName(), unit.getMaxHp(), unit.getDamageRange(), unit.getHitsPerSecond(), unit.getFov(), unit.getTilesPerSecond(), unit.getProductionTime(), unit.getSpriteSrc());
        id = unit.id;
        playerId = unit.playerId;
        hp = unit.hp;
        if (unit.position != null)
            position = new Position(unit.position);
        state = unit.state;
        targetId = unit.targetId;
        if (unit.destination != null)
            destination = new Position(unit.destination);

        timeOfLastStateChange = unit.timeOfLastStateChange;
    }

    public EmpireUnit() {
        this.id = UUID.randomUUID();
    }

    public EmpireUnit(UUID id, int playerId, EmpireUnitType unitType, Position position) {
        super(unitType);
        this.id = id;
        this.playerId = playerId;
        this.hp = getMaxHp();
        this.position = position;
        this.state = EmpireUnitState.Idle;
        this.targetId = null;
        this.destination = null;

    }

    //region Getter & Setter

    public UUID getId() {
        return id;
    }

    public int getPlayerId() {
        return playerId;
    }

    public EmpireUnitState getState() {
        return state;
    }

    public void setState(EmpireUnitState state) {
        timeOfLastStateChange = System.currentTimeMillis();
        this.state = state;
    }

    public boolean isFighting() { return state.equals(EmpireUnitState.Fighting); }
    public boolean isMoving() { return state.equals(EmpireUnitState.Moving); }
    public boolean isIdle() { return state.equals(EmpireUnitState.Idle); }

    public long getTimeInCurrentState() {
        return System.currentTimeMillis() - timeOfLastStateChange;
    }

    public UUID getTargetId() {
        return targetId;
    }

    public void setTargetId(UUID targetId) {
        this.targetId = targetId;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Position getDestination() {
        return destination;
    }

    public void setDestination(Position destination) {
        this.destination = destination;
    }

    //endregion

    //region CombatMethods

    public void damageUnit(int amount) {
        hp -= amount;
    }

    public boolean isAlive() {
        return hp > 0;
    }

    public int getHp() {
        return hp;
    }

    public void revive() {
        hp = getMaxHp();
    }

    //endregion

    //region Standard Methods

    @Override
    public String toString() {
        return  getUnitTypeName() + " " + "(" + hp + "|" + state + "|" + "x:" + position.getX() + " y:" + position.getY() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EmpireUnit that = (EmpireUnit) o;

        return this.id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id);
    }

    //endregion
}
