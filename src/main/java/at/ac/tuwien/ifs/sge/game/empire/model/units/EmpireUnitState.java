package at.ac.tuwien.ifs.sge.game.empire.model.units;

/**
 * {@link EmpireUnitState} represents all states an {@link EmpireUnit} can have in a game.
 */
public enum EmpireUnitState {
    Idle,
    Fighting,
    Moving
}
