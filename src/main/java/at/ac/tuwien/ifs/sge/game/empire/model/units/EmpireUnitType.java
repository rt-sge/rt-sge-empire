package at.ac.tuwien.ifs.sge.game.empire.model.units;

import at.ac.tuwien.ifs.sge.game.empire.model.configuration.UnitSpriteSrc;

import java.io.Serializable;

/**
 * {@link EmpireUnitType} represents the configuration of a {@link EmpireUnit}.
 * It defines every constant aspect of a unit. Therefore the defined values don't change in an ongoing game.
 */

public class EmpireUnitType implements Serializable {
    private int unitTypeId;
    private String unitTypeName;
    private int maxHp;
    private EmpireDamageRange damageRange;
    private double hitsPerSecond;
    private int fov;
    private double tilesPerSecond;
    private int productionTime;
    private UnitSpriteSrc spriteSrc;

    public EmpireUnitType() {

    }

    public EmpireUnitType(int unitTypeId, String unitTypeName, int maxHp, EmpireDamageRange damageRange, double hitsPerSecond, int fov, double tilesPerSecond, int productionTime, UnitSpriteSrc sprite) {
        this.unitTypeId = unitTypeId;
        this.unitTypeName = unitTypeName;
        this.maxHp = maxHp;
        this.damageRange = new EmpireDamageRange(damageRange);
        this.hitsPerSecond = hitsPerSecond;
        this.fov = fov;
        this.tilesPerSecond = tilesPerSecond;
        this.productionTime = productionTime;
        this.spriteSrc = sprite;
    }

    public EmpireUnitType(EmpireUnitType unitType) {
        this.unitTypeId = unitType.unitTypeId;
        this.unitTypeName = unitType.unitTypeName;
        this.maxHp = unitType.maxHp;
        this.damageRange = new EmpireDamageRange(unitType.damageRange);
        this.hitsPerSecond = unitType.hitsPerSecond;
        this.fov = unitType.fov;
        this.tilesPerSecond = unitType.tilesPerSecond;
        this.productionTime = unitType.productionTime;
        this.spriteSrc = unitType.spriteSrc;
    }


//region Getter & Setter


    public int getUnitTypeId() {
        return unitTypeId;
    }

    public void setUnitTypeId(int unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public String getUnitTypeName() {
        return unitTypeName;
    }

    public void setUnitTypeName(String unitTypeName) {
        this.unitTypeName = unitTypeName;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public EmpireDamageRange getDamageRange() {
        return damageRange;
    }

    public void setDamageRange(EmpireDamageRange damageRange) {
        this.damageRange = damageRange;
    }

    public double getHitsPerSecond() {
        return hitsPerSecond;
    }

    public void setHitsPerSecond(double hitsPerSecond) {
        this.hitsPerSecond = hitsPerSecond;
    }

    public int getFov() {
        return fov;
    }

    public void setFov(int fov) {
        this.fov = fov;
    }

    public double getTilesPerSecond() {
        return tilesPerSecond;
    }

    public void setTilesPerSecond(double tilesPerSecond) {
        this.tilesPerSecond = tilesPerSecond;
    }

    public int getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(int productionTime) {
        this.productionTime = productionTime;
    }

    public UnitSpriteSrc getSpriteSrc() {
        return spriteSrc;
    }

    public void setSpriteSrc(UnitSpriteSrc spriteSrc) {
        this.spriteSrc = spriteSrc;
    }

    //endregion


    @Override
    public String toString() {
        return unitTypeName;
        //return "UnitType: " + name + ", " + hp + "\u2764, " + hitsPerSecond + "/s " + damageRange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmpireUnitType that = (EmpireUnitType) o;

        if (unitTypeId != that.unitTypeId) return false;
        if (maxHp != that.maxHp) return false;
        if (hitsPerSecond != that.hitsPerSecond) return false;
        if (fov != that.fov) return false;
        if (Double.compare(that.tilesPerSecond, tilesPerSecond) != 0) return false;
        if (productionTime != that.productionTime) return false;
        if (!unitTypeName.equals(that.unitTypeName)) return false;
        if (!damageRange.equals(that.damageRange)) return false;
        return spriteSrc != null ? spriteSrc.equals(that.spriteSrc) : that.spriteSrc == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = unitTypeId;
        result = 31 * result + unitTypeName.hashCode();
        result = 31 * result + maxHp;
        result = 31 * result + damageRange.hashCode();
        result = (int) (31 * result + hitsPerSecond);
        result = 31 * result + fov;
        temp = Double.doubleToLongBits(tilesPerSecond);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + productionTime;
        result = 31 * result + (spriteSrc != null ? spriteSrc.hashCode() : 0);
        return result;
    }
}
