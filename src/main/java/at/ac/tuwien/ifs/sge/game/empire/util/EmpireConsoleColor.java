package at.ac.tuwien.ifs.sge.game.empire.util;

public class EmpireConsoleColor {
    public static String getColorForPlayer(int playerId)
    {
        return switch (playerId){
            case 0 -> ANSI_BLUE;
            case 1 -> ANSI_BRIGHT_YELLOW;
            case 2 -> ANSI_CYAN;
            case 3 -> ANSI_PURPLE;
            case 4 -> ANSI_BRIGHT_GREEN;
            case 5 -> ANSI_BRIGHT_RED;
            case 6 -> ANSI_BRIGHT_BLUE;
            case 7 -> ANSI_YELLOW;
            case 8 -> ANSI_BRIGHT_CYAN;
            case 9 -> ANSI_GREEN;
            case 10 -> ANSI_MAGENTA;
            case 11 -> ANSI_RED;
            default -> "";
        };
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_BRIGHT_RED = "\u001B[91m";
    public static final String ANSI_BRIGHT_GREEN = "\u001B[92m";
    public static final String ANSI_BRIGHT_YELLOW = "\u001B[93m";
    public static final String ANSI_BRIGHT_BLUE = "\u001B[94m";
    public static final String ANSI_MAGENTA = "\u001B[95m";
    public static final String ANSI_BRIGHT_CYAN = "\u001B[96m";
}
