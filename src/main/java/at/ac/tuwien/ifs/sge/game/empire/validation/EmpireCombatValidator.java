package at.ac.tuwien.ifs.sge.game.empire.validation;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.CombatStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireCombatException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * {@link EmpireCombatValidator} is a validator used to validate combat related events in empire.
 * Each method of the validator validates a specific event and returns a boolean, the value of which represents its validity.
 * {@link EmpireCombatValidator} can validate following events
 * <ul>
 *      <li>{@link CombatStartOrder}
 *      <li>{@link CombatStopOrder}
 *      <li>{@link CombatHitAction}
 * </ul>
 */

public final class EmpireCombatValidator extends EmpireValidator{

    private final Map<UUID, EmpireUnit> unitsById;
    private final EmpireMap empireMap;
    private final Map<Integer, EmpireUnitType> unitTypesById;
    private final List<Integer> players;

    /**
     */
    public EmpireCombatValidator(
        Map<UUID, EmpireUnit> unitsById,
        EmpireMap empireMap,
        Map<Integer, EmpireUnitType> unitTypesById,
        List<Integer> players) {
        this.unitsById = unitsById;
        this.empireMap = empireMap;
        this.unitTypesById = unitTypesById;
        this.players = players;
    }

    public void validateCombatStartOrder(CombatStartOrder order) throws EmpireCombatException, EmpireMapException {
        var attackerExisting = isUnitExisting(order.getAttackerId());
        var defenderExisting = isUnitExisting(order.getTargetId());

        if (!attackerExisting)
            throw new EmpireCombatException("Attacker can't be found! Combat: " + order);

        if (!defenderExisting)
            throw new EmpireCombatException("Target can't be found! Combat: " + order);

        var attacker = unitsById.get(order.getAttackerId());
        var target = unitsById.get(order.getTargetId());
        validateCombat(attacker, target);
    }

    public boolean verifyCombatStartOrder(CombatStartOrder order, int activePlayerId) {
        if (!isUnitExisting(order.getAttackerId()) && !(activePlayerId == -1 || players.contains(activePlayerId)))
            return false;
        if (activePlayerId == GameActionEvent.INTERNAL_EVENT_ID)
            return true;
        return isUnitOfPlayer(order.getAttackerId(), activePlayerId);
    }

    public void validateCombatStopOrder(CombatStopOrder order) throws EmpireCombatException {
        var attackerExisting = isUnitExisting(order.getAttackerId());
        if (!attackerExisting)
            throw new EmpireCombatException("Attacker can't be found! Combat: " + order);

        var attacker = unitsById.get(order.getAttackerId());
        if (!attacker.isFighting())
            throw new EmpireCombatException("Attacker is not fighting! So it can't be stopped!" + " Attacker: " + attacker);

        if (!attacker.isAlive())
            throw new EmpireCombatException("Attacker is not alive! " + " Attacker: " + attacker);
    }

    public boolean verifyCombatStopOrder(CombatStopOrder order, int activePlayerId) {
        if (!isUnitExisting(order.getAttackerId()) && !(activePlayerId == -1 || players.contains(activePlayerId)))
            return false;
        if (activePlayerId == GameActionEvent.INTERNAL_EVENT_ID)
            return true;
        return isUnitOfPlayer(order.getAttackerId(), activePlayerId);
    }

    public void validateHitAction(CombatHitAction action) throws EmpireCombatException, EmpireMapException {
        var attackerExisting = isUnitExisting(action.getAttackerId());
        var defenderExisting = isUnitExisting(action.getTargetId());

        if (!attackerExisting)
            throw new EmpireCombatException("Attacker can't be found! Combat: " + action);

        if (!defenderExisting)
            throw new EmpireCombatException("Target can't be found! Combat: " + action);

        var attacker = unitsById.get(action.getAttackerId());
        var target = unitsById.get(action.getTargetId());

        validateCombat(attacker, target);

        var correctDamage = unitTypesById.get(attacker.getUnitTypeId()).getDamageRange().damageInBounds(action.getCalculatedDamage());
        if (!correctDamage)
            throw new EmpireCombatException("CombatHitAction delivered wrong damage value!" + " Dmg: " + action.getCalculatedDamage() + " Unit Type: " + attacker.getUnitTypeId());

        if (!attacker.isFighting())
            throw new EmpireCombatException("Attacker is not fighting! So it can't deal dmg through a CombatHitAction!" + " Action: " + action);
    }


    private boolean isUnitExisting(UUID id) {
        return unitsById.containsKey(id);
    }

    private boolean isUnitOfPlayer(UUID unitId, int playerId) {
        if (!isUnitExisting(unitId)) return false;
        return unitsById.get(unitId).getPlayerId() == playerId;
    }

    private void validateCombat(EmpireUnit attacker, EmpireUnit target) throws EmpireCombatException, EmpireMapException {
        var areAdjacent = empireMap.areAdjacent(attacker.getPosition(), target.getPosition());
        if (!areAdjacent)
            throw new EmpireCombatException("Units are not adjacent! " + " Attacker: " + attacker + " Target: " + target);

        var attackerIsAlive = attacker.isAlive();
        var targetIsAlive = target.isAlive();
        if (!attackerIsAlive)
            throw new EmpireCombatException("Attacker is not alive! " + " Attacker: " + attacker);
        if (!targetIsAlive)
            throw new EmpireCombatException("Target is not alive! " + " Target: " + target);

        var areEnemies = attacker.getPlayerId() != target.getPlayerId();
        if (!areEnemies)
            throw new EmpireCombatException("Units are of the same team! No friendly fire possible!" + " Attacker: " + attacker + " Target: " + target);

        var targetTopOccupant = empireMap.getTile(target.getPosition()).getOccupants().peek().getId().equals(target.getId());
        var attackerTopOccupant = empireMap.getTile(attacker.getPosition()).getOccupants().peek().getId().equals(attacker.getId());

        if (!targetTopOccupant)
            throw new EmpireCombatException("Target isn't at the top of the tile! Only the unit on top of the tile can be attacked!" + " Target: " + target);
        if (!attackerTopOccupant)
            throw new EmpireCombatException("Attacker isn't at the top of the tile! A unit can only attack if it is on top of all occupants!" + " Attacker: " + attacker);
    }

    public Map<UUID, EmpireUnit> unitsById() {
        return unitsById;
    }

    public EmpireMap empireMap() {
        return empireMap;
    }

    public Map<Integer, EmpireUnitType> unitTypesById() {
        return unitTypesById;
    }

    public List<Integer> players() {
        return players;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (EmpireCombatValidator) obj;
        return Objects.equals(this.unitsById, that.unitsById) &&
                Objects.equals(this.empireMap, that.empireMap) &&
                Objects.equals(this.unitTypesById, that.unitTypesById) &&
                Objects.equals(this.players, that.players);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unitsById, empireMap, unitTypesById, players);
    }

    @Override
    public String toString() {
        return "EmpireCombatValidator[" +
                "unitsById=" + unitsById + ", " +
                "empireMap=" + empireMap + ", " +
                "unitTypesById=" + unitTypesById + ", " +
                "players=" + players + ']';
    }


    @Override
    public void validate(EmpireEvent event) throws EmpireCombatException, EmpireMapException {
        if (event instanceof CombatStartOrder order) {
            validateCombatStartOrder(order);
        } else if (event instanceof CombatStopOrder order) {
            validateCombatStopOrder(order);
        } else if (event instanceof CombatHitAction hit) {
            validateHitAction(hit);
        }
    }
}
