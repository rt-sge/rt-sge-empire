package at.ac.tuwien.ifs.sge.game.empire.validation;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.MovementStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMovementException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;


/**
 * {@link EmpireMovementValidator} is a validator used to validate movement related events in empire.
 * Each method of the validator validates a specific event and returns a boolean, the value of which represents its validity.
 * {@link EmpireMovementValidator} can validate following events
 * <ul>
 *      <li>{@link MovementStartOrder}
 *      <li>{@link MovementStopOrder}
 *      <li>{@link MovementAction}
 * </ul>
 */
public final class EmpireMovementValidator extends EmpireValidator{

    private final Map<UUID, EmpireUnit> unitsById;
    private final EmpireMap map;
    private final Map<Integer, EmpireUnitType> unitTypesById;
    private final List<Integer> players;

    /**
     */
    public EmpireMovementValidator(
        Map<UUID, EmpireUnit> unitsById,
        EmpireMap map,
        Map<Integer, EmpireUnitType> unitTypesById,
        List<Integer> players) {
        this.unitsById = unitsById;
        this.map = map;
        this.unitTypesById = unitTypesById;
        this.players = players;
    }

    public void validateMovementStartOrder(MovementStartOrder order) throws EmpireMovementException, EmpireMapException {
        existingAndAlive(order.getUnitId(), order);

        var unit = unitsById.get(order.getUnitId());
        validateMovement(order.getDestination(), unit.getPlayerId());

        var adjacent = map.areAdjacent(unit.getPosition(), order.getDestination());
        if (!adjacent)
            throw new EmpireMovementException("Unit position and destination are not adjacent! Movement: " + order);
    }

    public boolean verifyMovementStartOrder(MovementStartOrder order, int activePlayerId) {
        if (!isUnitExisting(order.getUnitId()) && !(activePlayerId == -1 || players.contains(activePlayerId)))
            return false;
        if (activePlayerId == GameActionEvent.INTERNAL_EVENT_ID)
            return true;
        return isUnitOfPlayer(order.getUnitId(), activePlayerId);
    }

    public void validateMovementStopOrder(MovementStopOrder order) throws EmpireMovementException {
        existingAndAlive(order.getUnitId(), order);

        var unit = unitsById.get(order.getUnitId());

        if (!unit.isMoving())
            throw new EmpireMovementException("Unit is not in moving state. Therefore movement is not possible! Movement: " + order);
    }

    public boolean verifyMovementStopOrder(MovementStopOrder order, int activePlayerId) {
        if (!isUnitExisting(order.getUnitId()) && !(activePlayerId == -1 || players.contains(activePlayerId)))
            return false;
        if (activePlayerId == GameActionEvent.INTERNAL_EVENT_ID)
            return true;
        return isUnitOfPlayer(order.getUnitId(), activePlayerId);
    }

    public void validateMovementAction(MovementAction action) throws EmpireMovementException, EmpireMapException {
        existingAndAlive(action.getUnitId(), action);

        var unit = unitsById.get(action.getUnitId());

        validateMovement(action.getDestination(), unit.getPlayerId());

        if (!unit.isMoving())
            throw new EmpireMovementException("Unit is not in moving state. Therefore movement is not possible!");

        var adjacent = map.areAdjacent(unit.getPosition(), action.getDestination());
        if (!adjacent)
            throw new EmpireMovementException("Unit position and destination are not adjacent! Movement: " + action);
    }

    private void existingAndAlive(UUID unitId, EmpireEvent event) throws EmpireMovementException {
        var existing = isUnitExisting(unitId);
        if (!existing) {
            throw new EmpireMovementException("Unit can't be found! Movement: " + event,
                (MovementAction) event);
        }

        var unit = unitsById.get(unitId);
        var alive = unit.isAlive();
        if (!alive)
            throw new EmpireMovementException("Unit is not alive. Therefore movement is not possible! Movement: " + event);
    }

    private void validateMovement(Position position, int playerId) throws EmpireMapException {
        var tile = map.getTile(position);
        if (tile == null)
            throw new EmpireMapException("Tile is null! This can happen during simulation due to missing vision!");
        var enemyTerritory = tile.getPlayerId() != playerId && !(tile.getPlayerId() == EmpireCity.UNOCCUPIED_ID);
        if (enemyTerritory)
            throw new EmpireMapException("Tile is already occupied by the enemy!");
        if (tile.getMaxOccupants() == 0)
            throw new EmpireMapException("Tile can not be occupied!");
        var full = (tile.getMaxOccupants() > tile.getOccupants().size()) || (tile.getMaxOccupants() == -1);
        if (!full)
            throw new EmpireMapException("Tile has the maximum amount of occupants!");
    }

    private boolean isUnitExisting(UUID id) {
        return unitsById.containsKey(id);
    }

    private boolean isUnitOfPlayer(UUID unitId, int playerId) {
        if (!isUnitExisting(unitId)) return false;
        return unitsById.get(unitId).getPlayerId() == playerId;
    }

    public Map<UUID, EmpireUnit> unitsById() {
        return unitsById;
    }

    public EmpireMap map() {
        return map;
    }

    public Map<Integer, EmpireUnitType> unitTypesById() {
        return unitTypesById;
    }

    public List<Integer> players() {
        return players;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (EmpireMovementValidator) obj;
        return Objects.equals(this.unitsById, that.unitsById) &&
                Objects.equals(this.map, that.map) &&
                Objects.equals(this.unitTypesById, that.unitTypesById) &&
                Objects.equals(this.players, that.players);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unitsById, map, unitTypesById, players);
    }

    @Override
    public String toString() {
        return "EmpireMovementValidator[" +
                "unitsById=" + unitsById + ", " +
                "map=" + map + ", " +
                "unitTypesById=" + unitTypesById + ", " +
                "players=" + players + ']';
    }


    @Override
    public void validate(EmpireEvent event) throws EmpireMapException, EmpireMovementException {
        if (event instanceof MovementStartOrder order) {
            validateMovementStartOrder(order);
        } else if (event instanceof MovementStopOrder order) {
            validateMovementStopOrder(order);
        } else if (event instanceof MovementAction move) {
            validateMovementAction(move);
        }
    }
}
