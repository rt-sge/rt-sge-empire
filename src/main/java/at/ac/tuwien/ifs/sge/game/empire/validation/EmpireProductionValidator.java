package at.ac.tuwien.ifs.sge.game.empire.validation;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.ProductionStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireProductionException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * {@link EmpireProductionValidator} is a validator used to validate production related events in empire.
 * Each method of the validator validates a specific event and returns a boolean, the value of which represents its validity.
 * {@link EmpireProductionValidator} can validate following events
 * <ul>
 *      <li>{@link ProductionStartOrder}
 *      <li>{@link ProductionStopOrder}
 *      <li>{@link ProductionAction}
 * </ul>
 */
public final class EmpireProductionValidator extends EmpireValidator {

    private final int unitCap;
    private final Map<Position, EmpireCity> citiesByPosition;
    private final Map<Integer, List<EmpireUnit>> unitsByPlayer;
    private final EmpireMap map;
    private final Map<Integer, EmpireUnitType> unitTypesById;
    private final List<Integer> players;

    /**
     *
     */
    public EmpireProductionValidator(
        int unitCap,
        Map<Position, EmpireCity> citiesByPosition,
        Map<Integer, List<EmpireUnit>> unitsByPlayer,
        EmpireMap map,
        Map<Integer, EmpireUnitType> unitTypesById,
        List<Integer> players) {
        this.unitCap = unitCap;
        this.unitsByPlayer = unitsByPlayer;
        this.citiesByPosition = citiesByPosition;
        this.map = map;
        this.unitTypesById = unitTypesById;
        this.players = players;
    }

    public void validateProductionStartOrder(ProductionStartOrder order) throws EmpireProductionException {
        var exists = cityExists(order.getCityPosition());
        var city = citiesByPosition.get(order.getCityPosition());
        var nrOfUnits = unitsByPlayer.get(city.getPlayerId()).size();
        if (nrOfUnits >= unitCap)
            throw new EmpireProductionException("Unit cap is reached!");
        if (!exists)
            throw new EmpireProductionException("City does not exist!" + " City position: " + order.getCityPosition());
        var unitTypeExists = unitTypeExists(order.getUnitTypeId());
        if (!unitTypeExists)
            throw new EmpireProductionException("UnitType does not exist!" + " Unit type: " + order.getUnitTypeId());
    }

    public boolean verifyProductionStartOrder(ProductionStartOrder order, int activePlayerId) {
        var entitiesExist = cityExists(order.getCityPosition()) && (activePlayerId == -1 || players.contains(activePlayerId)) && unitTypeExists(order.getUnitTypeId());
        if (!entitiesExist)
            return false;
        if (activePlayerId == GameActionEvent.INTERNAL_EVENT_ID)
            return isOccupied(order.getCityPosition());
        if (!isOccupiedByPlayer(order.getCityPosition(), activePlayerId))
            return false;
        return citiesByPosition.get(order.getCityPosition()).getPlayerId() == activePlayerId;
    }

    public void validateProductionStopOrder(ProductionStopOrder order) throws EmpireProductionException {
        var exists = cityExists(order.getCityPosition());
        if (!exists)
            throw new EmpireProductionException("City does not exist!" + " City position: " + order.getCityPosition());
        var producing = citiesByPosition.get(order.getCityPosition()).getState().equals(EmpireProductionState.Producing);
        if (!producing)
            throw new EmpireProductionException("City is not in production state! Therefore it can't be stopped!" + " City position: " + order.getCityPosition());
    }

    public boolean verifyProductionStopOrder(ProductionStopOrder order, int activePlayerId) {
        var entitiesExist = citiesByPosition.containsKey(order.getCityPosition()) && (activePlayerId == -1 || players.contains(activePlayerId));
        if (!entitiesExist)
            return false;
        if (activePlayerId == GameActionEvent.INTERNAL_EVENT_ID)
            return true;
        return citiesByPosition.get(order.getCityPosition()).getPlayerId() == activePlayerId;
    }

    public void validateProductionAction(ProductionAction action) throws EmpireProductionException {
        var exists = cityExists(action.getCityPosition());
        if (!exists) {
            throw new EmpireProductionException(
                "City does not exist!" + " City position: " + action.getCityPosition());
        }

        var occupied = isOccupied(action.getCityPosition());
        if (!occupied) {
            throw new EmpireProductionException(
                "City is not occupied! Therefore production is not possible!" + " City position: "
                    + action.getCityPosition());
        }

        var city = citiesByPosition.get(action.getCityPosition());
        var nrOfUnits = unitsByPlayer.get(city.getPlayerId()).size();
        if (nrOfUnits >= unitCap) {
            throw new EmpireProductionException("Unit cap is reached!");
        }

        var unitTypeExists = unitTypeExists(action.getUnitTypeId());
        if (!unitTypeExists) {
            throw new EmpireProductionException(
                "UnitType does not exist!" + " Unit type: " + action.getUnitTypeId());
        }

        if (!action.isAgentUpdate()) {
            var isProducing = city.getState().equals(EmpireProductionState.Producing);
            if (!isProducing)
                throw new EmpireProductionException("City is not in production state! Therefore production is not possible!" + " City position: " + action.getCityPosition());

        }
    }

    public boolean validateInitialSpawnAction(InitialSpawnAction action) throws EmpireInternalException {
        if (!map.isInside(action.getPosition())) {
            throw new EmpireInternalException("Position out of bounds!");
        }
        if (!unitTypesById.containsKey(action.getUnitTypeId())) {
            throw new EmpireInternalException("Id for unit type not existing!");
        }
        return true;
    }

    private boolean isOccupied(Position cityId) {
        return citiesByPosition.get(cityId).getPlayerId() != EmpireTerrain.UNOCCUPIED_ID;
    }

    private boolean isOccupiedByPlayer(Position cityId, int playerId) {
        return citiesByPosition.get(cityId).getPlayerId() == playerId;
    }

    private boolean cityExists(Position position) {
        return citiesByPosition.containsKey(position);
    }

    private boolean unitTypeExists(int unitType) {
        return unitTypesById.containsKey(unitType);
    }

    public Map<Position, EmpireCity> citiesByPosition() {
        return citiesByPosition;
    }

    public EmpireMap map() {
        return map;
    }

    public Map<Integer, EmpireUnitType> unitTypesById() {
        return unitTypesById;
    }

    public List<Integer> players() {
        return players;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (EmpireProductionValidator) obj;
        return Objects.equals(this.citiesByPosition, that.citiesByPosition) &&
                Objects.equals(this.map, that.map) &&
                Objects.equals(this.unitTypesById, that.unitTypesById) &&
                Objects.equals(this.players, that.players);
    }

    @Override
    public int hashCode() {
        return Objects.hash(citiesByPosition, map, unitTypesById, players);
    }

    @Override
    public String toString() {
        return "EmpireProductionValidator[" +
                "citiesByPosition=" + citiesByPosition + ", " +
                "map=" + map + ", " +
                "unitTypesById=" + unitTypesById + ", " +
                "players=" + players + ']';
    }

    @Override
    public void validate(EmpireEvent event) throws EmpireProductionException {
        if (event instanceof ProductionStartOrder order) {
            validateProductionStartOrder(order);
        } else if (event instanceof ProductionStopOrder order) {
            validateProductionStopOrder(order);
        } else if (event instanceof ProductionAction production) {
            validateProductionAction(production);
        }
    }
}
