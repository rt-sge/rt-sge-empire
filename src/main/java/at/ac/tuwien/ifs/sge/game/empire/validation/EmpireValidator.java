package at.ac.tuwien.ifs.sge.game.empire.validation;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireCombatException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMovementException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireProductionException;

public abstract class EmpireValidator {
    public abstract void validate(EmpireEvent event) throws EmpireCombatException, EmpireMapException, EmpireProductionException, EmpireMovementException;
}
