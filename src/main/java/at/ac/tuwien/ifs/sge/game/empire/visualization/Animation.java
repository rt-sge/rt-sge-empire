package at.ac.tuwien.ifs.sge.game.empire.visualization;


public class Animation {

    private final Frame[] frames;

    private final double speedMs;
    private final double frameDuration;

    private double maxAnchorY = 0;

    public Animation(Frame[] frames, double speedMs) {
        this.frames = frames;
        this.speedMs = speedMs;
        frameDuration = speedMs / frames.length;
        for (var frame : frames)
            maxAnchorY = Math.max(maxAnchorY, frame.getAnchorY());
    }

    public Frame getFrame(double timeElapsedMs) {
        timeElapsedMs = timeElapsedMs % speedMs;
        var index = (int) (timeElapsedMs / frameDuration);
        return frames[index];
    }

    public double getMaxAnchorY() {
        return maxAnchorY;
    }
}
