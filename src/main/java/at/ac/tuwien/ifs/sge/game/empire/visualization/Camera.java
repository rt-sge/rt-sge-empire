package at.ac.tuwien.ifs.sge.game.empire.visualization;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.map.Size;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrain;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrainType;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

public class Camera {

    public static final EmpireTerrainType noVisionEmpireTerrainType = new EmpireTerrainType("novision", -1, 'x', 0);

    private EmpireMap map;
    private EmpireTerrain[][] empireTiles;

    private final double tileSize;
    private double zoom;
    private final double minZoom;
    private final double maxZoom;

    private Rectangle2D mapBorder;
    private Size mapSize;

    private double viewWidth;
    private double viewHeight;

    private Point2D lookAt;

    public Camera(Empire game, double tileSize, double minTileSize, double maxTileSize, double viewWidth, double viewHeight, Point2D lookAt) {
        this(null, game.getGameConfiguration().getMapSize(), tileSize, minTileSize, maxTileSize, viewWidth, viewHeight, lookAt);
        this.map = game.getBoard();
    }

    public Camera(EmpireTerrain[][] empireTiles, Size mapSize, double tileSize, double minTileSize, double maxTileSize, double viewWidth, double viewHeight, Point2D lookAt) {

        this.tileSize = tileSize;
        this.zoom = 0.5;
        this.minZoom = minTileSize / tileSize;
        this.maxZoom = maxTileSize / tileSize;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;

        updateMap(empireTiles, mapSize, lookAt);
    }

    public void updateMap(EmpireTerrain[][] empireTiles, Size mapSize, Point2D lookAt) {
        this.empireTiles = empireTiles;
        this.mapSize = mapSize;
        var mapWidth = mapSize.getWidth() * tileSize;
        var mapHeight = mapSize.getHeight() * tileSize;
        mapBorder = new Rectangle2D(0, 0, mapWidth, mapHeight);

        if (lookAt == null)
            this.lookAt = new Point2D(mapWidth / 2D,  mapHeight / 2D);
        else
            this.lookAt = lookAt;
    }

    public void setLookAt(double x, double y) {
        setLookAt(new Point2D(x, y));
    }
    public void setLookAt(Point2D lookAt) {
        this.lookAt = checkAndFixLookAtBoundaries(lookAt);
    }
    public Point2D getLookAt() {
        return lookAt;
    }

    public void setZoom(double zoom, double x, double y) {
        if (zoom > maxZoom) zoom = maxZoom;
        else if (zoom < minZoom) zoom = minZoom;
        // move look at to keep mouse on the same map position when zooming
        var mouseMapPosBefore = mapPositionFromViewPosition(x, y);
        var mouseMapBeforeOrigX = mouseMapPosBefore.getX() / this.zoom;
        var mouseMapBeforeOrigY = mouseMapPosBefore.getY() / this.zoom;
        this.zoom = zoom;
        var mouseMapPosAfter = mapPositionFromViewPosition(x, y);
        var mouseMapAfterOrigX = mouseMapPosAfter.getX() / this.zoom;
        var mouseMapAfterOrigY = mouseMapPosAfter.getY() / this.zoom;
        var dx = mouseMapBeforeOrigX - mouseMapAfterOrigX;
        var dy = mouseMapBeforeOrigY - mouseMapAfterOrigY ;
        lookAt = new Point2D(lookAt.getX() + dx, lookAt.getY() + dy);
        lookAt = checkAndFixLookAtBoundaries(lookAt);
    }

    public double getZoom() {
        return zoom;
    }

    public EmpireTerrain[][] getTerrainInView() {
        var terrain = map != null ? map.getEmpireTiles() : empireTiles;

        var startX = getTileIndex(lookAt.getX() * zoom - viewWidth / 2);
        var endX = getTileIndex(lookAt.getX() * zoom +  viewWidth / 2 );
        var startY = getTileIndex(lookAt.getY() * zoom - viewHeight / 2 );
        var endY = getTileIndex(lookAt.getY() * zoom + viewHeight / 2 );

        if (startX < 0) startX = 0;
        if (endX >= mapSize.getWidth()) endX = mapSize.getWidth() - 1;
        if (startY < 0) startY = 0;
        if (endY >= mapSize.getHeight()) endY = mapSize.getHeight() - 1;

        if (endX < startX || endY < startY) return new EmpireTerrain[0][0];

        var inView = new EmpireTerrain[endY - startY + 1][endX - startX + 1];

        var yN = 0;
        for (var y = startY; y <= endY; y++) {
            var xN = 0;
            for (var x = startX; x <= endX; x++) {
                inView[yN][xN] = terrain[y][x];
                if (inView[yN][xN] == null) {
                    inView[yN][xN] = new EmpireTerrain(noVisionEmpireTerrainType, new Position(x, y));
                }
                xN++;
            }
            yN++;
        }
        return inView;
    }

    public int getTileIndex(double position) {
        return (int) (position / (tileSize * zoom));
    }

    public Point2D mapPositionFromViewPosition(double x, double y) {
        return mapPositionFromViewPosition(new Point2D(x, y));
    }
    public Point2D mapPositionFromViewPosition(Point2D position) {
        var dx = viewWidth / 2 - position.getX();
        var dy = viewHeight / 2 - position.getY();
        var x = lookAt.getX() * zoom - dx;
        var y = lookAt.getY() * zoom - dy;
        return new Point2D(x, y);
    }

    public Point2D viewPositionFromTilePosition(int x, int y) {
        return viewPositionFromTilePosition(new Position(x, y));
    }
    public Point2D viewPositionFromTilePosition(Position position) {
        var dx = lookAt.getX() - position.getX() * tileSize;
        var dy = lookAt.getY() - position.getY() * tileSize;
        var x = viewWidth / 2 - dx * zoom;
        var y = viewHeight / 2 - dy * zoom;
        return new Point2D(x, y);
    }

    public void updateViewSize(double width, double height) {
        if (width <= 0 || height <= 0) return;
        viewWidth = width;
        viewHeight = height;
        lookAt = checkAndFixLookAtBoundaries(lookAt);
    }

    public double getViewWidth() {
        return viewWidth;
    }

    public double getViewHeight() {
        return viewHeight;
    }

    private Point2D checkAndFixLookAtBoundaries(Point2D lookAt) {
        var newX = lookAt.getX();
        var newY = lookAt.getY();

        if (newX < 0) newX = 0;
        else if (newX > mapBorder.getWidth()) newX = mapBorder.getWidth();
        if (newY < 0) newY = 0;
        else if (newY > mapBorder.getHeight()) newY = mapBorder.getHeight();

        return new Point2D(newX, newY);
    }

}
