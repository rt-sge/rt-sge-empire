package at.ac.tuwien.ifs.sge.game.empire.visualization;

import at.ac.tuwien.ifs.sge.game.empire.map.Position;

public enum Direction {
    DOWN,
    DOWN_LEFT,
    LEFT,
    UP_LEFT,
    UP,
    UP_RIGHT,
    RIGHT,
    DOWN_RIGHT;

    public static Direction fromPositions(Position pos, Position target) {
        var dx = target.getX() - pos.getX();
        var dy = target.getY() - pos.getY();
        if (dx > 0) {
            if (dy < 0)
                return Direction.UP_RIGHT;
            else if (dy == 0)
                return Direction.RIGHT;
            else
                return Direction.DOWN_RIGHT;
        } else if (dx == 0) {
            if (dy < 0)
                return Direction.UP;
            else if (dy == 0)
                return null;
            else
                return Direction.DOWN;
        } else {
            if (dy < 0)
                return Direction.UP_LEFT;
            else if (dy == 0)
                return Direction.LEFT;
            else
                return Direction.DOWN_LEFT;
        }
    }
}