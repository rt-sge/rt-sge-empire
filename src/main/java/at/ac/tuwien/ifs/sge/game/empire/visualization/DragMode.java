package at.ac.tuwien.ifs.sge.game.empire.visualization;

public enum DragMode {
    none,
    initialized,
    moveCamera;

    static final int MIN_DRAG_DISTANCE = 5;
}
