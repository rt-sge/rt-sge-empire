package at.ac.tuwien.ifs.sge.game.empire.visualization;

import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.SpriteSrc;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrainType;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EmpireTerrainSprite {

    public static  Map<String, EmpireTerrainSprite> fromConfig(EmpireConfiguration configuration, double tileSize) throws MalformedURLException {
        var terrainSpritesByType = new HashMap<String, EmpireTerrainSprite>();
        var terrainTypes = configuration.getTerrainTypes();
        for (var terrainType : terrainTypes) {
            var sprite = EmpireTerrainSprite.fromType(terrainType, tileSize);
            var type = terrainType.getName();
            terrainSpritesByType.put(type, sprite);
        }
        return terrainSpritesByType;
    }

    public static EmpireTerrainSprite fromType(EmpireTerrainType type, double tileSize) throws MalformedURLException {
        var image = loadImageFromSpriteSrc(type.getSpriteSrc());
        return new EmpireTerrainSprite(type, new Frame(image, new Point2D(tileSize / 2, tileSize / 2)));
    }

    private static Image loadImageFromSpriteSrc(SpriteSrc spriteSrc) throws MalformedURLException {
        String url;
        if (spriteSrc.isEmbedded()) {
            url = Objects.requireNonNull(EmpireTerrainSprite.class.getClassLoader().getResource(spriteSrc.getSrc())).toExternalForm();
        } else {
            url = new File(spriteSrc.getSrc()).toURI().toURL().toExternalForm();
        }
        return new Image(url);
    }

    private final EmpireTerrainType terrainType;
    private final Frame terrainFrame;

    public EmpireTerrainSprite(EmpireTerrainType terrainType, Frame terrainFrame) {
        this.terrainType = terrainType;
        this.terrainFrame = terrainFrame;
    }

    public void draw(GraphicsContext context, double x, double y, double zoom) {
        terrainFrame.draw(context, x, y, zoom, false);
    }

    public EmpireTerrainType getTerrainType() {
        return terrainType;
    }

    @Override
    public String toString() {
        return terrainType.getName();
    }

}
