package at.ac.tuwien.ifs.sge.game.empire.visualization;

import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.AnimationConfig;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.SpriteSrc;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.*;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EmpireUnitSprite {

    public static Map<Integer, EmpireUnitSprite> fromConfig(EmpireConfiguration configuration, double tileSize) throws IOException {
        var unitSpritesByType = new HashMap<Integer, EmpireUnitSprite>();
        var unitTypes = configuration.getUnitTypes();
        for (var unitType : unitTypes) {
            var sprite = EmpireUnitSprite.fromType(unitType, tileSize);
            var typeId = unitType.getUnitTypeId();
            unitSpritesByType.put(typeId, sprite);
        }
        return unitSpritesByType;
    }

    public static EmpireUnitSprite fromType(EmpireUnitType type, double tileSize) throws IOException {
        var spriteSrc = type.getSpriteSrc();

        var idleAnimations = loadAnimationsForAllDirections(spriteSrc, spriteSrc.getIdleAnimation(), tileSize);
        var movementAnimations = loadAnimationsForAllDirections(spriteSrc, spriteSrc.getMovementAnimation(), tileSize);
        var attackAnimations = loadAnimationsForAllDirections(spriteSrc, spriteSrc.getAttackAnimation(), tileSize);

        return new EmpireUnitSprite(type, idleAnimations, movementAnimations, attackAnimations);
    }

    private static HashMap<Direction, Animation> loadAnimationsForAllDirections(SpriteSrc spriteSrc, AnimationConfig config, double tileSize) throws IOException {
        var animations = new HashMap<Direction, Animation>();
        for (var i = 0; i < Direction.values().length; i++) {
            var direction = Direction.values()[i];
            if (i < 5) {
                var animation = loadAnimationFromConfig(spriteSrc, config, direction, tileSize);
                animations.put(direction, animation);
            } else {
                var mirroredIndex = Direction.values().length - i;
                animations.put(direction, animations.get(Direction.values()[mirroredIndex]));
            }

        }
        return animations;
    }

    private static Animation loadAnimationFromConfig(SpriteSrc spriteSrc, AnimationConfig config, Direction direction, double tileSize) throws IOException {
        var idleAnimationFrames = new Frame[config.getFrames()];
        var path = spriteSrc.getSrc() + config.getName() + "_" + direction.ordinal() + "_";
        Point2D[] anchors = null;
        if (config.hasAnchors()) {
            var anchorsPath = path + "anchors.txt";
            anchors = loadAnchorsFromPath(anchorsPath, config.getFrames(), spriteSrc.isEmbedded());
        }
        for (var i = 0; i < config.getFrames(); i++) {
            var framePath = path + (i + 1) + ".png";
            var image = loadImageFromPath(framePath, spriteSrc.isEmbedded());
            var anchor = anchors != null ? anchors[i] : new Point2D(tileSize / 2, tileSize / 2);
            idleAnimationFrames[i] = new Frame(image, anchor, config.getScale());
        }
        return new Animation(idleAnimationFrames, config.getSpeed());
    }

    private static Point2D[] loadAnchorsFromPath(String path, int nrOfFrames, boolean embedded) throws IOException {
        var anchors = new Point2D[nrOfFrames];
        InputStream stream;
        if (embedded) {
            stream = Objects.requireNonNull(EmpireUnitSprite.class.getClassLoader().getResourceAsStream(path));
        } else {
            stream = new FileInputStream(path);
        }
        var reader = new BufferedReader(new InputStreamReader(stream));
        for (var i = 0; i < nrOfFrames; i++) {
            var frameHeader = reader.readLine();
            var anchorCoords = reader.readLine();
            var coords = anchorCoords.split(",");
            anchors[i] = new Point2D(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]));
        }
        return anchors;
    }

    private static Image loadImageFromPath(String path, boolean embedded) throws MalformedURLException {
        String url;
        if (embedded) {
            url = Objects.requireNonNull(EmpireUnitSprite.class.getClassLoader().getResource(path)).toExternalForm();
        } else {
            url = new File(path).toURI().toURL().toExternalForm();
        }
        return new Image(url);
    }

    private static final double HEALTH_BAR_WIDTH = 30D;
    private static final double HEALTH_BAR_HEIGHT = 5D;

    private final EmpireUnitType unitType;
    private final Map<Direction, Animation>  idleAnimations;
    private final Map<Direction, Animation> movementAnimations;
    private final Map<Direction, Animation> attackAnimations;


    public EmpireUnitSprite(EmpireUnitType unitType,
                            Map<Direction, Animation>  idleAnimations,
                            Map<Direction, Animation> movementAnimations,
                            Map<Direction, Animation> attackAnimations) {
        this.unitType = unitType;
        this.idleAnimations = idleAnimations;
        this.movementAnimations = movementAnimations;
        this.attackAnimations = attackAnimations;
    }

    public void draw(GraphicsContext unitGc,
                     GraphicsContext infoGc,
                     Color[] playerColors,
                     EmpireMap map,
                     EmpireUnit unit,
                     double x,
                     double y,
                     double zoom,
                     Boolean showId) {
        var state = unit.getState();
        var timeInStateMs = unit.getTimeInCurrentState();
        var pos = unit.getPosition();
        var direction = Direction.DOWN;
        Animation currentAnimation;
        switch (state) {
            case Fighting -> {
                var targetId = unit.getTargetId();
                var targetPos = map.getPosition(targetId);
                if (targetPos != null)
                    direction = Direction.fromPositions(pos, targetPos);
                currentAnimation = attackAnimations.get(direction);
            }
            case Moving -> {
                var destination = unit.getDestination();
                direction = Direction.fromPositions(pos, destination);
                currentAnimation = movementAnimations.get(direction);
            }
            default -> currentAnimation = idleAnimations.get(direction);
        }
        if (currentAnimation == null) {
            // what happened? o_O
            return;
        }
        var currentFrame = currentAnimation.getFrame(timeInStateMs);
        currentFrame.draw(unitGc, x, y, zoom, direction.ordinal() > 4);

        var healthBarX = x - HEALTH_BAR_WIDTH / 2;
        var healthBarY = y - currentAnimation.getMaxAnchorY() * zoom - HEALTH_BAR_HEIGHT - 2;
        var hpFactor = unit.getHp() / (double) unit.getMaxHp();
        var healthWidth = HEALTH_BAR_WIDTH * hpFactor;
        infoGc.setFill(playerColors[unit.getPlayerId()]);
        infoGc.fillRect(healthBarX, healthBarY, healthWidth, HEALTH_BAR_HEIGHT);
        infoGc.strokeRect(healthBarX, healthBarY, HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT);

        if (showId) {
            infoGc.setFill(Color.WHITE);
            infoGc.setFont(Font.font(null, FontWeight.BOLD, 12));
            infoGc.fillText(unit.getId().toString(), healthBarX, healthBarY + 30);
        }
    }





}
