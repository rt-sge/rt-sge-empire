package at.ac.tuwien.ifs.sge.game.empire.visualization;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXVisualization;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.*;

public class EmpireVisualization implements JavaFXVisualization<Empire, EmpireEvent> {


    private static final String[] KELLY_COLORS = new String[]{"#1d1d1d", "#fdfdfd", "#ebce2b", "#702c8c", "#db6917", "#96cde6", "#ba1c30", "#c0bd7f", "#7f7e80", "#d485b2", "#4277b6", "#df8461", "#463397", "#e1a11a", "#91218c", "#e8e948", "#7e1510", "#6f340d", "#d32b1e", "#2b3514"};

    private static final int TILE_SIZE = 48;
    private static final int MIN_TILE_SIZE = 16;
    private static final int MAX_TILE_SIZE = 64;
    private static final Color DISCOVERED_TILE_PAINT = new Color(0, 0, 0, 0.4);

    private static final int SIDEBAR_WIDTH = 200;

    private Map<String, EmpireTerrainSprite> terrainSpritesByType;
    private Map<Integer, EmpireUnitSprite> unitSpritesByType;
    private Color[] playerColors;

    private final Logger log;
    private Empire game;

    private ObservableList<Player> players;

    private ArrayList<Canvas> layers;
    private Canvas background;
    private Canvas units;
    private Canvas information;

    private Camera camera;
    private MouseNavigation mouseNavigation;

    private VisionMode visionMode = VisionMode.ALL_PLAYERS;

    private int observedPlayer = 0;

    public EmpireVisualization(Logger log) {
        this.log = log;
    }

    @Override
    public Node initialize(Empire game, String[] playerNames, double width, double height) {
        this.game = game;
        generatePlayers(playerNames);

        camera = new Camera(game, TILE_SIZE, MIN_TILE_SIZE, MAX_TILE_SIZE, width - SIDEBAR_WIDTH, height, null);

        generateLayers();

        loadSprites();

        Pane gamePane = new Pane(background, units, information);

        mouseNavigation = new MouseNavigation(camera, gamePane);
        mouseNavigation.registerDragMove();
        mouseNavigation.registerScrollZoom();
        mouseNavigation.registerHoverCoordinates();

        redraw();

        var sideBar = generateSideBar();

        return new HBox(sideBar, gamePane);
    }

    @Override
    public void redraw() {
        var bgGc = background.getGraphicsContext2D();
        var unitGc = units.getGraphicsContext2D();
        var infoGc = information.getGraphicsContext2D();
        bgGc.clearRect(0, 0, camera.getViewWidth(), camera.getViewHeight());
        unitGc.clearRect(0, 0, camera.getViewWidth(), camera.getViewHeight());
        infoGc.clearRect(0, 0, camera.getViewWidth(), camera.getViewHeight());

        var tiles = camera.getTerrainInView();
        if (tiles.length == 0 || tiles[0].length == 0) return;

        var map = game.getBoard();
        var vision = map.getActiveVisionByPosition();
        var visited = map.getDiscoveredByPosition();

        var firstTilePos = tiles[0][0].getPosition();
        var firstTileViewPos = camera.viewPositionFromTilePosition(firstTilePos);
        var xView = firstTileViewPos.getX();
        var yView = firstTileViewPos.getY();

        var mousePosition = mouseNavigation.getHoverPosition();

        var zoom = camera.getZoom();
        var tileSize = TILE_SIZE * zoom;

        for (var y = 0; y < tiles.length; y++) {
            for (var x = 0; x < tiles[0].length; x++) {
                var tile = tiles[y][x];
                var pos = tile.getPosition();

                var visible = true;
                var discovered = true;
                if (visionMode != VisionMode.EVERYTHING) {
                    discovered = false;
                    visible = false;
                    var visionOrigins = vision.get(pos);
                    var wasVisited = visited.get(pos);
                    if (visionMode == VisionMode.ALL_PLAYERS) {
                        for (var pid = 0; pid < game.getNumberOfPlayers(); pid++) {
                            if (visionOrigins[pid].hasVision()) {
                                visible = true;
                                break;
                            }
                            if (wasVisited[pid])
                                discovered = true;
                        }
                    } else if (visionOrigins[observedPlayer].hasVision()) {
                        visible = true;
                    } else if (wasVisited[observedPlayer]) {
                        discovered = true;
                    }
                }

                var xUpLeft = xView + x * tileSize;
                var yUpLeft = yView + y * tileSize;
                if ((visible || discovered) && tile.getMapIdentifier() != Camera.noVisionEmpireTerrainType.getMapIdentifier()) {
                    var terrainSprite = terrainSpritesByType.get(tile.getName());
                    var xPos = xUpLeft + tileSize / 2;
                    var yPos = yUpLeft + tileSize / 2;
                    terrainSprite.draw(bgGc, xPos, yPos, zoom);

                    if (visible) {
                        var occupants = tile.getOccupants();
                        if (occupants != null && occupants.size() > 0) {
                            var unit = occupants.peek();
                            var typeId = unit.getUnitTypeId();
                            var unitSprite = unitSpritesByType.get(typeId);
                            var showId = getDistance(mousePosition, new Point2D(xPos, yPos)) < 10;
                            unitSprite.draw(unitGc, infoGc, playerColors, map, unit, xPos, yPos, zoom, showId);

                            if (occupants.size() > 1) {
                                infoGc.setFill(Color.WHITE);
                                infoGc.setFont(Font.font(null, FontWeight.BOLD, 14));
                                infoGc.fillText(Integer.toString(tile.getOccupants().size()), xUpLeft + tileSize / 2, yUpLeft + tileSize);
                            }
                        }

                    } else {
                        bgGc.setFill(DISCOVERED_TILE_PAINT);
                        bgGc.fillRect(xUpLeft - 1, yUpLeft - 1, tileSize, tileSize);
                    }
                } else {
                    bgGc.setFill(Color.BLACK);
                    bgGc.fillRect(xUpLeft - 1, yUpLeft - 1, tileSize + 1, tileSize + 1);
                }
            }
        }
    }

    @Override
    public void applyUpdate(EmpireEvent action) {
        redraw();
    }

    @Override
    public boolean isAutoResizing() {
        return true;
    }

    @Override
    public void updateViewSize(double width, double height) {
        camera.updateViewSize(width - SIDEBAR_WIDTH, height);
        for (var layer : layers) {
            layer.setWidth(camera.getViewWidth());
            layer.setHeight(camera.getViewHeight());
        }
        redraw();
    }

    public void setObservedPlayer(int observedPlayer) {
        this.observedPlayer = observedPlayer;
    }

    public void setVisionMode(VisionMode visionMode) {
        this.visionMode = visionMode;
    }

    private double getDistance(Point2D pos1, Point2D pos2) {
        var dx = pos1.getX() - pos2.getX();
        var dy = pos1.getY() - pos2.getY();
        return Math.sqrt(dx * dx + dy * dy);
    }

    private void generatePlayers(String[] playerNames) {
        players = FXCollections.observableArrayList();
        var colors = Arrays.asList(KELLY_COLORS);
        //Collections.shuffle(colors);
        playerColors = new Color[game.getNumberOfPlayers()];
        for (var i = 0; i < game.getNumberOfPlayers(); i++) {
            var color = java.awt.Color.decode(colors.get(i));
            playerColors[i] = new Color(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, color.getAlpha() / 255f);
            players.add(new Player(i, playerNames[i], playerColors[i]));
        }
    }

    private void generateLayers() {
        var viewWidth = camera.getViewWidth();
        var viewHeight = camera.getViewHeight();
        background = new Canvas(viewWidth, viewHeight);
        units = new Canvas(viewWidth, viewHeight);
        information = new Canvas(viewWidth, viewHeight);
        Canvas gui = new Canvas(viewWidth, viewHeight);
        layers = new ArrayList<>(List.of(background, units, information, gui));
    }


    private void loadSprites() {
        try {
            terrainSpritesByType = EmpireTerrainSprite.fromConfig(game.getGameConfiguration(), TILE_SIZE);
            unitSpritesByType = EmpireUnitSprite.fromConfig(game.getGameConfiguration(), TILE_SIZE);
        } catch (Exception e) {
            log.error("Could not load sprite:");
            log.printStackTrace(e);
        }
    }

    private Node generateSideBar() {

        var visionLabel = new Label("Vision on: ");
        var visionChoiceBox = new ChoiceBox<>(FXCollections.observableArrayList(VisionMode.values()));
        visionChoiceBox.getSelectionModel().select(visionMode);
        visionChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> visionMode = newValue);
        visionChoiceBox.setMinWidth(150);
        VBox.setMargin(visionChoiceBox, new Insets(0, 0, 5, 0));

        var playerChoiceBox = new ChoiceBox<>(players);
        playerChoiceBox.getSelectionModel().select(observedPlayer);
        playerChoiceBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> observedPlayer = newValue.getPlayerId());
        playerChoiceBox.setMinWidth(150);
        VBox.setMargin(playerChoiceBox, new Insets(0, 0, 20, 0));
        playerChoiceBox.disableProperty().bind(visionChoiceBox.valueProperty().isNotEqualTo(VisionMode.PLAYER));

        var playerList = new ListView<Player>();
        playerList.setFocusTraversable(false);
        playerList.setCellFactory(list -> new PlayerListCell());
        playerList.setItems(players);
        VBox.setMargin(playerList, new Insets(0, 0, 20, 0));

        var navigationMovementLabel = new Label("Moving around:");
        navigationMovementLabel.setFont(Font.font(null, FontWeight.BOLD, 12));
        VBox.setMargin(navigationMovementLabel, new Insets(0, 0, 5, 0));
        var navigationMovement = new Label("Press any mouse button and drag");
        navigationMovement.setWrapText(true);
        VBox.setMargin(navigationMovement, new Insets(0, 0, 10, 0));

        var navigationZoomLabel = new Label("Zooming:");
        navigationZoomLabel.setFont(Font.font(null, FontWeight.BOLD, 12));
        VBox.setMargin(navigationZoomLabel, new Insets(0, 0, 5, 0));
        var navigationZoom = new Label("Scroll with the mouse wheel");
        navigationZoom.setWrapText(true);

        var vBox = new VBox(visionLabel, visionChoiceBox, playerChoiceBox, playerList, navigationMovementLabel, navigationMovement, navigationZoomLabel, navigationZoom);
        vBox.setMinWidth(SIDEBAR_WIDTH);
        vBox.setMaxWidth(SIDEBAR_WIDTH);
        vBox.setPadding(new Insets(0, 10, 0, 0));
        vBox.setAlignment(Pos.CENTER_LEFT);
        return vBox;
    }


}
