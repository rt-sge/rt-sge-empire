package at.ac.tuwien.ifs.sge.game.empire.visualization;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Frame {

    private final Image image;
    private final Point2D anchor;

    private final double scale;

    public Frame(Image image, Point2D anchor) {
        this.image = image;
        this.anchor = anchor;
        this.scale = 1;
    }

    public Frame(Image image, Point2D anchor, double scale) {
        this.image = image;
        this.anchor = anchor;
        this.scale = scale;
    }

    public void draw(GraphicsContext context, double x, double y, double zoom, boolean mirrored) {
        var frameWidth = image.getWidth() * zoom * scale;
        var frameHeight = image.getHeight() * zoom * scale;

        if (mirrored) {
            var dx = anchor.getX() * zoom * scale;
            var dy = -anchor.getY() * zoom * scale;
            context.drawImage(image, x + dx, y + dy, -frameWidth, frameHeight);
        } else {
            var dx = -anchor.getX() * zoom * scale;
            var dy = -anchor.getY() * zoom * scale;
            context.drawImage(image, x + dx, y + dy, frameWidth, frameHeight);
        }
    }

    public double getAnchorY() {
        return anchor.getY() * scale;
    }

    public Image getImage() {
        return image;
    }

    public Point2D getAnchor() {
        return anchor;
    }
}
