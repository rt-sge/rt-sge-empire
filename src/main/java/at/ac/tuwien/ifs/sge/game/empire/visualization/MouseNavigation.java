package at.ac.tuwien.ifs.sge.game.empire.visualization;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

import java.util.HashMap;
import java.util.Map;

public class MouseNavigation {

    private final Camera camera;
    private final Node node;

    private DragMode dragMode = DragMode.none;
    private Point2D dragStartPos;
    private Point2D dragStartLookAt;
    private Point2D hoverPosition = new Point2D(0, 0);

    private Map<MouseButton, Boolean> buttonPressed = new HashMap<>();
    private Map<MouseButton, EventHandler<MouseEvent>> buttonClicked = new HashMap<>();
    private boolean controlDown;

    private boolean hoverCoordinatesEnabled;
    private boolean dragMoveEnabled;
    private boolean scrollZoomEnabled;
    private boolean buttonPressedEnabled;
    private boolean buttonClickedEnabled;

    public MouseNavigation(Camera camera, Node node) {
        this.camera = camera;
        this.node = node;
    }

    public void registerDragMove() {
        if (!dragMoveEnabled) {
            if (!buttonPressedEnabled) {
                node.setOnMousePressed(this::onMousePressed);
                node.setOnMouseReleased(this::onMouseReleased);
            }
            node.setOnMouseDragged(this::onMouseDragged);
        }
        dragMoveEnabled = true;
    }

    public void registerScrollZoom() {
        if (!scrollZoomEnabled)
            node.setOnScroll(this::onScroll);
        scrollZoomEnabled = true;
    }

    public void registerHoverCoordinates() {
        if (!hoverCoordinatesEnabled) {
            if (!dragMoveEnabled)
                node.setOnMouseDragged(this::onMouseDragged);
            node.setOnMouseMoved(this::onMouseMoved);
        }
        hoverCoordinatesEnabled = true;
    }

    public void registerButtonPressed(MouseButton button) {
        buttonPressed.put(button, false);
        if (!buttonPressedEnabled && !dragMoveEnabled) {
            node.setOnMousePressed(this::onMousePressed);
            node.setOnMouseReleased(this::onMouseReleased);
        }
        buttonPressedEnabled = true;
    }

    public void registerOnMouseClicked(MouseButton button, EventHandler<MouseEvent> eventHandler) {
        buttonClicked.put(button, eventHandler);
        node.setOnMouseClicked(this::onMouseClicked);
        buttonClickedEnabled = true;
    }

    public Point2D getHoverPosition() {
        return hoverPosition;
    }

    public boolean isPressed(MouseButton button) {
        return buttonPressed.getOrDefault(button, false);
    }

    public boolean isControlDown() {return controlDown; }

    private void onMouseMoved(MouseEvent event) {
        controlDown = event.isControlDown();
        hoverPosition = new Point2D(event.getX(), event.getY());
    }

    private void onMousePressed(MouseEvent event) {
        controlDown = event.isControlDown();
        if (buttonPressedEnabled)
            buttonPressed.put(event.getButton(), true);

        if (dragMoveEnabled
                && (event.isMiddleButtonDown()
                || (!buttonPressedEnabled && event.isPrimaryButtonDown())
                || event.isSecondaryButtonDown())) {
            dragMode = DragMode.initialized;
            dragStartPos = new Point2D(event.getX(), event.getY());
            dragStartLookAt = camera.getLookAt();
        }
    }

    private void onMouseDragged(MouseEvent event) {
        controlDown = event.isControlDown();
        if (hoverCoordinatesEnabled)
            hoverPosition = new Point2D(event.getX(), event.getY());

        if (!dragMoveEnabled || dragMode == DragMode.none) return;

        var dx = event.getX() - dragStartPos.getX();
        var dy = event.getY() - dragStartPos.getY();
        if (dragMode == DragMode.initialized ) {
            if (Math.sqrt(dx * dx + dy * dy) > DragMode.MIN_DRAG_DISTANCE) {
                dragMode = DragMode.moveCamera;
            }
        } else {
            switch (dragMode) {
                case moveCamera:
                    var zoom = camera.getZoom();
                    var newLookAt = new Point2D(dragStartLookAt.getX() - dx / zoom, dragStartLookAt.getY() - dy / zoom);
                    camera.setLookAt(newLookAt);
                    break;
            }
        }
    }

    private void onMouseReleased(MouseEvent event) {
        if (buttonPressedEnabled)
            buttonPressed.put(event.getButton(), false);

        if (dragMoveEnabled && dragMode != DragMode.none) {
            dragMode = DragMode.none;
        }
    }

    private void onMouseClicked(MouseEvent event) {
        if (buttonClickedEnabled && buttonClicked.containsKey(event.getButton())) {
            buttonClicked.get(event.getButton()).handle(event);
        }
    }

    private void onScroll(ScrollEvent event) {
        var deltaY = event.getDeltaY();
        var zoomFactor = 1.05;
        if (deltaY < 0) {
            zoomFactor = 2 - zoomFactor;
        }
        camera.setZoom(camera.getZoom() * zoomFactor, event.getX(), event.getY());
    }
}
