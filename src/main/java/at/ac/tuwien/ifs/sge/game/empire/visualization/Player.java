package at.ac.tuwien.ifs.sge.game.empire.visualization;

import javafx.scene.paint.Color;

public class Player {
    private final Color color;
    private final int playerId;
    private final String name;

    public Player(int playerId, String name, Color color) {
        this.color = color;
        this.playerId = playerId;
        this.name = name;
    }

    public Color getColor() {
        return color;
    }


    public String getName() {
        return name;
    }

    public int getPlayerId() {
        return playerId;
    }


    @Override
    public String toString() {
        return name;
    }
}
