package at.ac.tuwien.ifs.sge.game.empire.visualization;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;


public class PlayerListCell extends ListCell<Player> {

    @Override
    protected void updateItem(Player item, boolean empty) {

        if (item != null) {
            var idLabel = new Label(Integer.toString(item.getPlayerId()));
            var colorRect = new Rectangle(0, 0, 20, 20);
            colorRect.setFill(item.getColor());
            HBox.setMargin(colorRect, new Insets(0, 20, 0, 10));
            var nameLabel = new Label(item.getName());
            var hBox = new HBox(idLabel, colorRect, nameLabel);
            hBox.setAlignment(Pos.CENTER_LEFT);
            hBox.setMinHeight(50);
            setGraphic(hBox);
        }
    }
}
