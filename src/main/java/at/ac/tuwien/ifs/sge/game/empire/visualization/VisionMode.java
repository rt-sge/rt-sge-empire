package at.ac.tuwien.ifs.sge.game.empire.visualization;

public enum VisionMode {
    PLAYER,
    ALL_PLAYERS,
    EVERYTHING
}
