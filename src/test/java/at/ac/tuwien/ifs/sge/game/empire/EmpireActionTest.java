package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireActionTest extends EmpireInitDataTest {
    @Test
    public void givenFightingUnit_whenHitAction_thenCorrectDamageTaken() throws InterruptedException, ActionException {
        empire.start();

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var target = empire.getUnit(infantryP0.getId());
        var attacker = empire.getUnit(infantryP1.getId());

        attacker.setState(EmpireUnitState.Fighting);

        int dmg = (int) target.getDamageRange().getUpperBound();
        var action = new CombatHitAction(attacker.getId(), target.getId(), dmg);

        empire.applyAction(action, 1);

        Thread.sleep(100);

        assertEquals(target.getMaxHp() - dmg, target.getHp());

    }

    @Test
    public void givenProducingCity_whenProductionAction_thenCorrectUnitSpawned() throws InterruptedException, EmpireMapException, ActionException {

        var city = (EmpireCity) empire.getBoard().getTile(new Position(5,5));
        var unitType = cavalry;

        city.setState(EmpireProductionState.Producing);

        infantryP1.setPosition(new Position(5,5));

        spawnUnit(empire, infantryP1);

        empire.getUnit(infantryP1.getId());

        var action = new ProductionAction(city.getPosition(), unitType.getUnitTypeId(), UUID.randomUUID());

        empire.start();

        empire.applyAction(action, 1);

        Thread.sleep(100);

        assertEquals(2, city.getOccupants().size());
        assertEquals(cavalry.getUnitTypeId(), city.getOccupants().peek().getUnitTypeId());
    }

    @Test
    public void givenProducingCity_whenInitialSpawnAction_thenCorrectUnitSpawned() throws InterruptedException, EmpireMapException, ActionException {

        var terrain = empire.getBoard().getTile(new Position(0,0));
        var unitType = cavalry;

        var action = new InitialSpawnAction(terrain.getPosition(), unitType.getUnitTypeId(), UUID.randomUUID(), 0);

        empire.start();

        empire.applyAction(action, 1);

        Thread.sleep(100);

        assertEquals(1, terrain.getOccupants().size());
        assertEquals(cavalry.getUnitTypeId(), terrain.getOccupants().peek().getUnitTypeId());
    }

    @Test
    public void givenMovingUnit_whenMoveAction_thenCorrectPosition() throws InterruptedException, ActionException {

        var unit = infantryP0;

        var source = unit.getPosition();
        var dest = new Position(unit.getPosition().getX() + 1, unit.getPosition().getY() + 1);

        var action = new MovementAction(unit.getId(), dest, source);

        empire.start();

        spawnUnit(empire, unit);

        var iP0 = empire.getUnit(infantryP0.getId());

        iP0.setState(EmpireUnitState.Moving);

        assertEquals(source, unit.getPosition());

        empire.applyAction(action, 1);

        Thread.sleep(100);

        assertEquals(dest, iP0.getPosition());
        assertNotEquals(source, dest);

    }

}
