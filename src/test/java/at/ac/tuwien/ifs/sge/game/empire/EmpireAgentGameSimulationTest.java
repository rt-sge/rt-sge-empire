package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireAgentGameSimulationTest extends EmpireInitDataTest {

    private Empire agentGame = null;

    @Test
    public void runningGameAndSimulationTest() throws InterruptedException, EmpireMapException, EmpireInternalException, ActionException {
        infantryP0.setPosition(new Position(6, 6));
        infantryP1.setPosition(new Position(6, 5));

        agentGame = new Empire(empire);

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());

        applyActionAndUpdates(spawnAction, 1, agentGame);

        spawnAction = new InitialSpawnAction(infantryP1.getPosition(), infantryP1.getUnitTypeId(), infantryP1.getId(), infantryP1.getPlayerId());

        applyActionAndUpdates(spawnAction, 1, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        empire.start();

        applyActionAndUpdates(new MovementStartOrder(iP0.getId(), new Position(5, 6)), 1, agentGame);

        var timeForMove = 1000d / iP0.getTilesPerSecond();
        Thread.sleep((long) (timeForMove * 0.1));

        empire.stop();

        agentGame.advance((long) timeForMove);

        assertEquals(new Position(5, 6), agentGame.getUnit(iP0.getId()).getPosition());
    }

    @Test
    public void ProductionActionTest() throws EmpireMapException, EmpireInternalException, InterruptedException, ActionException {
        infantryP0.setPosition(new Position(1, 1));
        infantryP1.setPosition(new Position(3, 1));

        agentGame = new Empire(empire);

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());

        applyActionAndUpdates(spawnAction, 0, agentGame);

        spawnAction = new InitialSpawnAction(infantryP1.getPosition(), infantryP1.getUnitTypeId(), infantryP1.getId(), infantryP1.getPlayerId());

        applyActionAndUpdates(spawnAction, 0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        empire.getCity(new Position(1,1)).setState(EmpireProductionState.Producing);

        empire.start();

        applyActionAndUpdates(new ProductionAction(iP0.getPosition(), 3, UUID.randomUUID()), 0, agentGame);

        assertEquals(2, agentGame.getUnitsById().size());
        assertEquals(3, empire.getUnitsById().size());
    }
}
