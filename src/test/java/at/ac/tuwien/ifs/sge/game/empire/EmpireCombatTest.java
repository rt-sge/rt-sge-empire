package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireCombatTest extends EmpireInitDataTest {

    @BeforeEach
    void resetTestData() {
        infantryP0.revive();
        infantryP1.revive();
        cavalryP1.revive();
        infantry2P0.revive();
    }

    @Test
    public void givenTwoValidUnits_whenInitiatingFight_thenFightTillDeath() throws InterruptedException {
        assertNotNull(empireConfiguration);
        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());


        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(iP0.getId(), iP1.getId())));

        empire.start();


        empire.waitForGameOver(TimeUnit.MINUTES.toMillis(10));



        assertTrue(iP0.getHp() <= 0 || iP1.getHp() <= 0);
    }


    @Test
    public void givenFourValidUnits_whenInitiatingOneFight_thenOneDies() throws InterruptedException {
        assertNotNull(empireConfiguration);
        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);
        spawnUnit(empire, infantry2P0);
        spawnUnit(empire, cavalryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());
        var i2P0 = empire.getUnit(infantry2P0.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.start();

        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(iP0.getId(), iP1.getId())));

        Thread.sleep(20000);

        assertTrue(iP0.getHp() <= 0 || iP1.getHp() <= 0);
    }


    @Test
    public void givenThreeValidUnits_whenInitiatingFight_thenFightTillDeath2v1() throws InterruptedException {
        assertNotNull(empireConfiguration);
        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);
        spawnUnit(empire, cavalryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.start();
        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(iP0.getId(), iP1.getId())));
        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(cP1.getId(), iP0.getId())));

        empire.waitForGameOver(TimeUnit.MINUTES.toMillis(10));

        assertTrue(iP0.getHp() <= 0 || iP1.getHp() <= 0);
    }

    @Test
    public void givenThreeValidUnits_whenSwitchingTargets_thenFightTillDeath2v1() throws InterruptedException {
        assertNotNull(empireConfiguration);
        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantry2P0);
        spawnUnit(empire, cavalryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var i2P0 = empire.getUnit(infantry2P0.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(cP1.getId(), iP0.getId())));
        empire.start();
        Thread.sleep(1000);
        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(cP1.getId(), i2P0.getId())));

        empire.waitForGameOver(TimeUnit.MINUTES.toMillis(10));

        assertTrue(iP0.getHp() < iP0.getMaxHp() || i2P0.getHp() < i2P0.getMaxHp());
    }

    @Test
    public void givenValidUnit_whenQueueNoInternalHitAction_thenInvalidAction() throws InterruptedException {
        assertNotNull(empireConfiguration);

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        empire.start();

        empire.scheduleActionEvent(new GameActionEvent<>(iP0.getPlayerId(), new CombatHitAction(iP0.getId(), iP1.getId(), iP0.getDamageRange().getNextDamage()), System.currentTimeMillis() + 1));

        Thread.sleep(10);

        assertTrue(iP0.getHp() == infantry.getMaxHp() || iP1.getHp() == infantry.getMaxHp());
    }

    @Test
    public void given2ValidUnitsInCity_whenAttackingNotTopOccupant_thenInvalidAction() throws InterruptedException {
        assertNotNull(empireConfiguration);

        infantryP1.setPosition(new Position(1,1));
        cavalryP1.setPosition(new Position(1,1));
        infantryP0.setPosition(new Position(0,1));

        spawnUnit(empire, infantryP1);
        spawnUnit(empire, cavalryP1);
        spawnUnit(empire, infantryP0);

        var iP1 = empire.getUnit(infantryP1.getId());
        var iP0 = empire.getUnit(infantryP0.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.start();

       empire.scheduleActionEvent(new GameActionEvent<>(0, new CombatStartOrder(iP0.getId(), iP1.getId()), System.currentTimeMillis() + 1));

        Thread.sleep(10);

        assertEquals(iP1.getHp(), infantry.getMaxHp());
        assertEquals(iP0.getState(), EmpireUnitState.Idle);
        assertEquals(iP1.getState(), EmpireUnitState.Idle);
        assertEquals(cP1.getState(), EmpireUnitState.Idle);
    }

    @Test
    public void given2ValidUnitsInCity_whenAttackingTopOccupant_thenInvalidAction() throws InterruptedException {
        assertNotNull(empireConfiguration);

        infantryP1.setPosition(new Position(1,1));
        cavalryP1.setPosition(new Position(1,1));

        infantryP0.setPosition(new Position(0,1));

        spawnUnit(empire, infantryP1);
        spawnUnit(empire, cavalryP1);
        spawnUnit(empire, infantryP0);

        var iP1 = empire.getUnit(infantryP1.getId());
        var iP0 = empire.getUnit(infantryP0.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.start();

        empire.scheduleActionEvent(new GameActionEvent<>(0, new CombatStartOrder(iP0.getId(), cP1.getId()), System.currentTimeMillis() + 1));

        Thread.sleep(100);

        assertEquals(EmpireUnitState.Fighting, iP0.getState());
        assertEquals(EmpireUnitState.Fighting, cP1.getState());
        assertEquals(EmpireUnitState.Idle, iP1.getState());
    }
}
