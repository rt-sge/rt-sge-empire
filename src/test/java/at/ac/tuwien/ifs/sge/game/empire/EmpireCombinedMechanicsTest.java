package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireCombinedMechanicsTest extends EmpireInitDataTest {

    @Test
    public void givenTwoUnits_OneAttackingOneMovingOutOfRangeBeforeHit_thenNoHit() throws InterruptedException {
        empire.start();

        infantryP0.setPosition(new Position(0, 0));
        infantryP1.setPosition(new Position(1, 0));

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var attackingUnit = empire.getUnit(infantryP0.getId());
        var movingUnit = empire.getUnit(infantryP1.getId());

        //Move
        var timeForMove = 1000d / movingUnit.getTilesPerSecond();
        var timeForAttack = 1000d / attackingUnit.getHitsPerSecond();
        empire.scheduleActionEvent(new GameActionEvent<>(movingUnit.getPlayerId(), new MovementStartOrder(movingUnit, new Position(2, 0)), System.currentTimeMillis() + 1));

        Thread.sleep((int) (timeForMove - (timeForAttack / 2)));

        //Attack
        empire.scheduleActionEvent(
                new GameActionEvent<>(attackingUnit.getPlayerId(),
                        new CombatStartOrder(attackingUnit.getId(), movingUnit.getId()), System.currentTimeMillis() + 1));

        Thread.sleep((int) timeForAttack * 2L);

        assertFalse(empire.getBoard().areAdjacent(attackingUnit.getPosition(), movingUnit.getPosition()));
        assertEquals(infantry.getMaxHp(), movingUnit.getHp());
        assertEquals(EmpireUnitState.Idle, movingUnit.getState());
        assertEquals(EmpireUnitState.Idle, attackingUnit.getState());

    }

    @Test
    public void givenTwoUnits_OneAttackingOneMovingInRangeBeforeHit_thenHit() throws InterruptedException {
        empire.start();

        infantryP0.setPosition(new Position(0, 0));
        infantryP1.setPosition(new Position(1, 0));

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var attackingUnit = empire.getUnit(infantryP0.getId());
        var movingUnit = empire.getUnit(infantryP1.getId());

        //Move
        var timeForMove = 1000d / movingUnit.getTilesPerSecond();
        var timeForAttack = 1000d / attackingUnit.getHitsPerSecond();
        empire.scheduleActionEvent(new GameActionEvent<>(movingUnit.getPlayerId(), new MovementStartOrder(movingUnit, new Position(1, 1)), System.currentTimeMillis() + 1));

        Thread.sleep((int) timeForMove - (int) ((timeForAttack) * 0.8));

        //Attack
        empire.scheduleActionEvent(
                new GameActionEvent<>(attackingUnit.getPlayerId(),
                        new CombatStartOrder(attackingUnit.getId(), movingUnit.getId()), System.currentTimeMillis() + 1));

        Thread.sleep(10);

        assertEquals(EmpireUnitState.Fighting, attackingUnit.getState());

        Thread.sleep(2L * (int) timeForAttack);

        assertTrue(empire.getBoard().areAdjacent(attackingUnit.getPosition(), movingUnit.getPosition()));
        assertEquals(EmpireUnitState.Fighting, movingUnit.getState());
        assertEquals(EmpireUnitState.Fighting, attackingUnit.getState());
    }

    @Test
    public void givenTwoUnits_OneAttackingThenMovingOutOfRange_thenFightButMove() throws InterruptedException {

        empire.start();

        infantryP0.setPosition(new Position(1, 0));
        infantryP1.setPosition(new Position(0, 0));

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var attackingUnit = empire.getUnit(infantryP0.getId());
        var targetUnit = empire.getUnit(infantryP1.getId());

        //Move
        var timeForMove = 1000d / targetUnit.getTilesPerSecond();
        var timeForAttack = 1000d / attackingUnit.getHitsPerSecond();
        empire.scheduleActionEvent(
                new GameActionEvent<>(attackingUnit.getPlayerId(),
                        new CombatStartOrder(attackingUnit.getId(), targetUnit.getId()), System.currentTimeMillis() + 1));

        Thread.sleep((int) (timeForAttack / 2d));

        //Attack
        empire.scheduleActionEvent(
                new GameActionEvent<>(attackingUnit.getPlayerId(),
                        new MovementStartOrder(attackingUnit.getId(), new Position(2, 0)), System.currentTimeMillis() + 1));

        Thread.sleep(2L * (int) timeForMove);

        assertFalse(empire.getBoard().areAdjacent(attackingUnit.getPosition(), targetUnit.getPosition()));
        assertEquals(EmpireUnitState.Idle, targetUnit.getState());
        assertEquals(EmpireUnitState.Idle, attackingUnit.getState());
        assertEquals(new Position(2, 0), attackingUnit.getPosition());
    }
}
