package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInvalidConfigurationException;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireConfigurationTest extends EmpireInitDataTest {

    @Test
    public void givenInvalidConfigYaml_whenValidating_thenReturnFalse() throws IOException {
        var config = (EmpireConfiguration) EmpireConfiguration.getYaml().load(invalidConfigYaml);
        assertThrows(EmpireInvalidConfigurationException.class, config::validateConfiguration);
    }

    @Test
    public void givenValidConfigYaml_whenValidating_thenReturnTrue() throws IOException {
        var config = (EmpireConfiguration) EmpireConfiguration.getYaml().load(playgroundYaml3P);
        assertDoesNotThrow(config::validateConfiguration);
    }
    @Test
    public void givenValidConfigYaml2_whenValidating_thenReturnTrue() throws IOException {
        var config = (EmpireConfiguration) EmpireConfiguration.getYaml().load(defaultYaml);
        assertDoesNotThrow(config::validateConfiguration);
    }

    @Test
    public void givenValidConfig_whenGetConfigForPlayerAndValidate_thenReturnTrue() throws IOException {
        var config = (EmpireConfiguration) EmpireConfiguration.getYaml().load(playgroundYaml3P);

        var c = (EmpireConfiguration)config.getConfigurationForPlayer(0);
        assertDoesNotThrow(c::validateConfiguration);
    }
    @Test
    public void givenValidConfigWithStartingUnits_whenGetConfigForPlayerAndValidate_thenReturnTrue() throws IOException {
        var config = (EmpireConfiguration) EmpireConfiguration.getYaml().load(testStartingUnitsConfig);

        var c = (EmpireConfiguration)config.getConfigurationForPlayer(0);
        assertDoesNotThrow(c::validateConfiguration);
    }
}
