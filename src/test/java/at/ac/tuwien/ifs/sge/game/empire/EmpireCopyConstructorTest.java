package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireCopyConstructorTest extends EmpireInitDataTest {
    @Test
    public void EmpireUnitCopyTest() {
        Position position = new Position(0, 0);
        EmpireUnit unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, position);
        var copy = new EmpireUnit(unit);
        copy.getDamageRange().setUpperBound(8);
        unit.setState(EmpireUnitState.Fighting);
        unit.damageUnit(4);

        assertNotEquals(copy, unit);
    }

    @Test
    public void EmpireCopyTest() {
        Empire game = new Empire(empireConfiguration, logger);
        spawnUnit(game, infantryP0);
        var copy = new Empire(game);
        spawnUnit(game, cavalryP1);

        assertNotEquals(game, copy);
    }
}
