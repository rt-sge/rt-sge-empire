package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireFogOfWarTest extends EmpireInitDataTest {
    @Test
    public void fogOfWarMovementTest() throws InterruptedException {
        var cavalryP0 = new EmpireUnit(UUID.randomUUID(), 0, cavalry, new Position(6,6));
        infantryP1.setPosition(new Position(0,1));
        cavalryP1.setPosition(new Position(5, 1));

        spawnUnit(empire, cavalryP0);

        spawnUnit(empire, infantryP1);

        spawnUnit(empire, cavalryP1);


        var cP0 = empire.getUnit(cavalryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.start();

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(), new Position(5,5))));

        System.out.println(empire.getBoard().getDiscoveredMap(0));
        System.out.println(empire.getBoard().getDiscoveredMap(1));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));


        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(4, 5))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(3, 5))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(2, 4))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(1, 3))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(1, 2))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(1, 1))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new CombatStartOrder(iP1.getId(), cP0.getId())));

        Thread.sleep(500);

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(2, 1))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(3, 1))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(4, 1))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(5, 2))));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP1.getId(),  new Position(6, 2))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(6, 3))));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP1.getId(),  new Position(5, 3))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP0.getId(),  new Position(6, 4))));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(cP1.getId(),  new Position(5, 4))));

        Thread.sleep((long) (1.1/ cP0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new CombatStartOrder(cP1.getId(), cP0.getId())));

        System.out.println(empire.getBoard().getDiscoveredMap(0));
    }

    @Test
    public void fogOfWarKillTest() throws InterruptedException {
        var cavalryP0 = new EmpireUnit(UUID.randomUUID(),0, cavalry, new Position(6,6));
        infantryP0.setPosition(new Position(0,1));
        infantry2P0.setPosition(new Position(2,2));
        cavalryP1.setPosition(new Position(1, 1));

        spawnUnit(empire, cavalryP0);

        spawnUnit(empire, infantryP0);

        spawnUnit(empire, cavalryP1);

        spawnUnit(empire, infantry2P0);


        var cP0 = empire.getUnit(cavalryP0.getId());
        var iP0 = empire.getUnit(infantryP0.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());
        var i2P0 = empire.getUnit(infantry2P0.getId());

        empire.start();

        empire.scheduleActionEvent(GameActionEvent.internal(
                new CombatStartOrder(cP1.getId(), iP0.getId())));

        Thread.sleep(10000);

        empire.scheduleActionEvent(GameActionEvent.internal(
                new CombatStartOrder(cP1.getId(), i2P0.getId())));

        Thread.sleep(10000);

        assertFalse(iP0.isAlive());
    }
}
