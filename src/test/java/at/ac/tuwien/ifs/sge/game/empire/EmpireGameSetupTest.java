package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireGameSetupTest extends EmpireInitDataTest {

    @Test
    public void test_GameWinning() throws InterruptedException {
        var config = (EmpireConfiguration)EmpireConfiguration.getYaml().load(smallPlayGround3P);
        empire = new Empire(config, 3, logger);

        infantryP0.setPosition(new Position(0,0));
        infantryP1.setPosition(new Position(1,0));
        cavalryP2.setPosition(new Position(1,1));

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);
        spawnUnit(empire, cavalryP2);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());
        var cP2 = empire.getUnit(cavalryP2.getId());

        empire.start();

        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(iP0.getId(), cP2.getId())));
        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(iP1.getId(), cP2.getId())));

        empire.waitForGameOver(TimeUnit.MINUTES.toMillis(10));
        assertTrue(empire.isGameOver());
    }

}
