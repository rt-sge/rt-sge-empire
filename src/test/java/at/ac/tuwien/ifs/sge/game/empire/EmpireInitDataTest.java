package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameUpdate;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireYaml;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireTerrainType;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;
import at.ac.tuwien.ifs.sge.game.empire.visualization.EmpireVisualization;
import javafx.application.Application;
import javafx.application.Platform;
import org.junit.jupiter.api.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static at.ac.tuwien.ifs.sge.game.empire.util.EmpireUtil.streamToString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireInitDataTest {
    protected String defaultYaml;
    protected String default_test_yaml;
    protected String terrainYaml;
    protected String unitYaml;
    protected String playgroundYaml3P;
    protected String smallPlayGround3P;
    protected String invalidConfigYaml;
    protected String testStartingUnitsConfig;
    protected String justCitiesYaml;

    protected EmpireConfiguration empireConfiguration;
    protected Empire empire;
    protected EmpireVisualizationTest.VisualizationMock visualizationMock;

    protected EmpireUnit infantryP0;
    protected EmpireUnit infantryP1;
    protected EmpireUnit cavalryP1;
    protected EmpireUnit infantry2P0;
    protected EmpireUnit cavalryP2;
    protected EmpireUnitType infantry;
    protected EmpireUnitType cavalry;
    protected EmpireVisualization visualization;
    protected Thread visualizationThread;
    protected final Logger logger = new Logger(-1, "[sge ", "",
            "trace]: ", System.out, "",
            "debug]: ", System.out, "",
            "info]: ", System.out, "",
            "warn]: ", System.err, "",
            "error]: ", System.err, "");

    @BeforeAll
    void loadTestData() throws IOException {
        defaultYaml = streamToString(Objects.requireNonNull(Empire.class.getResourceAsStream("default_config_2p.yaml")));
        default_test_yaml = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("default_test_config.yaml")));
        terrainYaml = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("test_terrain.yaml")));
        unitYaml = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("test_unit.yaml")));
        playgroundYaml3P = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("playground_3p.yaml")));
        smallPlayGround3P = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("small_playground_3p.yaml")));
        invalidConfigYaml = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("invalid_config.yaml")));
        testStartingUnitsConfig = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("test_starting_units_config.yaml")));
        justCitiesYaml = streamToString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("just_cities.yaml")));

        // test terrain init
        assertNotNull(terrainYaml);
        var config = EmpireYaml.getYaml(EmpireTerrainType.class).load(terrainYaml);
        assertNotNull(config);
        assertEquals(config.getClass(), EmpireTerrainType.class);

        // test unit init
        assertNotNull(unitYaml);
        var unitType = EmpireYaml.getYaml(EmpireUnitType.class).load(unitYaml);
        assertNotNull(unitType);
        assertEquals(unitType.getClass(), EmpireUnitType.class);

        // test empireConfiguration init
        assertNotNull(default_test_yaml);
        empireConfiguration = EmpireConfiguration.getYaml().load(default_test_yaml);
        assertNotNull(empireConfiguration);
        assertEquals(empireConfiguration.getClass(), EmpireConfiguration.class);

        // test unit init
        assertNotNull(empireConfiguration);
        infantry = empireConfiguration.getUnitTypes().stream().filter(type -> type.getUnitTypeName().equals("Infantry")).findFirst().orElse(null);
        cavalry = empireConfiguration.getUnitTypes().stream().filter(type -> type.getUnitTypeName().equals("Cavalry")).findFirst().orElse(null);

        assertNotNull(infantry);
        assertNotNull(cavalry);

        visualizationThread = new Thread(() -> Application.launch(EmpireVisualizationTest.VisualizationMock.class));
        visualizationThread.start();
        visualizationMock = EmpireVisualizationTest.VisualizationMock.waitForInstance();
        assertNotNull(visualizationMock);

    }

    @BeforeEach
    public void resetUnitPosition() {
        infantryP0 = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(5, 5));
        assertNotNull(infantryP0);
        assertEquals(infantryP0.getClass(), EmpireUnit.class);
        assertEquals(infantryP0.getHp(), infantry.getMaxHp());

        infantry2P0 = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(5, 6));
        assertNotNull(infantry2P0);
        assertEquals(infantry2P0.getClass(), EmpireUnit.class);
        assertEquals(infantry2P0.getHp(), infantry.getMaxHp());

        infantryP1 = new EmpireUnit(UUID.randomUUID(), 1, infantry, new Position(6, 5));
        assertNotNull(infantryP1);
        assertEquals(infantryP1.getClass(), EmpireUnit.class);
        assertEquals(infantryP1.getHp(), infantry.getMaxHp());

        cavalryP1 = new EmpireUnit(UUID.randomUUID(), 1, cavalry, new Position(6, 6));
        assertNotNull(cavalryP1);
        assertEquals(cavalryP1.getClass(), EmpireUnit.class);
        assertEquals(cavalryP1.getHp(), cavalry.getMaxHp());

        cavalryP2 = new EmpireUnit(UUID.randomUUID(), 2, cavalry, new Position(6, 6));
        assertNotNull(cavalryP2);
        assertEquals(cavalryP2.getClass(), EmpireUnit.class);
        assertEquals(cavalryP2.getHp(), cavalryP2.getMaxHp());

        empire = new Empire(empireConfiguration, 2, logger);

        visualization = new EmpireVisualization(logger);
        Platform.runLater(() -> visualizationMock.visualizeGame(empire, visualization));
    }

    @AfterEach
    public void stopGame() {
        empire.stop();
    }

    @AfterAll
    public void stopVisualization() throws Exception {
        visualizationMock.stop();
    }

    public static void spawnUnit(Empire game, EmpireUnit unit) {
        game.applyActionEvent(GameActionEvent.internal(new InitialSpawnAction(unit.getPosition(), unit.getUnitTypeId(), unit.getId(), unit.getPlayerId())));
    }

    public void applyActionAndUpdates(EmpireEvent action, int playerId, Empire agentGame) throws EmpireMapException, EmpireInternalException, ActionException {
        var time = System.currentTimeMillis() + 1;
        var result = empire.applyAction(action, time);
        var updates = action.getUpdates(empire, result, time);

        List<GameUpdate<EmpireEvent>> uncoupledUpdates = new ArrayList<>();
        if (updates != null) {
            for (var update : updates) {
                var playersToUpdate = new ArrayList<>(update.getPlayersToUpdate());
                var updateAction = update.getAction();
                var exeTime = update.getExecutionTimeMs();

                playersToUpdate = (ArrayList<Integer>) serializeAndDeserialize(playersToUpdate);
                updateAction = (EmpireEvent) serializeAndDeserialize(updateAction);

                uncoupledUpdates.add(new GameUpdate<>(updateAction, playersToUpdate, exeTime));
            }
        }

        for (GameUpdate<EmpireEvent> n : uncoupledUpdates) {
            if (n.getPlayersToUpdate().contains(playerId))
                agentGame.applyAction(n.getAction(), time);
        }
    }

    private Object serializeAndDeserialize(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            byte[] array = bos.toByteArray();
            bos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(array);
            ObjectInput in = new ObjectInputStream(bis);
            Object o = in.readObject();
            in.close();
            return o;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
