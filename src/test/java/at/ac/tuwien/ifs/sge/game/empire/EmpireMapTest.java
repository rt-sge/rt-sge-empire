package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.EmpireMap;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireMapTest extends EmpireInitDataTest {
    @Test
    public void givenValidEmpireConfig_whenInstantiatingEmpireBoard_thenValidEmpireBoard() {
        // test map init
        assertNotNull(empireConfiguration);
        var board = new EmpireMap(empireConfiguration.getEmpireTiles(), new ConcurrentHashMap<>(),new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), 2);
        assertNotNull(board);
        assertEquals(board.getClass(), EmpireMap.class);
    }

    @Test
    public void givenValidConfig_whenSpawningUnitOnValidGras_thenSpawnAtCorrectPosition() throws EmpireMapException, EmpireInternalException {
        assertNotNull(empireConfiguration);

        var unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(0, 0));
        empire.spawnUnit(unit);
        assertEquals(empire.getBoard().getEmpireTiles()[0][0].getOccupants().size(), 1);
        assertEquals(empire.getBoard().getEmpireTiles()[0][0].getOccupants().peek(), unit);
    }

    @Test
    public void givenValidConfig_whenSpawning2UnitsOnValidGras_thenInvalidPositionException() throws EmpireMapException, EmpireInternalException {
        assertNotNull(empireConfiguration);

        var unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(0, 0));
        var unit2 = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(0, 0));
        empire.spawnUnit(unit);
        assertThrows(EmpireMapException.class, () -> empire.spawnUnit(unit2));
    }

    @Test
    public void givenValidConfig_whenSpawningUnitOnValidMountain_thenInvalidPositionException() {
        assertNotNull(empireConfiguration);

        var unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(3, 2));

        assertThrows(EmpireMapException.class, () -> empire.spawnUnit(unit));
    }

    @Test
    public void givenValidConfig_whenSpawningUnitsInEmptyCity_thenCorrectCityOccupants() throws EmpireMapException, EmpireInternalException {
        assertNotNull(empireConfiguration);

        var unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(1, 1));
        var unit2 = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(1, 1));
        empire.spawnUnit(unit);
        empire.spawnUnit(unit2);
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getClass(), EmpireCity.class);
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getOccupants().size(), 2);
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getOccupants().peek(), unit);
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getOccupants().toArray()[1], unit2);
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getPlayerId(), unit.getPlayerId());

    }
    @Test
    public void givenValidConfig_whenRemovingUnitOnValidGras_thenNoUnitOnTile() throws EmpireMapException, EmpireInternalException {
        assertNotNull(empireConfiguration);

        var unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(0, 0));
        empire.spawnUnit(unit);
        empire.removeUnit(unit.getId());
        assertEquals(empire.getBoard().getEmpireTiles()[0][0].getOccupants().size(), 0);
    }
    @Test
    public void givenValidConfig_whenRemovingUnitOnValidCity_thenEmptyUnoccupiedCity() throws EmpireMapException, EmpireInternalException {
        assertNotNull(empireConfiguration);

        var unit = new EmpireUnit(UUID.randomUUID(), 0, infantry, new Position(1, 1));
        empire.spawnUnit(unit);
        empire.removeUnit(unit.getId());
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getOccupants().size(), 0);
        assertEquals(empire.getBoard().getEmpireTiles()[1][1].getPlayerId(), -1);
    }
    @Test
    public void givenOneValidSpawnedUnit_whenGetPossibleActions_thenCorrectActionsReturned() throws EmpireMapException, EmpireInternalException {
        var unitP0 = infantryP0;
        unitP0.setPosition(new Position(1,0));

        empire.spawnUnit(unitP0);

        var actions = empire.getPossibleActions(unitP0.getPlayerId());
        System.out.println(actions.size() + " actions");
        for (EmpireEvent action : actions) {
            empire.getLogger().logEmpireEvent(action);
        }

        assertEquals(5, actions.size());
        assertFalse(actions.stream().anyMatch(e -> e.getClass() != MovementStartOrder.class));

        System.out.println(empire.getBoard());
    }

    @Test
    public void givenTwoValidSpawnedUnits_whenGetPossibleActions_thenCorrectActionsReturned() throws EmpireMapException, EmpireInternalException {
        var unitP0 = infantryP0;
        unitP0.setPosition(new Position(0,1));

        var unitP1 = infantryP1;
        unitP1.setPosition(new Position(1,1));

        empire.spawnUnit(unitP0);
        empire.spawnUnit(unitP1);

        var actions = empire.getPossibleActions(unitP0.getPlayerId());
        System.out.println(actions.size() + " actions");
        for (EmpireEvent action : actions) {
            empire.getLogger().logEmpireEvent(action);
        }

        assertEquals(5, actions.size());
        assertTrue(actions.stream().anyMatch(e -> e.getClass() == MovementStartOrder.class));
        assertTrue(actions.stream().anyMatch(e -> e.getClass() == CombatStartOrder.class));

        var actions2 = empire.getPossibleActions(unitP1.getPlayerId());
        System.out.println(actions2.size() + " actions");
        for (EmpireEvent empireEvent : actions2) {
            empire.getLogger().logEmpireEvent(empireEvent);
        }

        assertEquals(11, actions2.size());
        assertTrue(actions2.stream().anyMatch(e -> e.getClass() == MovementStartOrder.class));
        assertTrue(actions2.stream().anyMatch(e -> e.getClass() == CombatStartOrder.class));
    }
}

