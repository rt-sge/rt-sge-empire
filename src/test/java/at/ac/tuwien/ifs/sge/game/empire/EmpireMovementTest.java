package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireMovementTest extends EmpireInitDataTest {
    @Test
    public void givenSpawnedUnit_whenInitiatingMove_thenMovesToPosition() throws InterruptedException {
        infantryP0.setPosition(new Position(6, 6));

        spawnUnit(empire, infantryP0);

        var iP0 = empire.getUnit(infantryP0.getId());

        var dest = new Position(6, 5);

        empire.start();

        logger.info(empire.getBoard());

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(iP0.getId(), dest)));

        Thread.sleep((long) (0.5 / iP0.getTilesPerSecond() * 1000));
        assertEquals(EmpireUnitState.Moving, iP0.getState());
        Thread.sleep((long) (0.6 / iP0.getTilesPerSecond() * 1000));

        assertEquals(EmpireUnitState.Idle, iP0.getState());
        assertEquals(dest, iP0.getPosition());
    }

    @Test
    public void givenSpawnedUnit_whenMovingAndChangingDestination_thenResetTimerAndMoveToDestination() throws InterruptedException {

        infantryP0.setPosition(new Position(6, 6));

        spawnUnit(empire, infantryP0);

        var iP0 = empire.getUnit(infantryP0.getId());

        var dest = new Position(6, 5);
        var dest2 = new Position(5, 5);
        empire.start();

        logger.info(empire.getBoard());

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(iP0.getId(), dest)));

        Thread.sleep((long) (0.5 / iP0.getTilesPerSecond() * 1000));

        assertEquals(EmpireUnitState.Moving, iP0.getState());
        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(iP0.getId(), dest2)));

        Thread.sleep((long) (1.1 / iP0.getTilesPerSecond() * 1000));

        assertEquals(EmpireUnitState.Idle, iP0.getState());
        assertEquals(dest2, iP0.getPosition());
    }

    @Test
    public void givenSpawnedUnitInCity_whenMovingAnotherUnitToCity_thenTwoUnitsInCity() throws InterruptedException, EmpireMapException {
        var cityPos = new Position(5, 5);
        infantryP0.setPosition(cityPos);
        var dest = new Position(cityPos.getX() + 1, cityPos.getY());
        infantry2P0.setPosition(dest);

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantry2P0);

        var i2P0 = empire.getUnit(infantry2P0.getId());

        logger.info(empire.getBoard());

        empire.start();
        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(i2P0.getId(), cityPos)));

        Thread.sleep((long) (1.1 / i2P0.getTilesPerSecond() * 1000));

        assertEquals(EmpireUnitState.Idle, i2P0.getState());
        assertEquals(cityPos, i2P0.getPosition());
        assertEquals(2, empire.getBoard().getTile(cityPos).getOccupants().size());

    }

    @Test
    public void givenTwoSpawnedUnits_whenMovingToSameDest_thenOnlyOneReachesDestination() throws InterruptedException {

        infantryP0.setPosition(new Position(6, 6));
        var dest = new Position(5, 5);
        infantry2P0.setPosition(new Position(6, 5));

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantry2P0);

        var iP0 = empire.getUnit(infantryP0.getId());
        var i2P0 = empire.getUnit(infantry2P0.getId());

        empire.start();

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(iP0.getId(), dest)));

        Thread.sleep((long) (0.5 / i2P0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(i2P0.getId(), dest)));

        Thread.sleep((long) (0.6 / i2P0.getTilesPerSecond() * 1000));

        assertEquals(EmpireUnitState.Idle, iP0.getState());
        assertEquals(EmpireUnitState.Idle, i2P0.getState());
        assertEquals(dest, iP0.getPosition());
        assertEquals(new Position(6, 5), i2P0.getPosition());

    }

    @Test
    public void givenTwoSpawnedUnits_whenMovingToSameDestOneAfterAnother_thenOnlyOneReachesDestination() throws InterruptedException {

        infantryP0.setPosition(new Position(6, 6));
        var dest = new Position(5, 6);
        infantry2P0.setPosition(new Position(6, 5));

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantry2P0);

        var iP0 = empire.getUnit(infantryP0.getId());
        var i2P0 = empire.getUnit(infantry2P0.getId());

        empire.start();

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(iP0.getId(), dest)));

        Thread.sleep((long) (1/ i2P0.getTilesPerSecond() * 1000));

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(i2P0.getId(), dest)));

        Thread.sleep(100);

        assertEquals(EmpireUnitState.Idle, iP0.getState());
        assertEquals(EmpireUnitState.Idle, i2P0.getState());
        assertEquals(dest, iP0.getPosition());
        assertEquals(new Position(6, 5), i2P0.getPosition());

    }

    @Test
    public void given1SpawnedUnit_whenSimulatingAndMoving_thenUnitMovesAndIsNewInstance() throws InterruptedException, ActionException {
        infantryP0.setPosition(new Position(6, 6));
        var destination = new Position(5, 6);

        spawnUnit(empire, infantryP0);
        var iP0 = empire.getUnit(infantryP0.getId());
        var movementOrder = new MovementStartOrder(iP0.getId(), destination);
        Empire game = (Empire) empire.copy();
        GameActionEvent<EmpireEvent> event = new GameActionEvent<>(0,
                movementOrder,
                game.getGameClock().getGameTimeMs() + 1);
        game.scheduleActionEvent(event);
        game.advance(10000);
        var iP0Simulation = game.getUnit(infantryP0.getId());
        assertNotEquals(iP0, iP0Simulation);
        assertEquals(destination, iP0Simulation.getPosition());
    }

}
