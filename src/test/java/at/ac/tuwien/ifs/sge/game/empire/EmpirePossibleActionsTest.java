package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpirePossibleActionsTest extends EmpireInitDataTest {

    @Test
    public void givenGameWith1Unit_whenPossibleActions_thenCorrectActions() throws InterruptedException, EmpireMapException, EmpireInternalException {
        var unit = infantryP0;
        var unit2 = infantry2P0;
        var unit3 = infantryP1;
        unit.setPosition(new Position(1,1));
        unit3.setPosition(new Position(2, 1));
        unit2.setPosition(new Position(3,1));

        empire.spawnUnit(unit);
        empire.spawnUnit(unit2);
        empire.spawnUnit(unit3);

        var iP1 = empire.getUnitsById().get(unit3.getId());

        empire.start();

        empire.scheduleActionEvent(new GameActionEvent<>(unit3.getPlayerId(), new CombatStartOrder(unit3.getId(), unit.getId()), System.currentTimeMillis() + 10));

        Thread.sleep(100);

        assertEquals(EmpireUnitState.Fighting, iP1.getState());

        var actions = empire.getPossibleActions(1);
        System.out.println("Possible Actions\n");
        for (EmpireEvent e : actions) {
            empire.getLogger().logEmpireEvent(e);
        }
        System.out.println(empire.getBoard());

        assertEquals(8, actions.size());
    }

    @Test
    public void given2ValidUnitsInCity_whenPossibleActions_thenCorrectActions() throws InterruptedException, EmpireMapException {
        assertNotNull(empireConfiguration);

        infantryP1.setPosition(new Position(1,1));
        cavalryP1.setPosition(new Position(1,1));

        infantryP0.setPosition(new Position(0,1));

        spawnUnit(empire, infantryP1);
        spawnUnit(empire, cavalryP1);
        spawnUnit(empire, infantryP0);

        var iP1 = empire.getUnit(infantryP1.getId());
        var iP0 = empire.getUnit(infantryP0.getId());
        var cP1 = empire.getUnit(cavalryP1.getId());

        empire.start();

        var actions = empire.getPossibleActions(0);
        System.out.println("Possible Actions\n");
        for (EmpireEvent action : actions) {
            empire.getLogger().logEmpireEvent(action);
        }
        System.out.println(empire.getBoard());

        assertEquals(5, actions.size());

        actions = empire.getPossibleActions(1);
        System.out.println("Possible Actions\n");
        for (EmpireEvent e : actions) {
            empire.getLogger().logEmpireEvent(e);
        }
        System.out.println(empire.getBoard());

        assertEquals(18, actions.size());
    }


}
