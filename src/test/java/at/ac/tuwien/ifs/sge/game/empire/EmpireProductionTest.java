package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.ProductionStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireProductionTest extends EmpireInitDataTest {

    @Test
    public void givenNewGame_whenProduceUnit_thenUnitSpawnsInCity() throws InterruptedException, EmpireMapException {

        empire.start();
        infantryP0.setPosition(new Position(5, 5));

        spawnUnit(empire, infantryP0);

        var iP0 = empire.getUnit(infantryP0.getId());

        var terrain = empire.getBoard().getTile(iP0.getPosition());
        assertEquals(terrain.getMapIdentifier(), 'c');

        var city = (EmpireCity) terrain;
        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(city.getOccupants().size(), 1);
        assertEquals(city.getOccupants().peek(), iP0);

        var p = new ProductionStartOrder(city.getPosition(), infantry.getUnitTypeId());
        empire.scheduleActionEvent(new GameActionEvent<>(iP0.getPlayerId(), p, System.currentTimeMillis() + 1));

        Thread.sleep((infantry.getProductionTime() + 1) * 1000L);

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(2, city.getOccupants().size());
        assertEquals(new ArrayList<>(city.getOccupants()).get(1).getUnitTypeId(), infantry.getUnitTypeId());
    }

    @Test
    public void givenNewGameAndProducingCity_whenProduceAnotherUnitInSameCity_thenCancelProductionAndRestart() throws InterruptedException {

        empire.start();
        infantryP0.setPosition(new Position(5, 5));

        spawnUnit(empire, infantryP0);

        var iP0 = empire.getUnit(infantryP0.getId());

        var city = empire.getCity(iP0.getPosition());

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(1, city.getOccupants().size());
        assertEquals(iP0, city.getOccupants().peek());

        var p = new ProductionStartOrder(city.getPosition(), infantry.getUnitTypeId());
        empire.scheduleActionEvent(new GameActionEvent<>(iP0.getPlayerId(), p, System.currentTimeMillis() + 1));

        Thread.sleep(infantry.getProductionTime() * 1000L / 4);

        var p2 = new ProductionStartOrder(city.getPosition(), cavalry.getUnitTypeId());
        empire.scheduleActionEvent(new GameActionEvent<>(iP0.getPlayerId(), p2, System.currentTimeMillis() + 1));

        Thread.sleep((cavalry.getProductionTime() + 1) * 1000L);

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(2, city.getOccupants().size());
        assertEquals(cavalry.getUnitTypeId(), new ArrayList<>(city.getOccupants()).get(0).getUnitTypeId());
        assertEquals(infantry.getUnitTypeId(), new ArrayList<>(city.getOccupants()).get(1).getUnitTypeId());
    }

    @Test
    public void givenNewGame_whenProduceInDifferentCities_thenSpawnInCorrectCities() throws InterruptedException, EmpireMapException {

        empire.start();
        infantryP0.setPosition(new Position(5, 5));
        spawnUnit(empire, infantryP0);
        infantryP1.setPosition(new Position(1, 5));
        spawnUnit(empire, infantryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());


        var terrain = empire.getBoard().getTile(iP0.getPosition());
        var terrain2 = empire.getBoard().getTile(iP1.getPosition());
        assertEquals(terrain.getMapIdentifier(), 'c');
        assertEquals(terrain2.getMapIdentifier(), 'c');

        var city = (EmpireCity) terrain;
        var city2 = (EmpireCity) terrain2;

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(city.getOccupants().size(), 1);
        assertEquals(city.getOccupants().peek(), iP0);

        assertEquals(city2.getPlayerId(), iP1.getPlayerId());
        assertEquals(city2.getOccupants().size(), 1);
        assertEquals(city2.getOccupants().peek(), iP1);

        var p = new ProductionStartOrder(city.getPosition(), cavalry.getUnitTypeId());
        empire.scheduleActionEvent(GameActionEvent.internal(p));

        Thread.sleep(infantry.getProductionTime() * 1000L / 4);

        var p2 = new ProductionStartOrder(city2.getPosition(), infantry.getUnitTypeId());
        empire.scheduleActionEvent(GameActionEvent.internal(p2));

        Thread.sleep((cavalry.getProductionTime() + 1) * 1000L);

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(2, city.getOccupants().size());
        assertEquals(new ArrayList<>(city.getOccupants()).get(0).getUnitTypeId(), cavalry.getUnitTypeId());

        assertEquals(city2.getPlayerId(), iP1.getPlayerId());
        assertEquals(2, city2.getOccupants().size());
        assertEquals(new ArrayList<>(city2.getOccupants()).get(1).getUnitTypeId(), infantry.getUnitTypeId());
    }

    @Test
    public void givenNewGame_whenProduceUnitInUnoccupiedCity_thenUnverifiedAction() throws InterruptedException, EmpireMapException {

        empire.start();
        infantryP0.setPosition(new Position(1, 5));

        var terrain = empire.getBoard().getTile(infantryP0.getPosition());
        assertEquals(terrain.getMapIdentifier(), 'c');

        var city = (EmpireCity) terrain;

        assertEquals(city.getPlayerId(), EmpireCity.UNOCCUPIED_ID);
        assertEquals(city.getOccupants().size(), 0);

        var p = new ProductionStartOrder(city.getPosition(), infantry.getUnitTypeId());
        empire.scheduleActionEvent(new GameActionEvent<>(infantryP0.getPlayerId(), p, System.currentTimeMillis() + 1));

        Thread.sleep((infantry.getProductionTime() + 1) * 1000L);

        assertEquals(city.getPlayerId(), EmpireCity.UNOCCUPIED_ID);
        assertEquals(city.getOccupants().size(), 0);
    }

    @Test
    public void givenNewGame_whenStartProduceUnitAndStop_thenNoUnitSpawnsInCity() throws InterruptedException, EmpireMapException {

        empire.start();
        infantryP0.setPosition(new Position(5, 5));
        spawnUnit(empire, infantryP0);

        var iP0 = empire.getUnit(infantryP0.getId());

        var terrain = empire.getBoard().getTile(iP0.getPosition());
        assertEquals(terrain.getMapIdentifier(), 'c');

        var city = (EmpireCity) terrain;
        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(city.getOccupants().size(), 1);
        assertEquals(city.getOccupants().peek(), iP0);

        var p = new ProductionStartOrder(city.getPosition(), infantry.getUnitTypeId());
        empire.scheduleActionEvent(new GameActionEvent<>(iP0.getPlayerId(), p, System.currentTimeMillis() + 1));

        Thread.sleep((infantry.getProductionTime()) * 1000L / 4);

        empire.scheduleActionEvent(GameActionEvent.internal(new ProductionStopOrder(p.getCityPosition())));

        Thread.sleep((infantry.getProductionTime() + 1) * 1000L);

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(city.getOccupants().size(), 1);
    }

    @Test
    public void givenNewGame_whenQueueNoInternalProductionAction_thenInvalidAction() throws InterruptedException, EmpireMapException {

        empire.start();
        infantryP0.setPosition(new Position(5, 5));

        spawnUnit(empire, infantryP0);

        var iP0 = empire.getUnit(infantryP0.getId());

        var terrain = empire.getBoard().getTile(iP0.getPosition());
        assertEquals(terrain.getMapIdentifier(), 'c');

        var city = (EmpireCity) terrain;
        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(city.getOccupants().size(), 1);
        assertEquals(city.getOccupants().peek(), iP0);

        var p = new ProductionAction(city.getPosition(), infantry.getUnitTypeId(), UUID.randomUUID());
        empire.scheduleActionEvent(new GameActionEvent<>(iP0.getPlayerId(), p, System.currentTimeMillis() + 1));

        Thread.sleep((infantry.getProductionTime() + 1) * 1000L);

        assertEquals(city.getPlayerId(), iP0.getPlayerId());
        assertEquals(city.getOccupants().size(), 1);
    }
}
