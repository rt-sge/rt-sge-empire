package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireSimulationTest extends EmpireInitDataTest {

    @Test
    public void combatSimulationTest() throws ActionException {
        assertNotNull(empireConfiguration);

        empire.advance(100);

        spawnUnit(empire, infantryP0);
        spawnUnit(empire, infantryP1);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        empire.scheduleActionEvent(GameActionEvent.internal(new CombatStartOrder(iP0.getId(), iP1.getId())));

        empire.advance(20000);

        assertTrue(iP0.getHp() <= 0 || iP1.getHp() <= 0);
    }

    @Test
    public void movementSimulationTest() throws ActionException {
        infantryP0.setPosition(new Position(6, 6));
        spawnUnit(empire, infantryP0);

        empire.advance(1000);

        var iP0 = empire.getUnit(infantryP0.getId());
        var dest = new Position(6, 5);

        empire.scheduleActionEvent(GameActionEvent.internal(
                new MovementStartOrder(iP0.getId(), dest),empire.getGameClock().getGameTimeMs()));

        empire.advance((long) (500 / iP0.getTilesPerSecond()));

        assertEquals(EmpireUnitState.Moving, iP0.getState());

        empire.advance((long) (1000 / iP0.getTilesPerSecond()));

        assertEquals(EmpireUnitState.Idle, iP0.getState());
        assertEquals(dest, iP0.getPosition());
    }

}
