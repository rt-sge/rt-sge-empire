package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.CombatHitAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.InitialSpawnAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.MovementAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.ProductionAction;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.CombatStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.stop.MovementStopOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireInternalException;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmpireUpdatesTests extends EmpireInitDataTest {

    private Empire agentGame = null;

    @Test
    public void testUpdatesFrom_InitialSpawnAction() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);
        infantryP0.setPosition(new Position(0, 0));

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());

        applyActionAndUpdates(spawnAction,0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP0.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP0.getId()).getPosition()));
    }

    @Test
    public void testUpdatesFrom_ProductionAction() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);

        infantryP0.setPosition(new Position(3, 1));

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());
        applyActionAndUpdates(spawnAction,0, agentGame);

        var productionAction = new ProductionAction(new Position(3, 1), infantry2P0.getUnitTypeId(), infantry2P0.getId());
        ((EmpireCity) empire.getBoard().getTile(3, 1)).setState(EmpireProductionState.Producing);
        applyActionAndUpdates(productionAction,0, agentGame);

        var i2P0 = empire.getUnit(infantry2P0.getId());

        assertEquals(agentGame.getUnit(i2P0.getId()), empire.getUnit(i2P0.getId()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(i2P0.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(i2P0.getId()).getPosition()));
    }

    @Test
    public void testUpdatesFrom_MovementStartOrder() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);

        infantryP0.setPosition(new Position(0, 0));

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());
        applyActionAndUpdates(spawnAction,0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());

        var moveOrder = new MovementStartOrder(iP0.getId(), new Position(1, 1));
        applyActionAndUpdates(moveOrder,0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
    }

    @Test
    public void testUpdatesFrom_MovementStopOrder() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);
        infantryP0.setPosition(new Position(0, 0));

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());
        applyActionAndUpdates(spawnAction,0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());

        var moveOrder = new MovementStartOrder(iP0.getId(), new Position(1, 1));
        applyActionAndUpdates(moveOrder,0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));

        var moveStopOrder = new MovementStopOrder(iP0.getId());
        applyActionAndUpdates(moveStopOrder,0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
    }

    @Test
    public void testUpdatesFrom_MovementAction() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);

        infantryP0.setPosition(new Position(0, 0));

        var spawnAction = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());
        applyActionAndUpdates(spawnAction,0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());

        var moveOrder = new MovementStartOrder(iP0.getId(), new Position(1, 1));
        applyActionAndUpdates(moveOrder,0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));

        var movementAction = new MovementAction(iP0.getId(), new Position(1, 1), new Position(0, 0));
        applyActionAndUpdates(movementAction,0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
        assertEquals(new Position(1, 1), empire.getUnit(iP0.getId()).getPosition());
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP0.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP0.getId()).getPosition()));
    }

    @Test
    public void testUpdatesFrom_CombatStartOrder() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);

        infantryP1.setPosition(new Position(0, 0));
        infantryP0.setPosition(new Position(0, 1));

        var spawnAction = new InitialSpawnAction(infantryP1.getPosition(), infantryP1.getUnitTypeId(), infantryP1.getId(), infantryP1.getPlayerId());

        applyActionAndUpdates(spawnAction,0, agentGame);

        var spawnAction2 = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());

        applyActionAndUpdates(spawnAction2, 0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        var combatOrder = new CombatStartOrder(iP1.getId(), iP0.getId());

        applyActionAndUpdates(combatOrder, 0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
        assertEquals(agentGame.getUnit(iP1.getId()), empire.getUnit(iP1.getId()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP0.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP0.getId()).getPosition()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP1.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP1.getId()).getPosition()));
    }

    @Test
    public void testUpdatesFrom_CombatStopOrder() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);

        infantryP1.setPosition(new Position(0, 0));
        infantryP0.setPosition(new Position(0, 1));

        var spawnAction = new InitialSpawnAction(infantryP1.getPosition(), infantryP1.getUnitTypeId(), infantryP1.getId(), infantryP1.getPlayerId());

        applyActionAndUpdates(spawnAction, 0, agentGame);

        var spawnAction2 = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());

        applyActionAndUpdates(spawnAction2, 0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        var combatOrder = new CombatStartOrder(iP1.getId(), iP0.getId());

        applyActionAndUpdates(combatOrder, 0, agentGame);

        var combatStopOrder = new CombatStopOrder(iP1.getId());

        applyActionAndUpdates(combatStopOrder, 0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
        assertEquals(agentGame.getUnit(iP1.getId()), empire.getUnit(iP1.getId()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP0.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP0.getId()).getPosition()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP1.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP1.getId()).getPosition()));
    }

    @Test
    public void testUpdatesFrom_CombatHitAction() throws EmpireMapException, EmpireInternalException, ActionException {
        empire = new Empire(EmpireConfiguration.getYaml().load(default_test_yaml), 2, logger);

        agentGame = new Empire(empire);

        infantryP1.setPosition(new Position(0, 0));
        infantryP0.setPosition(new Position(0, 1));

        var spawnAction = new InitialSpawnAction(infantryP1.getPosition(), infantryP1.getUnitTypeId(), infantryP1.getId(), infantryP1.getPlayerId());

        applyActionAndUpdates(spawnAction, 0, agentGame);

        var spawnAction2 = new InitialSpawnAction(infantryP0.getPosition(), infantryP0.getUnitTypeId(), infantryP0.getId(), infantryP0.getPlayerId());

        applyActionAndUpdates(spawnAction2, 0, agentGame);

        var iP0 = empire.getUnit(infantryP0.getId());
        var iP1 = empire.getUnit(infantryP1.getId());

        var combatOrder = new CombatStartOrder(iP1.getId(), iP0.getId());

        applyActionAndUpdates(combatOrder, 0, agentGame);

        var combatHitAction = new CombatHitAction(combatOrder.getAttackerId(), combatOrder.getTargetId(), 1);

        applyActionAndUpdates(combatHitAction, 0, agentGame);

        assertEquals(agentGame.getUnit(iP0.getId()), empire.getUnit(iP0.getId()));
        assertEquals(agentGame.getUnit(iP1.getId()), empire.getUnit(iP1.getId()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP0.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP0.getId()).getPosition()));
        assertEquals(agentGame.getBoard().getTile(agentGame.getUnit(iP1.getId()).getPosition()),
                empire.getBoard().getTile(empire.getUnit(iP1.getId()).getPosition()));
    }

    @Test
    public void testUpdatesFrom_InitialSpawn() {
        empire = new Empire(EmpireConfiguration.getYaml().load(justCitiesYaml), 2, logger);

        var agentGame0 = new Empire(empire);
        var agentGame1 = new Empire(empire);

        var actions = empire.initialize();

        actions.forEach((p, value) -> {
            if (p == 0) {
                value.forEach(a  -> {
                    try {
                        agentGame0.applyAction(a, System.currentTimeMillis() + 10);
                    } catch (ActionException e) {
                        e.printStackTrace();
                    }
                });
            } else {
                value.forEach(a -> {
                    try {
                        agentGame1.applyAction(a, System.currentTimeMillis() + 10);
                    } catch (ActionException e) {
                        e.printStackTrace();
                    }
                });
            }
        });

        assertEquals(2, agentGame0.getUnitsById().size());
        assertEquals(2, agentGame1.getUnitsById().size());
    }
}
