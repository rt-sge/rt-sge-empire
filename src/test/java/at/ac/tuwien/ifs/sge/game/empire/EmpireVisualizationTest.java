package at.ac.tuwien.ifs.sge.game.empire;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.visualization.EmpireVisualization;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXVisualization;
import at.ac.tuwien.ifs.sge.gui.util.UI;
import com.google.common.io.Resources;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class EmpireVisualizationTest {

    private Empire game;
    private VisualizationMock visualizationMock;
    private Logger logger;



    @BeforeEach
    public void setup() {
        var thread = new Thread(() -> Application.launch(VisualizationMock.class));
        thread.start();
        visualizationMock = VisualizationMock.waitForInstance();
        assertNotNull(visualizationMock);
        logger = new Logger(0, "[sge ", "",
                "trace]: ", System.out, "",
                "debug]: ", System.out, "",
                "info]: ", System.out, "",
                "warn]: ", System.err, "",
                "error]: ", System.err, "");
        var url = Empire.class.getResource("default_config_2p.yaml");
        assertDoesNotThrow(() -> {
            EmpireConfiguration configuration = EmpireConfiguration.getYaml().load(Resources.toString(url, StandardCharsets.UTF_8));
            game = new Empire(configuration, 2, logger);
        });
    }

    @Test
    public void test_visualization() {
        var visualization = new EmpireVisualization(logger);
        Platform.runLater(() -> visualizationMock.visualizeGame(game, visualization));

        var unitP0 = new EmpireUnit(UUID.randomUUID(),
                0,
                game.getGameConfiguration().getUnitTypes().get(2),
                new Position(1, 0));
        var unitP1 = new EmpireUnit(UUID.randomUUID(),
                0,
                game.getGameConfiguration().getUnitTypes().get(1),
                new Position(3, 1));
        var unitP2 = new EmpireUnit(UUID.randomUUID(),
                0,
                game.getGameConfiguration().getUnitTypes().get(0),
                new Position(3, 1));

       EmpireInitDataTest.spawnUnit(game, unitP0);

        game.initialize();

        game.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) {}
    }

    public static class VisualizationMock extends Application {

        public static VisualizationMock waitForInstance() {
            try {
                var successful = INSTANCE_LATCH.await(2000, TimeUnit.MILLISECONDS);
                return successful ? instance : null;
            } catch (InterruptedException interruptedException) {
                return null;
            }
        }

        private static final float FRAMES_PER_SECOND = 30;
        private static final float VISUALIZATION_PADDING = 10;
        private static final CountDownLatch INSTANCE_LATCH = new CountDownLatch(1);
        private static VisualizationMock instance;

        private boolean stopped = false;

        private AnchorPane root;
        private JavaFXVisualization<?,?> visualization;
        private Timeline visualizationTimeline;

        public VisualizationMock() {
            instance = this;
            INSTANCE_LATCH.countDown();
        }


        @Override
        public void start(Stage stage) throws Exception {
            stage.setTitle("Visualization Mock");

            stage.setWidth(1920);
            stage.setHeight(1080);

            root = new AnchorPane();
            root.widthProperty().addListener(this::onViewSizeChanged);
            root.heightProperty().addListener(this::onViewSizeChanged);

            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        }

        @Override
        public void stop() throws Exception {
            if (visualizationTimeline != null)
                visualizationTimeline.stop();
            synchronized (this) {
                stopped = true;
                notifyAll();
            }
            super.stop();
        }

        public void waitTillStopped() throws InterruptedException {
            synchronized (this) {
                while (!stopped)
                    wait();
            }
        }

        public <G extends Game<A, ?>, A> void  visualizeGame(G game, JavaFXVisualization<G, A> visualization) {
            root.getChildren().clear();
            if (visualizationTimeline != null)
                visualizationTimeline.stop();

            this.visualization = visualization;
            var playerNames = new String[game.getNumberOfPlayers()];
            for (var i = 0; i < playerNames.length; i++)
                playerNames[i] = "Mock Agent " + i;
            var visualizationRoot = visualization.initialize(game,
                    playerNames,
                    root.getWidth() - 2 * VISUALIZATION_PADDING,
                    root.getHeight() - 2 * VISUALIZATION_PADDING);
            var hbox = new HBox();
            var vbox = new VBox();
            hbox.setAlignment(Pos.CENTER);
            vbox.setAlignment(Pos.CENTER);
            var vboxContentNode = visualizationRoot;
            if (!visualization.isAutoResizing()) {
                var scrollPane = new ScrollPane();
                scrollPane.setContent(visualizationRoot);
                vboxContentNode = scrollPane;
            }
            vbox.getChildren().add(vboxContentNode);
            hbox.getChildren().add(vbox);
            UI.setAllAnchors(hbox, VISUALIZATION_PADDING);
            root.getChildren().add(hbox);


            visualizationTimeline = new Timeline(
                    new KeyFrame(
                            Duration.millis(1000 / FRAMES_PER_SECOND),
                            event -> visualization.redraw()));
            visualizationTimeline.setCycleCount(Timeline.INDEFINITE);
            visualizationTimeline.play();
        }

        private void onViewSizeChanged(Observable observable, Number oldValue, Number newValue) {
            if (visualization != null)
                visualization.updateViewSize(root.getWidth() - 2 * VISUALIZATION_PADDING,
                        root.getHeight() - 2 * VISUALIZATION_PADDING);
        }
    }
}
